//
//  ChatViewCollectionViewController.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 2/17/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import UIKit
import Photos

class ChatViewCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, CustomHeaderDelegate{
    
    // MARK: - Properties
    fileprivate let textReuseIdentifier = "TextCell"
    fileprivate let photoReuseIdentifier = "PhotoCell"
    fileprivate let sectionInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
    fileprivate var databaseAccess:NewChatViewDatabaseAccess!
    internal var posts = [Post]()
    fileprivate var uniquePosts = Set<String>()
    fileprivate var imageDictionary = [String: UIImage?]()
    fileprivate var numParticipants = 0
    var isFromBubble = false
    var isOnChat = true
    var hideMessages = false
    var colViewBack = UIColor()
    var viewBack = UIColor()
    let screenWidth = UIScreen.main.bounds.width
    internal var headerView = ChatHeaderCollectionReusableView()
    internal var isSaved = false
    fileprivate var likeView = UIImageView()
    var panGestureRecognizer : UIPanGestureRecognizer = UIPanGestureRecognizer()
    
    var captureSession = AVCaptureSession()
    var navController = UINavigationController()
    var tabBar : UITabBar = UITabBar()
    var inputScreen : InputTabBarController? = nil
    
    var bubbleImageViews = [BubbleImageView]()
    var selectedConversationBubble = 0
    
    
    // Refresh controls
    var refreshControl: UIRefreshControl!
    var customRefreshView: UIView!
    var labelsArray: Array<UILabel> = []
    var isAnimating = false
    var currentColorIndex = 0
    var currentLabelIndex = 0
    var timer: Timer!
    
    var conversationTitle = ""
    var conversationID = ""
    
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    
    // location objects
    // let geoCoder = Geocoder()

    override func viewDidLoad() {
        super.viewDidLoad()
        databaseAccess = NewChatViewDatabaseAccess.init(chatView: self, conversationID: conversationID)
        databaseAccess.observeNumParticipants()
        databaseAccess.incrementNumParticipants(conversationID: conversationID)
        databaseAccess.observeMessages()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Do any additional setup after loading the view.
        setupGestureRecognizers()
        setupSubviews()
        
        // Remember colors of the background
        colViewBack = (self.collectionView?.backgroundColor)!
        //viewBack = self.view.backgroundColor!
        
        if isFromBubble {
            setupBubbleView()
        }
        
        self.setupUniquePosts()
    }
    
    private func setupUniquePosts() {
        for post in posts {
            uniquePosts.insert(post.postID)
        }
    }
    public func decrementNumParticipants() {
        databaseAccess.decrementNumParticipants(conversationID: conversationID)
    }
    
    private func setupBubbleView() {
        let savedConversations = Repo.sharedInstance.savedConversations
        
        let bigBubbleImageView = BubbleImageView(image: UIImage(named: "logo.png"))
        bigBubbleImageView.frame.size.height = bigBubbleImageView.RADIUS * 1.5
        bigBubbleImageView.frame.size.width = bigBubbleImageView.RADIUS * 1.5
        bigBubbleImageView.layer.cornerRadius = (bigBubbleImageView.RADIUS * 1.5)/2.0
        self.view.addSubview(bigBubbleImageView)
        
        bigBubbleImageView.center = CGPoint(x: self.view.frame.width / 2.0, y: self.view.frame.height - (bigBubbleImageView.frame.size.height/2.0) - 10.0)
        
        bubbleImageViews.append(bigBubbleImageView)
        
        if savedConversations.count > 1 {
            for index in 1...savedConversations.count-1 {
                let bubbleImageView = BubbleImageView(image: UIImage(named: "logo.png"))
                bubbleImageView.layer.cornerRadius = (bubbleImageView.RADIUS)/2.0
                self.view.addSubview(bubbleImageView)
                let distanceFromCenter:CGFloat = CGFloat((bigBubbleImageView.frame.size.width/2.0) + CGFloat(25.0 * Double(index)) + CGFloat((bubbleImageView.frame.size.width/2.0) * CGFloat((index * 2) - 1)))
                
                
                bubbleImageView.center = CGPoint(x: bigBubbleImageView.center.x + distanceFromCenter, y: bigBubbleImageView.center.y)
                
                bubbleImageViews.append(bubbleImageView)
            }
        }
    }
    
    private func checkIsSaved() {
        if Repo.sharedInstance.savedConversations.contains(where: { (conversation) in
            return conversation.conversationID == self.conversationID
        }) {
            self.setIsSaved(isSaved: true)
        } else {
            self.setIsSaved(isSaved: false)
        }
    }
    
    private func createNoMessagesLabel() {
        let label = UILabel()
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.lightGray
        label.text = "No Posts here yet. Start the Conversation!"
        
        view.superview?.addSubview(label)
    }
    
    private func loadCustomRefreshContents() {
        let refreshContents = Bundle.main.loadNibNamed("RefreshContents", owner: self, options: nil)
        customRefreshView = refreshContents?[0] as! UIView
        customRefreshView.frame = refreshControl.bounds
        
        for i in 1...customRefreshView.subviews.count {
            labelsArray.append(customRefreshView.viewWithTag(i) as! UILabel)
        }
        
        refreshControl.addSubview(customRefreshView)
    }
    
    private func setupGestureRecognizers() {
        
        let doubleTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didDoubleTapCollectionView(gesture:)))
        doubleTapGesture.numberOfTapsRequired = 2  // add double tap
        self.collectionView?.addGestureRecognizer(doubleTapGesture)
        
        let singleTapGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didSingleTapCollectionView(gesture:)))
        singleTapGesture.numberOfTapsRequired = 1
        singleTapGesture.require(toFail: doubleTapGesture)
        self.collectionView?.addGestureRecognizer(singleTapGesture)
        
        
        let pinchGesture: UIPinchGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(didPinchCollectionView(gesture:)))
        self.collectionView?.addGestureRecognizer(pinchGesture)
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(didLongPressCollectionView(gesture:)))
        self.collectionView?.addGestureRecognizer(longPressRecognizer)
        
        // Add the pan gesture
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(recognizer:)))
        panGestureRecognizer.require(toFail: (navController.interactivePopGestureRecognizer)!)
        self.view.addGestureRecognizer(panGestureRecognizer)
    }
    
    private func setupSubviews() {
        refreshControl = UIRefreshControl()
        refreshControl.backgroundColor = UIColor.clear //UIColor(colorLiteralRed: 144.0/255.0, green: 190.0/255.0, blue: 226.0/255.0, alpha: 1.0)
        refreshControl.tintColor = UIColor.clear //UIColor.white
        self.collectionView?.addSubview(refreshControl)
        loadCustomRefreshContents()
    }
    
    internal func didLongPressCollectionView(gesture: UILongPressGestureRecognizer) {
        let pointInCollectionView: CGPoint = gesture.location(in: self.collectionView)
        if let selectedIndexPath = self.collectionView!.indexPathForItem(at: pointInCollectionView) {
            let post = posts[(selectedIndexPath.item)]
            let potentialFlaggedPostID = post.postID
            let potentialFlaggedUserID = post.userID
            
            let alert = UIAlertController(title: "",
                                          message: "Flag as Inappropriate?",
                                          preferredStyle: .actionSheet)
            let saveAction = UIAlertAction(title: "Flag",
                                           style: .default) { _ in
                                            
                                            self.databaseAccess.flagPost(postID: potentialFlaggedPostID, userID: potentialFlaggedUserID)
            }
            
            let cancelAction = UIAlertAction(title: "Cancel",
                                             style: .default)
            
            alert.addAction(saveAction)
            alert.addAction(cancelAction)
            present(alert, animated: true, completion: nil)
        }
    }
    
    internal func didPinchCollectionView(gesture: UIPinchGestureRecognizer) {
        let pointInCollectionView: CGPoint = gesture.location(in: self.collectionView)
        let selectedIndexPath: IndexPath? = self.collectionView!.indexPathForItem(at: pointInCollectionView)
        
        if (posts[(selectedIndexPath?.item)!].isText) {
            return
        }
        
        let collectionViewCell = self.collectionView?.cellForItem(at: selectedIndexPath! as IndexPath) as! ChatViewPhotoCell
        let imageView = collectionViewCell.imageView as UIImageView
        let newImageView = UIImageView(image: imageView.image)
        newImageView.frame = self.view.frame
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleToFill
        newImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        
    }
    
    internal func didSingleTapCollectionView(gesture: UITapGestureRecognizer) {
        let pointInCollectionView: CGPoint = gesture.location(in: self.collectionView)
        let selectedIndexPath: IndexPath? = self.collectionView!.indexPathForItem(at: pointInCollectionView)
        
        if let postIndex = selectedIndexPath?.item {
            if (posts[postIndex].isText) {
                return
            }
            
            let collectionViewCell = self.collectionView?.cellForItem(at: selectedIndexPath! as IndexPath) as! ChatViewPhotoCell
            let imageView = collectionViewCell.imageView as UIImageView
            let newImageView = UIImageView(image: imageView.image)
            newImageView.frame = self.view.frame
            newImageView.backgroundColor = .black
            newImageView.contentMode = .scaleToFill
            newImageView.isUserInteractionEnabled = true
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
            newImageView.addGestureRecognizer(tap)
            self.view.addSubview(newImageView)
            
        } else {
            return
        }
    }
    
    internal func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    internal func didDoubleTapCollectionView(gesture: UITapGestureRecognizer) {
        let pointInCollectionView: CGPoint = gesture.location(in: self.collectionView)
        if self.collectionView!.indexPathForItem(at: pointInCollectionView) != nil {
            //_ = self.collectionView!.cellForItem(at: selectedIndexPath as IndexPath)!
            
            /*if ( posts[selectedIndexPath.item].isLocalUserLiked ) {
                posts[selectedIndexPath.item].isLocalUserLiked = false
                databaseAccess.decrementNumLikes(post: posts[selectedIndexPath.item])
            } else {
                posts[selectedIndexPath.item].isLocalUserLiked = true
                databaseAccess.incrementNumLikes(post: posts[selectedIndexPath.item])
            }*/
            
        } else {
            return
        }
        
    }
    
    internal func didTapSaveButton(save: Bool) {
        if (!save) {
            _ = databaseAccess.saveConversation(userID: Repo.sharedInstance.userID)
        } else {
            _ = databaseAccess.unsaveConversation(userID: Repo.sharedInstance.userID)
        }
        setIsSaved(isSaved: !save)
    }
    
    public func makePosts(posts: [Post]) {
        if posts.count == 0 {
            createNoMessagesLabel()
            return
        }
        
        for post in posts {
            
            databaseAccess.checkIsLiked(post: post)
            
            if (!self.uniquePosts.contains(post.postID)) {
                self.posts.append(post)
                self.uniquePosts.insert(post.postID)
            } else if let updatePost = self.posts.first(where: { $0.postID == post.postID }) {
                
                if (updatePost != post) {
                    updatePost.numLikes = post.numLikes
                    updatePost.numFlagged = post.numFlagged
                    updatePost.rating = post.rating
                }
            }
            
            if (!post.isText) {
                databaseAccess.addPhotoMessage(post: post)
            }
        }
        self.collectionView?.reloadData()
    }
    
    public func makePhotoPost(image: UIImage, post: Post) {
        imageDictionary[post.postID] = image
        self.sortPosts()
        self.collectionView?.reloadData()
    }
    
    public func makePhotoPostNotSet(post: Post) {
        imageDictionary[post.postID] = nil
        self.collectionView?.reloadData()
    }
    
    private func sortPosts() {
        self.posts.sort(by: {(post1: Post, post2: Post) -> Bool in
            return post1.postTime > post2.postTime
        })
        
    }
    
    public func setIsSaved(isSaved: Bool) {
        self.isSaved = isSaved
        if (self.isSaved) {
            headerView.setSavedImage()
        } else {
            headerView.setUnsavedImage()
        }
    }
    
    public func updateNumParticipants(num: Int) {
        numParticipants = num
        headerView.numParticipants.text = num.description
        self.checkIsSaved()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func setPostIsLocalUserLiked(post: Post) {
        //post.isLocalUserLiked = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let post = posts[indexPath.item]
        
        if (post.isText) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: textReuseIdentifier, for: indexPath) as! ChatViewTextCell
            cell.contentLabel.text = post.content
            cell.locationLabel.text = post.userLocationString
            cell.numLikesLabel.text = post.numLikes.description
            /*if (post.isLocalUserLiked) {
                cell.likedImage.image = UIImage(named: "like_filled.png")
            } else {
                cell.likedImage.image = UIImage(named: "like_empty.png")
            }*/
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: photoReuseIdentifier, for: indexPath) as! ChatViewPhotoCell
            if let image = imageDictionary[post.postID] {
                cell.activityIndicator.stopAnimating()
                cell.imageView.image = image!
                cell.locationLabel.text = post.userLocationString
                cell.numLikesLabel.text = post.numLikes.description
                /*if (post.isLocalUserLiked) {
                    cell.likedImage.image = UIImage(named: "like_filled.png")
                } else {
                    cell.likedImage.image = UIImage(named: "like_empty.png")
                }*/
                return cell
            } else {
                cell.activityIndicator.startAnimating()
                cell.locationLabel.text = post.userLocationString
                return cell
            }
        }

    }
    
    public func getImageDictionary() -> [String: UIImage?] {
        return imageDictionary
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */
    // Code from: http://www.appcoda.com/custom-pull-to-refresh/
    private func getNextColor() -> UIColor {
        var colorsArray: Array<UIColor> = [UIColor.magenta, UIColor.yellow, UIColor.red, UIColor.green, UIColor.blue, UIColor.orange]
        
        if currentColorIndex > colorsArray.count-1 {
            currentColorIndex = 0
        }
        
        let returnColor = colorsArray[currentColorIndex]
        currentColorIndex += 1
        
        return returnColor
    }
    
    private func animateRefreshStep1() {
        isAnimating = true
        
        UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: { () -> Void in
            self.labelsArray[self.currentLabelIndex].transform = CGAffineTransform(rotationAngle: CGFloat(M_PI_4))
            self.labelsArray[self.currentLabelIndex].textColor = self.getNextColor()
            
        }, completion: { (finished) -> Void in
            
            UIView.animate(withDuration: 0.05, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: { () -> Void in
                self.labelsArray[self.currentLabelIndex].transform = CGAffineTransform.identity
                self.labelsArray[self.currentLabelIndex].textColor = UIColor.black
                
            }, completion: { (finished) -> Void in
                self.currentLabelIndex += 1
                
                if self.currentLabelIndex < self.labelsArray.count {
                    self.animateRefreshStep1()
                }
                else {
                    self.animateRefreshStep2()
                }
            })
        })
    }
    
    func animateRefreshStep2() {
        UIView.animate(withDuration: 0.35, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: { () -> Void in
            self.labelsArray[0].transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.labelsArray[1].transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.labelsArray[2].transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.labelsArray[3].transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.labelsArray[4].transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            self.labelsArray[5].transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            
        }, completion: { (finished) -> Void in
            UIView.animate(withDuration: 0.25, delay: 0.0, options: UIViewAnimationOptions.curveLinear, animations: { () -> Void in
                self.labelsArray[0].transform = CGAffineTransform.identity
                self.labelsArray[1].transform = CGAffineTransform.identity
                self.labelsArray[2].transform = CGAffineTransform.identity
                self.labelsArray[3].transform = CGAffineTransform.identity
                self.labelsArray[4].transform = CGAffineTransform.identity
                self.labelsArray[5].transform = CGAffineTransform.identity
                
            }, completion: { (finished) -> Void in
                if self.refreshControl.isRefreshing {
                    self.currentLabelIndex = 0
                    self.animateRefreshStep1()
                }
                else {
                    self.isAnimating = false
                    self.currentLabelIndex = 0
                    for label in self.labelsArray {
                        label.textColor = UIColor.black
                        label.transform = CGAffineTransform.identity
                    }
                }
            })
        })
    }

    func endOfReloadingData() {
        refreshControl.endRefreshing()
        
        timer.invalidate()
        timer = nil
    }
    
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.collectionView?.reloadData()
        if refreshControl.isRefreshing {
            if !isAnimating {
                timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.endOfReloadingData), userInfo: nil, repeats: true)
                animateRefreshStep1()
            }
        }
    }
    
    func animateIn() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            self.collectionView?.backgroundColor = self.colViewBack
            self.view.backgroundColor = self.viewBack
            self.view!.center = CGPoint(x: self.screenWidth * 0.5, y: self.view!.center.y)
        },
                       completion: { finished in
                        self.isOnChat = true
                        
                        self.hideMessages = false
                        self.collectionView?.reloadData()
        })
    }
    
    private func swipeBubbles(swipe: CGFloat) {
        
        let savedConversations = Repo.sharedInstance.savedConversations
        let previousSelectedBubble = selectedConversationBubble
        var direction:CGFloat = 1.0
        
        if swipe > 0 {
            if selectedConversationBubble > 0 {
                selectedConversationBubble -= 1
                direction = 1.0
            } else {
                selectedConversationBubble = 0
            }
            
        } else if swipe < 0 {
            if selectedConversationBubble < savedConversations.count-1 {
                selectedConversationBubble += 1
                direction = -1.0
            } else {
                selectedConversationBubble = savedConversations.count-1
            }
        }
        
        if previousSelectedBubble != selectedConversationBubble {
            
            for index in 0...bubbleImageViews.count-1 {
                
                if index == previousSelectedBubble {
                    UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
                        self.bubbleImageViews[previousSelectedBubble].transform = CGAffineTransform(scaleX: CGFloat(2.0/3.0), y: CGFloat(2.0/3.0))
                        
                    }, completion: nil)
                } else if index == selectedConversationBubble {
                    UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
                        self.bubbleImageViews[self.selectedConversationBubble].transform = CGAffineTransform(scaleX: CGFloat(1.5), y: CGFloat(1.5))
                    }, completion: nil)
                }
                
                UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
                    self.bubbleImageViews[index].transform = CGAffineTransform(translationX: (direction * self.bubbleImageViews[index].frame.size.width/2.0) + (direction * 20.0), y: 0.0)
                }, completion: nil)
            }
            
            
            
            
            
            /*
            for view in bubbleImageViews {
                if swipe < 0 {
                    UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
                        view.transform = CGAffineTransform(translationX: -view.frame.size.width/2.0 - 20.0, y: 0.0)
                    }, completion: nil)
                } else if swipe > 0 {
                    UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
                        view.transform = CGAffineTransform(translationX: view.frame.size.width/2.0 + 20.0, y: 0.0)
                    }, completion: nil)
                }
            }*/
            
            loadNewConversation()
        }
        
        
        
        
    }
    
    private func loadNewConversation() {
        let savedConversations = Repo.sharedInstance.savedConversations
        let newConversation = savedConversations[selectedConversationBubble]
        
        self.posts = newConversation.posts
        self.conversationTitle = newConversation.title
        self.conversationID = newConversation.conversationID
        self.databaseAccess = NewChatViewDatabaseAccess(chatView: self, conversationID: newConversation.conversationID)
        self.checkIsSaved()
        self.databaseAccess.observeMessages()
        self.databaseAccess.observeNumParticipants()
        self.collectionView?.reloadData()
        
    }
    
    // Switching between camera and conversation view
    func handlePan(recognizer: UIPanGestureRecognizer) {
        
        if isFromBubble {
            
            if recognizer.state == .ended {
                let location = recognizer.location(in: self.view!)
                if location.y > 620.0{
                    
                    let translation = recognizer.translation(in: self.view!)
                    swipeBubbles(swipe: translation.x)
                    return
                    
                }
                
            }
            
            if recognizer.state == .began || recognizer.state == .changed {
                
                let location = recognizer.location(in: self.view!)
                if location.y > 620.0 {
                    return
                }
            }
        }
        
        if (recognizer.state == UIGestureRecognizerState.ended) {
            if (self.view!.center.x <= 0) {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.collectionView?.backgroundColor = UIColor.clear
                    self.view.backgroundColor = UIColor.clear
                    self.view!.center = CGPoint(x: self.screenWidth * -0.5, y: self.view!.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth/2 , y: self.tabBar.center.y)},
                               completion: { finished in
                                self.isOnChat = false
                                self.disableSwipe()
                                self.hideMessages = true
                                self.collectionView?.reloadData()
                })
            }
            else {
                captureSession.stopRunning()
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.view!.center = CGPoint(x: UIScreen.main.bounds.width * 0.5, y: self.view!.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth * 1.5 , y: self.tabBar.center.y)
                },
                               completion: { finished in
                                self.isOnChat = true
                                self.enableSwipe()
                                self.hideMessages = false
                                self.collectionView?.reloadData()
                })
            }
        }
        
        if recognizer.state == .began || recognizer.state == .changed {
            let translation = recognizer.translation(in: self.view!)
            //print(translation.x)
            
            
            if (translation.x > 80) {
                if (captureSession.isRunning){
                    captureSession.stopRunning()
                }
                
                self.collectionView?.backgroundColor = colViewBack
                //chatVC.view.backgroundColor = chatVC.viewBack
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.view!.center = CGPoint(x: self.screenWidth * 0.5, y: self.view!.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth * 1.5 , y: self.tabBar.center.y)
                },
                               completion: { finished in
                                self.isOnChat = true
                                self.enableSwipe()
                                self.hideMessages = false
                                self.collectionView?.reloadData()
                })
            }
            else if (translation.x < -80) {
                
                DispatchQueue.global(qos: .background).async {
                    _ = StartCamera(captureSession: self.captureSession)
                }
                
                self.isOnChat = false
                self.disableSwipe()
                
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.collectionView?.backgroundColor = UIColor.clear
                    self.view.backgroundColor = UIColor.clear
                    self.view!.center = CGPoint(x: self.screenWidth * -0.5, y: self.view!.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth/2 , y: self.tabBar.center.y)
                    },
                               completion: { finished in
                                self.hideMessages = true
                                self.collectionView?.reloadData()
                })
            }
            else if((translation.x < 0 || translation.x > 0 )
                && self.tabBar.center.x >= self.screenWidth * 0.5
                && (self.view?.center.x)! <= self.screenWidth * 0.5) {
                
                DispatchQueue.global(qos: .background).async {
                    _ = StartCamera(captureSession: self.captureSession)
                }
                
                self.view?.backgroundColor = self.colViewBack
                self.view.backgroundColor = self.colViewBack
                self.view!.center = CGPoint(x: self.view!.center.x + translation.x, y: self.view!.center.y)
                self.tabBar.center = CGPoint(x: self.tabBar.center.x + translation.x, y: self.tabBar.center.y)
                //print("here")
                recognizer.setTranslation(CGPoint.zero, in: self.view!)
            }
            
        }
    }
    
    func disableSwipe() {
        navController.interactivePopGestureRecognizer?.isEnabled = false
    }
    func enableSwipe() {
        navController.interactivePopGestureRecognizer?.isEnabled = true
    }
    
   
}

protocol CustomHeaderDelegate: class {
    func didTapSaveButton(save: Bool)
}



// MARK: - Private
private extension ChatViewCollectionViewController{
    
    func postForIndexPath(indexPath: IndexPath) -> Post {
        return posts[(indexPath as NSIndexPath).row]
    }
}

// MARK: - UICollectionViewDataSource
extension ChatViewCollectionViewController {
    
    
    override func collectionView(_ collectionView: UICollectionView,
                                 viewForSupplementaryElementOfKind kind: String,
                                 at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                             withReuseIdentifier: "ChatHeaderCollectionReusableView",
                                                                             for: indexPath) as! ChatHeaderCollectionReusableView
            self.headerView = headerView
            headerView.title.text = self.conversationTitle
            headerView.numParticipants.text = numParticipants.description
            headerView.isSaved = self.isSaved
            headerView.delegate = self
            return headerView
        default:
            assert(false, "Unexpected element kind")
        }
    }
}


