//
//  PostView.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 3/15/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Andrew Cunningham
 Last Updated: 15 March 2017
 Purpose: This View Controller displays the conversation screen. PostViews are placed on the VC.
 */

import UIKit

class PostView: UIView {
    
    var locationLabel = UILabel()
    var isLeftSide = true

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
