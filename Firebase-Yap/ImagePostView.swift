//
//  ImagePostView.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 3/15/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Andrew Cunningham
 Last Updated: 15 March 2017
 Purpose: This View Controller displays the conversation screen. PostViews are placed on the VC.
 */

import UIKit

class ImagePostView: PostView {
    
    var imageView = UIImageView()
    var activityIndicator = UIActivityIndicatorView()
    
    init() {
        super.init(frame: CGRect(x: 0.0, y: Constants.CONVERSATION_HEADER_HEIGHT, width: UIScreen.main.bounds.height / 4.0 + 10.0, height: UIScreen.main.bounds.width / 2.5))
        
        backgroundColor = UIColor.white
        layer.borderWidth = 2.0
        layer.borderColor = UIColor.white.cgColor
        layer.masksToBounds = false
        layer.cornerRadius = 5.0
        
        self.isUserInteractionEnabled = true
        
        locationLabel = UILabel(frame: CGRect(x: 3.0, y: frame.height - 23.0, width: frame.width - 10.0, height: 20.0))
        locationLabel.font = UIFont(name: "KohinoorBangla-Semibold", size: CGFloat(12))
        locationLabel.textAlignment = .right
        
        imageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: frame.width, height: frame.height - 23.0))
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0.0, y: 0.0, width: imageView.frame.width, height: imageView.frame.height))
        activityIndicator.hidesWhenStopped = true
        activityIndicator.stopAnimating()
        activityIndicator.color = UIColor.lightGray
        
        self.addSubview(locationLabel)
        self.addSubview(imageView)
        self.addSubview(activityIndicator)
        
        isUserInteractionEnabled = true
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap(gesture:))))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func handleTap(gesture: UITapGestureRecognizer) {
        
        if gesture.state == .began {
            layer.shadowOffset = CGSize(width: 0, height: 5)
            layer.shadowOpacity = 0.3
            layer.shadowRadius = 2
            
            let when = DispatchTime.now() + 0.1 // change to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.layer.shadowOffset = CGSize(width: 0, height: 0)
                self.layer.shadowOpacity = 0.0
                self.layer.shadowRadius = 0
            }
        }
        
        let fullScreenImageView = FullScreenImageView()
        if imageView.image != nil {
            fullScreenImageView.imageView.image = imageView.image
            self.superview?.superview?.superview?.addSubview(fullScreenImageView)
            UIView.animate(withDuration: 0.4, animations: {
                fullScreenImageView.bottomGradientView.frame.origin = CGPoint(x: 0.0, y: fullScreenImageView.frame.height - fullScreenImageView.bottomGradientView.frame.height)
                fullScreenImageView.topGradientView.frame.origin = CGPoint(x: 0.0, y: 0.0)
                fullScreenImageView.likeView.frame.origin = CGPoint(x: fullScreenImageView.frame.width/2.0 - 25.0, y: fullScreenImageView.bounds.height - 75.0)
            })
        }
        
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
