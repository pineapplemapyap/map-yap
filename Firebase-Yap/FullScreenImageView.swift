//
//  FullScreenImageView.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 3/15/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Andrew Cunningham
 Last Updated: 15 March 2017
 Purpose: This View Controller displays the conversation screen. PostViews are placed on the VC.
 */

import UIKit

class FullScreenImageView: UIView {
    
    var imageView = UIImageView()
    var topGradientView = UIView()
    var bottomGradientView = UIView()
    var borderIsHidden = false
    var localUserLiked = false
    var likeView = UIImageView()
    
    init() {
        super.init(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        backgroundColor = UIColor.black
        
        imageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
        imageView.frame.origin = CGPoint(x: self.frame.width/2.0 - imageView.frame.width/2.0, y: self.frame.height/2.0 - imageView.frame.height/2.0)
        
        likeView = UIImageView(frame: CGRect(x: self.frame.width / 2.0 - 25.0, y: self.frame.height, width: 50.0, height: 50.0))
        likeView.image = #imageLiteral(resourceName: "heartOff")
        likeView.isUserInteractionEnabled = true
        likeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleLikeButtonTap(gesture:))))
        
        self.addSubview(imageView)
        createGradientLayer()
        self.addSubview(likeView)
        isUserInteractionEnabled = true
        createGestureRecognizers()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func handleTap(gesture: UITapGestureRecognizer) {
        if borderIsHidden {
            borderIsHidden = false
            UIView.animate(withDuration: 0.4, animations: {
                self.bottomGradientView.frame.origin = CGPoint(x: 0.0, y: (self.superview?.bounds.height)! - self.bottomGradientView.frame.height)
                self.topGradientView.frame.origin = CGPoint(x: 0.0, y: 0.0)
                self.likeView.frame.origin.y -= 100.0
            })
        } else {
            borderIsHidden = true
            UIView.animate(withDuration: 0.4, animations: {
                self.bottomGradientView.frame.origin = CGPoint(x: 0.0, y: (self.superview?.bounds.height)! + self.bottomGradientView.frame.height)
                self.topGradientView.frame.origin = CGPoint(x: 0.0, y: -self.topGradientView.frame.height)
                self.likeView.frame.origin.y += 100.0
            })
        }
    }
    
    func handleSwipe(gesture: UISwipeGestureRecognizer) {
        let swipeGesture = gesture
        var directionX:CGFloat = 0.0
        var directionY:CGFloat = 0.0
        
        switch swipeGesture.direction {
        case UISwipeGestureRecognizerDirection.right:
            directionX = 2.0
        case UISwipeGestureRecognizerDirection.down:
            directionY = 2.0
        case UISwipeGestureRecognizerDirection.left:
            directionX = -1.0
        case UISwipeGestureRecognizerDirection.up:
            directionY = -1.0
        default:
            break
        }
        
        UIView.animate(withDuration: 0.3, animations: {
            self.center = CGPoint(x: self.bounds.width * directionX, y: self.bounds.height * directionY)
        }, completion: { (finished) in
            self.borderIsHidden = false
            self.isHidden = false
        })
    }
    
    func createGestureRecognizers() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(gesture:)))
        swipeRight.direction = .right
        self.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(gesture:)))
        swipeLeft.direction = .left
        self.addGestureRecognizer(swipeLeft)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(gesture:)))
        swipeUp.direction = .up
        self.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipe(gesture:)))
        swipeDown.direction = .down
        self.addGestureRecognizer(swipeDown)
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap(gesture:))))
    }
    
    func handleLikeButtonTap(gesture: UITapGestureRecognizer) {
        if !localUserLiked {
            localUserLiked = true
            likeView.image = #imageLiteral(resourceName: "heartOn")
        } else {
            localUserLiked = false
            likeView.image = #imageLiteral(resourceName: "heartOff")
        }
    }
    
    func createGradientLayer() {
        
        let gradientView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.bounds.width, height: self.bounds.height))
        topGradientView = UIView(frame: CGRect(x: 0.0, y: -100.0, width: self.bounds.width, height: 100.0))
        bottomGradientView = UIView(frame: CGRect(x: 0.0, y: self.bounds.height-100.0, width: self.bounds.width, height: 100.0))
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: CGSize(width: self.bounds.width, height: 100.0))
        gradientLayer.colors = [UIColor.black.cgColor, UIColor.clear.cgColor]
        topGradientView.layer.addSublayer(gradientLayer)
        
        let footerGradientLayer = CAGradientLayer()
        footerGradientLayer.frame = CGRect(origin: CGPoint(x: 0.0, y: 0.0), size: CGSize(width: self.bounds.width, height: 100.0))
        footerGradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        bottomGradientView.layer.addSublayer(footerGradientLayer)
        gradientView.addSubview(topGradientView)
        gradientView.addSubview(bottomGradientView)
        
        self.addSubview(gradientView)
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
