//
//  ChatViewTextCell.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 2/22/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import UIKit

class ChatViewTextCell: UICollectionViewCell {

    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var likedImage: UIImageView!
    @IBOutlet weak var numLikesLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
