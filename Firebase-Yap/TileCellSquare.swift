//
//  TileCell.swift
//  Firebase-Yap
//
//  Created by Tyler Mulley on 2/16/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Tyler Mulley
 Last Updated: 3 February 2017
 Purpose: This file is for a single tile
 */

import UIKit

class TileCellSquare: UICollectionViewCell {
    
    @IBOutlet weak var tileImageView: UIImageView!
    @IBOutlet weak var eyeImageView: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numParticipantsLabel: UILabel!
    
    var originalCenter : CGPoint = CGPoint()
    var swiped : Bool = false
    var row : Int = 0
    var index : IndexPath = IndexPath()
    var activityIndicator = UIActivityIndicatorView()
    
}
