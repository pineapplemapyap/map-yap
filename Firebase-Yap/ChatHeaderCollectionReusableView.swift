//
//  ChatHeaderCollectionReusableView.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 2/17/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import UIKit

class ChatHeaderCollectionReusableView: UICollectionReusableView {
        
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var numParticipants: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    
    weak var delegate: CustomHeaderDelegate?
    var isSaved = false
    
    @IBAction func didTapSaveButton(_ sender: Any) {
        isSaved = !isSaved
        delegate?.didTapSaveButton(save: !isSaved)
    }
    
    public func setSavedImage() {
        isSaved = true
        saveButton.setImage(UIImage(named: "heartOn"), for: .normal)
    }
    
    public func setUnsavedImage() {
        isSaved = false
        saveButton.setImage(UIImage(named: "heartOff"), for: .normal)
    }
}
