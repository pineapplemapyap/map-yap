//
//  InputTabBarController.swift
//  Firebase-Yap
//
//  Created by Tyler Mulley on 1/31/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Tyler Mulley
 Last Updated: 2 February 2017
 Purpose: The InputTabBar VC has the tab bar for the 3 different tab tabs: Camera, Text, Gallery
 */

import UIKit

class InputTabBarController: UITabBarController {
    
    var conversationID = ""
    var conversationTitle = ""
    var navTitle = ""
    var posts = [Post]()
    var navBar = UINavigationBar()
    var navController = UINavigationController()
    var databaseAccess:InputTabBarDatabaseAccess!
    var isFromBubble = false
    var mapBubble = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Repo.sharedInstance.currentConversationID = self.conversationID
        // Do any additional setup after loading the view.
        let numControllers = self.viewControllers?.count
        for i in 0...numControllers! - 1 {
            if self.viewControllers?[i] is InputScreen {
                let cameraVC = self.viewControllers?[i] as! InputScreen
                cameraVC.conversationID = conversationID
                cameraVC.posts = posts
                cameraVC.navBar = navBar
                cameraVC.navController = self.navController
                cameraVC.conversationTitle = self.conversationTitle
                cameraVC.isFromBubble = self.isFromBubble
                cameraVC.mapBubble = self.mapBubble
            }
            if self.viewControllers?[i] is VideoVC {
                let videoVC = self.viewControllers?[i] as! VideoVC
                videoVC.conversationID = conversationID
                videoVC.navBar = navBar
            }
            if self.viewControllers?[i] is TextInputVC {
                let textVC = self.viewControllers?[i] as! TextInputVC
                textVC.conversationID = conversationID
                textVC.navBar = navBar
            }
            if self.viewControllers?[i] is GalleryVC {
                let galVC = self.viewControllers?[i] as! GalleryVC
                galVC.conversationID = conversationID
                galVC.navBar = navBar
            }
        }
        
        
        // Set nav title
        self.title = navTitle
        
        // Set up db access
        databaseAccess = InputTabBarDatabaseAccess(inputTabBarVC: self)
        
        // Set nav icons
        databaseAccess.isSavedConversation(userID: Repo.sharedInstance.userID)
        
        // Get the number of participants
        _ = databaseAccess.getNumParticipants()

    }
    
    func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
        //This method will be called when user changes tab.
        if (item.title == Constants.VIDEO) {
            let videoVC = self.viewControllers?[2] as! VideoVC
            videoVC.viewDidLoad()
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // Called by databse access
    public func setUpFavoritesIcon(isSaved: Bool) {
        
        var saveConvoIcon = UIImage()
        var iconPath = ""
        
        if (isSaved) { // Add favorites icon
            iconPath = Constants.LIKE_FILLED_ICON
        } else { // Add favorites icon
            iconPath = Constants.LIKE_EMPTY_ICON
        }
        
        saveConvoIcon = (UIImage(named: iconPath)?.withRenderingMode(.alwaysOriginal))!
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: saveConvoIcon, style: .plain, target: self, action: #selector(self.saveConversation))
    }
    
    public func updateNumParticipantsLabel(numParticipants:Int) {
        // Add number of viewers iconh
        let numParticipantsIcon = UIImage(named: Constants.EYE_ICON)?.withRenderingMode(.alwaysOriginal)
        let numberOfParticipantsIcon = UIBarButtonItem(image: numParticipantsIcon, style: .plain, target: self, action: nil)
        numberOfParticipantsIcon.isEnabled = false
        
        // Create the button for the number of partipants
        let numberOfParticipantsText = UIBarButtonItem(title: String(numParticipants), style: .plain, target: self, action: nil)
        numberOfParticipantsText.isEnabled = false
        numberOfParticipantsText.tintColor = UIColor.black
        
        self.navigationItem.rightBarButtonItems = [numberOfParticipantsText, numberOfParticipantsIcon]
    }
    
    func saveConversation(){
        // add conversationID to userID -> saved_conversations
        _ = databaseAccess.saveConversation(userID: Repo.sharedInstance.userID)
        
        // change icon to filled heart
        let saveConvoIcon = UIImage(named: Constants.LIKE_FILLED_ICON)?.withRenderingMode(.alwaysOriginal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: saveConvoIcon, style: .plain, target: self, action: #selector(unsaveConversation))
    }
    
    func unsaveConversation() {
        _ = databaseAccess.unsaveConversation(userID: Repo.sharedInstance.userID)
        
        // change icon back to empty heart
        let saveConvoIcon = UIImage(named: Constants.LIKE_EMPTY_ICON)?.withRenderingMode(.alwaysOriginal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: saveConvoIcon, style: .plain, target: self, action: #selector(saveConversation))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
