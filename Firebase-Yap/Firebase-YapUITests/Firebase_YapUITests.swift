//
//  Firebase_YapUITests.swift
//  Firebase-YapUITests
//
//  Created by Andrew Cunningham on 1/5/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
import XCTest
import Firebase
@testable import Firebase_Yap

class Firebase_YapUITests: XCTestCase {
    
    //var vc: ViewController!
    let timestamp = "admin: \(Date.init().description)"
    
    override func setUp() {
        super.setUp()
        
        FIRApp.configure()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        //let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        //vc = storyboard.instantiateInitialViewController() as! ViewController
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        //let ref = FIRDatabase.database().reference(withPath: "conversations")
        //let query = ref.queryOrdered(byChild: "title").queryEqual(toValue: timestamp)
   
    }
    
    func tapFirstCell(app: XCUIApplication) {
        let tableView = app.tables["ConversationTableVC"]
        //let tableView = app.tables.containing(.table, identifier: "ConversationTableVC")
        XCTAssert(tableView.exists)
        //XCTAssertTrue(tableView.cells.count > 0)
        
        let firstCell = tableView.cells.element(boundBy: 0)
        firstCell.tap()
        print("value \(firstCell)")
    }
    
    // Conversation screen (tile) tests
    func testLoadConversation() {
        let app = XCUIApplication()
        tapFirstCell(app: app)
    }
    
    func testCreateConversation() {
        let app = XCUIApplication()
        app.navigationBars["MapYap"].buttons["Add"].tap()
        
        let newConversationAlert = app.alerts["New Conversation"]
        newConversationAlert.collectionViews.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .textField).element.typeText(timestamp)
        newConversationAlert.buttons["Save"].tap()
        XCTAssert(XCUIApplication().tables.staticTexts[timestamp].exists)
    }
    
    // Post screen (conversation) tests
    func testTapFavorite() {
        let app = XCUIApplication()
        //tapFirstCell(app: app)
        app.tables.staticTexts["wallaby"].tap()
        
        let wallabyNavigationBar = app.navigationBars["wallaby"]
        ///XCTAssert(wallabyNavigationBar.buttons["like empty"].exists)
        wallabyNavigationBar.buttons["like empty"].tap()
        //XCTAssert(wallabyNavigationBar.buttons["like filled"].exists)
        wallabyNavigationBar.buttons["like filled"].tap()
    }
    
    func testNumViewersIconExists() {
        
        let app = XCUIApplication()
        app.tables["ConversationTableVC"].staticTexts["Chapel"].tap()
        
        let chapelNavigationBar = app.navigationBars["Chapel"]
        //XCTAssert(chapelNavigationBar.buttons["eye"].exists)
        chapelNavigationBar.buttons["eye"].tap()
        //XCTAssert(chapelNavigationBar.buttons["eye"].exists)
        chapelNavigationBar.buttons["1"].tap()
    }
    
    func testSwipeFromPostsToCovnersations() {
        
        let app = XCUIApplication()
        let chapelStaticText = app.tables["ConversationTableVC"].staticTexts["Chapel"]
        chapelStaticText.tap()
        app.collectionViews.containing(.cell, identifier:": media message").element.swipeRight()
    }
    
    func testSwipeFromPostsToCamera() {
        
        let app = XCUIApplication()
        app.tables["ConversationTableVC"].staticTexts["Chapel"].tap()
        
        //app.collectionViews.cells["<+0.00000000,+0.00000000> +/- 0.00m (speed -1.00 mps / course -1.00) @ 2/15/17, 12:28:33 AM Eastern Standard Time: it's cold"].tap()

        
        app.otherElements.containing(.navigationBar, identifier:"Chapel").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 0).children(matching: .collectionView).element.swipeRight()
    }
    
    // Camera tests
    func testCapturePicture() {
        
        let app = XCUIApplication()
        app.tables["ConversationTableVC"].staticTexts["Chapel"].tap()
        
        let mediaMessageCollectionView = app.collectionViews.containing(.cell, identifier:": media message").element
        mediaMessageCollectionView.swipeLeft()
        mediaMessageCollectionView.tap()
        
        let element2 = app.otherElements.containing(.navigationBar, identifier:"Chapel").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let element = element2.children(matching: .other).element(boundBy: 1)
        element.tap()
    }
    
    func testCameraCancel() {
        let app = XCUIApplication()
        app.tables["ConversationTableVC"].staticTexts["Chapel"].tap()
        
        let mediaMessageCollectionView = app.collectionViews.containing(.cell, identifier:": media message").element
        mediaMessageCollectionView.swipeLeft()
        mediaMessageCollectionView.tap()
        
        let element2 = app.otherElements.containing(.navigationBar, identifier:"Chapel").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        let element = element2.children(matching: .other).element(boundBy: 1)
        
        element.tap()
        app.images["x_icon.png"].tap()
    }
    
    func testCameraFlip() {
        let app = XCUIApplication()
        app.tables["ConversationTableVC"].staticTexts["Chapel"].tap()
        
        let mediaMessageCollectionView = app.collectionViews.containing(.cell, identifier:": media message").element
        mediaMessageCollectionView.swipeLeft()
        mediaMessageCollectionView.tap()

        app.images["flipCam.png"].tap()
    }
    
    func testTapText() {
        let app = XCUIApplication()
        app.tables["ConversationTableVC"].staticTexts["Chapel"].tap()
        
        let mediaMessageCollectionView = app.collectionViews.containing(.cell, identifier:": media message").element
        mediaMessageCollectionView.swipeLeft()
        mediaMessageCollectionView.tap()
        
        app.toolbars.buttons["Gallery"].tap()
    }
    
    func testTapGallery() {
        let app = XCUIApplication()
        app.tables["ConversationTableVC"].staticTexts["Chapel"].tap()
        
        let mediaMessageCollectionView = app.collectionViews.containing(.cell, identifier:": media message").element
        mediaMessageCollectionView.swipeLeft()
        mediaMessageCollectionView.tap()
        
        app.toolbars.buttons["Gallery"].tap()
    }
    
    // Text screen tests
    func testPostingText() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results
        
        let app = XCUIApplication()
        app.tables["ConversationTableVC"].staticTexts["Chapel"].tap()
        
        let mediaMessageCollectionView = app.collectionViews.containing(.cell, identifier:": media message").element
        mediaMessageCollectionView.swipeLeft()
        mediaMessageCollectionView.tap()
        
        let tabBarsQuery = app.tabBars
        tabBarsQuery.buttons["Text"].tap()
        
        //textView.typeText("hey")
        app.toolbars.buttons["POST"].tap()
    app.collectionViews.containing(.cell, identifier:"<+0.00000000,+0.00000000> +/- 0.00m (speed -1.00 mps / course -1.00) @ 2/15/17, 12:34:27 AM Eastern Standard Time: red box speaker today =(").element.tap()
    }
    
    func testTextToCamera() {
        let app = XCUIApplication()
        app.tables["ConversationTableVC"].staticTexts["Chapel"].tap()
        
        let mediaMessageCollectionView = app.collectionViews.containing(.cell, identifier:": media message").element
        mediaMessageCollectionView.swipeLeft()
        mediaMessageCollectionView.tap()
        
        let tabBarsQuery = app.tabBars
        tabBarsQuery.buttons["Text"].tap()
        tabBarsQuery.buttons["Camera"].tap()
    }
    
    // Gallery tests
    func testGalleryCancel() {
        let app = XCUIApplication()
        app.tables["ConversationTableVC"].staticTexts["Chapel"].tap()
        
        let mediaMessageCollectionView = app.collectionViews.containing(.cell, identifier:": media message").element
        mediaMessageCollectionView.swipeLeft()
        mediaMessageCollectionView.tap()
        
        let tabBarsQuery = app.tabBars
        tabBarsQuery.buttons["Gallery"].tap()
        app.navigationBars["Moments"].buttons["Cancel"].tap()
    }
    
    func testGalleryToCamera() {
        
        let app = XCUIApplication()
        app.tables["ConversationTableVC"].staticTexts["Chapel"].tap()
        
        let mediaMessageCollectionView = app.collectionViews.containing(.cell, identifier:": media message").element
        mediaMessageCollectionView.swipeLeft()
        mediaMessageCollectionView.tap()

        let tabBarsQuery = app.tabBars
        tabBarsQuery.buttons["Gallery"].tap()
        tabBarsQuery.buttons["Camera"].tap()
    }
    
    func testFavoriteIconOn() {
        
    }
}
*/
