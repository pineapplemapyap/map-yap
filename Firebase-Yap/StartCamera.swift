//
//  StartCamera.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 1/28/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Tyler Mulley
 Last Updated: 1 February 2017
 Purpose: This class starts the camera capture session
 */

import Foundation
import AVFoundation

// Starts the camera when it appears to user
class StartCamera {
    var cameraSession = AVCaptureSession()
    
    init(captureSession: AVCaptureSession) {
        cameraSession = captureSession
        cameraSession.startRunning()
    }
}
