//
//  Conversation.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 1/5/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Andrew Cunningham
 Last Updated: 2 Febraury 2017
 Purpose: The Conversation class contains all the information pertaining to the conversation.
 */

import Foundation
import Firebase
import MapKit

class Conversation: NSObject {
    
    // Reference to the Firebase Database
    let ref: FIRDatabaseReference?
    
    // ID of the conversation
    let conversationID: String
    
    // Location of the conversation
    let location: CLLocation
    
    // The ID of the user who started the conversation
    let ownerUserID: String
    
    // The title of the conversation (set by the owner)
    let title: String
    
    // The time the conversation was started by the owner
    let startTime: Date
    
    // The number of participants in the conversation (the number of users currently viewing the conversation)
    var numParticipants = 0
    
    // The number of active users in the conversation (the number of users currently posting in the conversation)
    var activeUsers = [String]()
    
    // The list of all the posts in the conversation
    var posts = [Post]()
    
    // The image that will be displayed on the tile screen for the conversation
    var tileImage: UIImage
    
    init(conversationID: String, location: CLLocation, ownerUserID: String, title: String, startTime: Date) {
        self.conversationID = conversationID
        self.location = location
        self.startTime = startTime
        self.ownerUserID = ownerUserID
        self.title = title
        self.ref = nil
        self.tileImage = UIImage()
    }
    
    init(snapshot: FIRDataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        
        conversationID = snapshot.key
        let locationData = snapshotValue["location"] as! [String:AnyObject]
        let lat = locationData["lat"] as! Double
        let lon = locationData["lon"] as! Double
        
        location = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lon))
        title = snapshotValue["title"] as! String
        ownerUserID = snapshotValue["owner_user_id"] as! String
        self.tileImage = UIImage()
        
        let startTimeData = snapshotValue["start_time"] as! String
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZZZZZ"
        startTime = dateFormatter.date(from: startTimeData)!
        
        numParticipants = snapshotValue["num_participants"] as! Int
        
        var newPost = Post()
        
        if let postsData = snapshotValue["posts"] as? [String:AnyObject] {
            for (postID, data) in postsData {
                
                let dataDict = data as! [String:AnyObject]
                
                let locationData = snapshotValue["location"] as! [String:AnyObject]
                let lat = locationData["lat"] as! Double
                let lon = locationData["lon"] as! Double
                let postLocation = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lon))
                
                newPost = Post(content: dataDict["content"]! as! String,
                               userID: dataDict["user_id"] as! String,
                               time: dateFormatter.date(from: dataDict["post_time"] as! String)!,
                               postID: postID,
                               numLikes: dataDict["num_likes"] as! Int,
                               rating: dataDict["rating"] as! Double,
                               userLocation: postLocation,
                               isText: dataDict["is_text"] as! Bool,
                               conversationID: conversationID)
                
                posts.append(newPost)
            }
        }
        
        ref = snapshot.ref
    }
    func toAnyObject() -> Any {
        return [
            "title": title,
            "owner_user_id": ownerUserID,
            "num_participants" : numParticipants,
            "location" : location.toAnyObject(),
            "posts" : "", // pushing no posts because conversation just started
            "start_time" : startTime.description,
        ]
    }
}
