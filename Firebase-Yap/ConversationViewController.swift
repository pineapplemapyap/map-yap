//
//  ConversationViewController.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 3/15/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Andrew Cunningham
 Last Updated: 15 March 2017
 Purpose: This View Controller displays the conversation screen. PostViews are placed on the VC.
 */

import UIKit
import Photos

class ConversationViewController: UIViewController, UIScrollViewDelegate {
    
    // Outlets
    @IBOutlet weak var eyeImageView: UIImageView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var saveButton: UIImageView!
    @IBOutlet weak var numParticipantsLabel: UILabel!
    @IBOutlet weak var conversationTitleLabel: UILabel!

    // Properties:
    // Gets the number of particpants in the conversation from the database
    fileprivate var numParticipants = 100 {
        didSet {
            if numParticipants > 999 {
                numParticipantsString = convertToString(num: numParticipants)
            } else {
                numParticipantsString = numParticipants.description
            }
        }
    }
    
    // String of the variable numPrticipants that is set to the label to display to the user
    fileprivate var numParticipantsString = "100" {
        didSet {
            numParticipantsLabel.text = numParticipantsString
        }
    }
    
    // Used to determine which icon should be displayed for the conversation
    fileprivate var conversationIsSaved = false {
        didSet {
            if conversationIsSaved {
                saveButton.image = UIImage(named: Constants.LIKE_FILLED_ICON)
            } else {
                saveButton.image = UIImage(named: Constants.LIKE_EMPTY_ICON)
            }
            
        }
    }
    
    // Title of the conversation, display on the header bar
    var conversationTitle = ""
    
    // Databse; databaseAccess is the class with all the interaction to the database
    fileprivate var databaseAccess:ConversationDatabaseAccess!
    var conversationID:String = ""
    fileprivate var savedConversations = Repo.sharedInstance.savedConversations
    
    // Dictionary to store all the images for this conversation
    private(set) var imageDictionary = [String: UIImage]()
    fileprivate var displayDictionary = [String: Bool]()
    
    // Dictionary to store all the posts for this conversation
    fileprivate var postDictionary = [String: ImagePostView]()
    fileprivate var postViewsDisplaying = [PostView]()
    
    // Determines if the posts align with the left side of the screen
    fileprivate var isLeftSide = true
    
    // Size scroll view content size
    fileprivate var contentSizeStart = CGSize()
    
    // Determines if this VC was called from the saved conversations bubble on the map
    var isFromBubble = false
    
    // Array storing the saved conversations
    var savedConversationBubbles = [SavedConversationBubble]()
    
    // Array storing the posts for this conversation
    var posts = [Post]()
    var uniquePosts = Set<String>()
    
    // Determines if the
    var isOnChat = false
    
    // Determines if
    var isMessagesHidden = false
    
    // Background color for the VC
    var backgroundColor = UIColor()
    
    // For camera
    var captureSession = AVCaptureSession()
    var navController = UINavigationController()
    var tabBar : UITabBar = UITabBar()
    var inputScreen : CameraViewController? = nil
    
    // For swiping back to tile or to camera
    let screenWidth = UIScreen.main.bounds.width
    var panGestureRecognizer : UIPanGestureRecognizer = UIPanGestureRecognizer()
    var bubbleMarginY = CGFloat()
    var isInitialOpen = true
    
    // For tracking the conversation bubbles on the screen
    var bubbleDistance:CGFloat = 0.0
    var selectedConversationBubble = 0
    var previousSelectedBubble = 0
    
    // Refernce to NoMessagesLabel
    private var noMessagesLabel = UILabel()
    
    var mapBubble = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headerView.backgroundColor = Constants.BLUE
        // Setup the VC
        self.runSetups()
        self.scrollView.delegate = self
        self.contentSizeStart = CGSize(width: self.view.frame.width, height: 0.0)// Constants.CONVERSATION_HEADER_HEIGHT)
        self.scrollView.contentSize = self.contentSizeStart
        self.scrollView.alwaysBounceHorizontal = false
        self.scrollView.alwaysBounceVertical = true
        self.headerView.layer.zPosition = 1
        self.saveButton.layer.zPosition = 1
        self.headerView.isUserInteractionEnabled = false
        self.conversationTitleLabel.text = conversationTitle
        self.backgroundColor = self.view.backgroundColor!
        self.saveButton.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:))))
        let bubble = SavedConversationBubble()
        bubbleMarginY = (self.view.frame.height) - bubble.frame.height - Constants.BUBBLE_SNAP_DISTANCE
        
        let swipeRightGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.dismissConversation))
        swipeRightGestureRecognizer.direction = UISwipeGestureRecognizerDirection.right
        self.scrollView.addGestureRecognizer(swipeRightGestureRecognizer)

        
        // Add the pan gesture
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(recognizer:)))
        panGestureRecognizer.require(toFail: swipeRightGestureRecognizer)
        self.view.addGestureRecognizer(panGestureRecognizer)
        
        self.addEditBubble(bubble: bubble)
        self.addBackBubble(bubble: bubble)
    }
    
    func dismissConversation() {
        print("disCon")
        UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 5.0, initialSpringVelocity: 8.0, options: .curveEaseIn, animations: {
            self.inputScreen?.view.frame.origin.x = self.view.frame.width
            self.mapBubble.frame.origin.x += self.mapBubble.frame.width
        }, completion: { finished in
            self.inputScreen?.view.removeFromSuperview()
        })
    }
    
    func addEditBubble(bubble: Bubble) {
        let editBubble = IconBubble()
        editBubble.setImage(image: UIImage(named: "clearView.png")!, color: Constants.TEAL_BLUE)
        editBubble.setupViewController(conversationVC: self)
        let editWidth = editBubble.frame.width/1.5
        let editHeight = editBubble.frame.height/1.5
        let editImageView = UIImageView(frame: CGRect(x: (bubble.frame.width - editWidth)/2.0 + 2.0, y: (bubble.frame.height - editHeight)/2.0 - 7.0, width: editWidth, height: editHeight))
        editImageView.image = #imageLiteral(resourceName: "pencil-5")
        editImageView.clipsToBounds = true
        editBubble.addSubview(editImageView)
        self.view.addSubview(editBubble)
        editBubble.center = CGPoint(x: self.view.frame.width - editBubble.frame.width / 2.0, y: self.view.frame.height/2.0)
    }
    
    func addBackBubble(bubble: Bubble) {
        let backBubble = IconBubble()
        backBubble.setImage(image: UIImage(named: "clearView.png")!, color: Constants.TEAL_BLUE)
        backBubble.setupViewController(conversationVC: self)
        backBubble.isBack = true
        let backWidth = backBubble.frame.width/1.5
        let backHeight = backBubble.frame.height/1.5
        let backImageView = UIImageView(frame: CGRect(x: (bubble.frame.width - backWidth)/2.0 + 2.0, y: (bubble.frame.height - backHeight)/2.0 - 7.0, width: backWidth, height: backHeight))
        backImageView.image = #imageLiteral(resourceName: "chevron-left")
        backImageView.clipsToBounds = true
        backBubble.addSubview(backImageView)
        self.view.addSubview(backBubble)
        backBubble.center = CGPoint(x: backBubble.frame.width/2.0, y: self.view.frame.height/2.0)
    }
    
    func openInputScreen() {
        /*DispatchQueue.global(qos: .background).async {
            _ = StartCamera(captureSession: self.captureSession)
        }*/
        
        inputScreen?.cameraView.session?.startRunning()
        
        self.disableSwipe()
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
            self.view.backgroundColor = UIColor.clear
            self.view!.center = CGPoint(x: self.screenWidth * -0.5, y: self.view!.center.y)
            self.tabBar.center = CGPoint(x: self.screenWidth/2 , y: self.tabBar.center.y)
        },
                       completion: { finished in
                        self.isOnChat = false
                        self.isMessagesHidden = true
                        self.showNewPosts()
                        self.hideBubbles()
        })
    }
    
    func openTileScreen() {
        self.dismissConversation()
    }
    
    private func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0
        }
    }

    
    // Setup the VC
    public func runSetups() {
        
        self.conversationTitleLabel.text = self.conversationTitle
        self.scrollView.contentOffset.y = 0
        
        self.databaseAccess = ConversationDatabaseAccess(vc: self)
        Repo.sharedInstance.currentConversationID = self.conversationID
        
        if isFromBubble {
            self.createSavedConversationBubbles()
        }
        
        self.updateHeartIcon()
        self.updateNumParticipants()
        self.databaseAccess.observeNumParticipants()
        self.databaseAccess.observeMessages()
    }
    
    
    
    /* START: Saved Conversation */

    // Tapping on the saved conversation buton
    func handleTap(recognizer: UITapGestureRecognizer) {
        
        let location = recognizer.location(in: self.view)
        if (location.x > saveButton.frame.origin.x - 10.0 && location.x < saveButton.frame.origin.x + saveButton.frame.width + 10.0 && location.y > saveButton.frame.origin.y - 10.0 && location.y < saveButton.frame.origin.y + saveButton.frame.height + 10.0)
        {
            conversationIsSaved = !conversationIsSaved
            databaseAccess.updateSaveConversation(isSaved: conversationIsSaved)
        }
    }
    
    // update the saved conversations in the databse after tapping on the button
    public func updateSavedConversations() {
        savedConversations = Repo.sharedInstance.savedConversations
        //createSavedConversationBubbles()
        
        if savedConversations.count > 0 {
            selectedConversationBubble = 0
            if isFromBubble {
                loadNewConversation(conversationID: (savedConversations.first?.conversationID)!)
            }
        } else {
            bringInMapView() // doesn't actually bring in the map view
        }
        
    }
    
    // Update the number of participants
    public func updateNumParticipants(value: Int) {
        numParticipants = value
    }
    
    // Update the saved Conversations button depnding on whether the conversation was saved by the user or not
    private func updateHeartIcon() {
        if isFromBubble {
            conversationIsSaved = true
        }
        
        if savedConversations.contains(where: { (conversation) in
            return self.conversationID == conversation.conversationID
        }) {
            conversationIsSaved = true
        } else {
            conversationIsSaved = false
        }
    }
    
    // Update the number of participants in the database
    private func updateNumParticipants() {
        databaseAccess.incrementNumParticipants()
    }
    
    // Decrease the number of participants when the user leaves the conversation
    public func decrementNumParticipants() {
        databaseAccess.decrementNumParticipants()
    }
    /* END: Saved Conversation */
    
    // Navigate back to the map
    private func bringInMapView() {
        let viewControllers: [UIViewController] = self.navController.viewControllers
        
        for aViewController in viewControllers {
            if(aViewController is MapViewController){
                self.navController.popToViewController(aViewController, animated: true);
            }
        }
    }

    // Create each unique post
    public func makePosts(posts: [Post]) {
        if posts.count == 0 {
            createNoMessagesLabel()
            return
        } else {
            self.noMessagesLabel.removeFromSuperview()
        }
        
        for post in posts {
            
            //databaseAccess.checkIsLiked(post: post)
            
            if (!self.uniquePosts.contains(post.postID)) {
                self.posts.append(post)
                self.uniquePosts.insert(post.postID)
            } else if let updatePost = self.posts.first(where: { $0.postID == post.postID }) {
                
                if (updatePost != post) {
                    updatePost.numLikes = post.numLikes
                    updatePost.numFlagged = post.numFlagged
                    updatePost.rating = post.rating
                }
            }
            
            if (!post.isText) {
                databaseAccess.addPhotoMessage(post: post)
            }
        }
        // reload data here
        showNewPosts()
    }
    
    // Create the photo posts that have not been created yet
    public func makePhotoPostNotSet(post: Post) {
        showNewPosts()
    }
    
    // Create a photo post from an image and post
    public func makePhotoPost(image: UIImage, post: Post) {
        imageDictionary[post.postID] = image
        
        if let postImageView = postDictionary[post.postID] {
            postImageView.activityIndicator.stopAnimating()
            postImageView.imageView.image = image
        }
    }
    
    // Create a no messages label for when there are no posts
    private func createNoMessagesLabel() {
        // say No Messages!
        let label = UILabel(frame: CGRect(x: self.scrollView.frame.width / 2.0 - self.scrollView.frame.width / 6.0, y: self.scrollView.frame.height/2.0 - self.scrollView.frame.height / 2.5, width: self.scrollView.frame.width / 2.0, height: self.scrollView.frame.height / 5.0))
        label.font = UIFont(name: "KohinoorBangla-Semibold", size: CGFloat(30))
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        label.text = "No posts yet. Start the conversation!"
        label.textColor = UIColor(colorLiteralRed: 0.1, green: 0.1, blue: 0.1, alpha: 0.7)
        label.center.x = self.scrollView.center.x
        
        self.noMessagesLabel = label
        
        self.scrollView.addSubview(noMessagesLabel)
    }
    
    // Convert the given number to a string to display on a label
    private func convertToString(num: Int) -> String {
        var newNum = num
        var suffix = ""
        
        if newNum < 1000000 {
            newNum /= 1000
            suffix = "k"
        } else {
            newNum /= 1000000
            suffix = "m"
        }
        
        return newNum.description + suffix
    }
    
    public func reloadData() {
        
        imageDictionary = [String: UIImage]()
        displayDictionary = [String: Bool]()
        postDictionary = [String: ImagePostView]()
        postViewsDisplaying = [PostView]()
        savedConversations = Repo.sharedInstance.savedConversations
    }
    
    // Show the new posts
    public func showNewPosts() {
        
        for post in posts {
            
            if !post.isText {
                if displayDictionary[post.postID] == nil {
                    createImagePostView(post: post)
                }
            } else {
                if displayDictionary[post.postID] == nil {
                    createTextPostView(post: post)
                }
            }
        }
    }
    
    // Create the saved conversation bubbles at the bottom of the screen
    private func createSavedConversationBubbles() {
        
        if savedConversationBubbles.count == savedConversations.count {
            return
        }
        
        if savedConversations.count == 0 {
            return
        }
        
        for view in self.view.subviews {
            if view is SavedConversationBubble {
                view.removeFromSuperview()
            }
        }
        
        for index in 0...savedConversations.count-1 {
            
            let bubble = SavedConversationBubble()
            bubble.setImage(image: #imageLiteral(resourceName: "heart-2"), color: Constants.RED)
            bubble.setupViewController(conversationVC: self)
            bubble.conversationID = savedConversations[index].conversationID
            
            self.view.addSubview(bubble)
            bubble.center = CGPoint(x: self.view.frame.width / 2.0 + bubbleDistance*CGFloat(index), y: self.view.frame.height - bubble.frame.width/2.0 -  Constants.BUBBLE_SNAP_DISTANCE)
            bubbleDistance = bubble.frame.width / 2.0 + 60.0
            savedConversationBubbles.append(bubble)
        }
    }
    
    // Create an image post
    private func createImagePostView(post: Post) {
        
        let newImagePostView = ImagePostView()
        
        if let image = imageDictionary[post.postID] {
            newImagePostView.imageView.image = image
            
            
        } else {
            newImagePostView.activityIndicator.startAnimating()
        }
        
        newImagePostView.locationLabel.text = post.userLocationString
        newImagePostView.isLeftSide = isLeftSide
        
        
        self.scrollView.addSubview(newImagePostView)
        self.updateScrollView()
        displayDictionary[post.postID] = true
        postDictionary[post.postID] = newImagePostView
        
        let viewDestination = self.placeNewViewAtTop()
        
        postViewsDisplaying.append(newImagePostView)
        
        newImagePostView.frame.origin = CGPoint(x: self.view.frame.width/2.0, y: self.view.frame.height)
        
        moveViewsDown(newPost: newImagePostView)
        
        if isInitialOpen {
            UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseIn, animations: {
                newImagePostView.frame.origin = CGPoint(x: viewDestination.x, y: viewDestination.y)
            }, completion: nil)
        } else {
            newImagePostView.frame.origin = CGPoint(x: viewDestination.x, y: viewDestination.y)
        }
        
    }
    
    // Create a text post
    private func createTextPostView(post: Post) {
        let newTextPostView = TextPostView()
        newTextPostView.contentLabel.text = post.content
        newTextPostView.locationLabel.text = post.userLocationString
        newTextPostView.isLeftSide = isLeftSide
        
        self.scrollView.addSubview(newTextPostView)
        self.updateScrollView()
        displayDictionary[post.postID] = true
        
        let viewDestination = self.placeNewViewAtTop()
        
        postViewsDisplaying.append(newTextPostView)
        
        newTextPostView.frame.origin = CGPoint(x: self.view.frame.width/2.0, y: self.view.frame.height)
        
        moveViewsDown(newPost: newTextPostView)
        
        if isInitialOpen {
            UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseIn, animations: {
                newTextPostView.frame.origin = CGPoint(x: viewDestination.x, y: viewDestination.y)
            }, completion: nil)
        } else {
            newTextPostView.frame.origin = CGPoint(x: viewDestination.x, y: viewDestination.y)
        }
    }
    
    // Determine where the next view should be placed on the screen
    private func placeNewViewAtTop() -> CGPoint {
        
        var destPoint = CGPoint()
        if isLeftSide {
            destPoint = CGPoint(x: Constants.CONVERSATION_POSTS_MARGIN, y: self.scrollView.frame.origin.y + Constants.CONVERSATION_POSTS_MARGIN)
        } else {
            //(postViewsDisplaying.last?.frame.width)! + 20.0
            destPoint = CGPoint(x: self.scrollView.frame.width - (postViewsDisplaying.last?.frame.width)! - Constants.CONVERSATION_POSTS_MARGIN, y: ((postViewsDisplaying.last?.frame.origin.y)! + Constants.CONVERSATION_POSTS_MARGIN))
        }
        
        return destPoint
    }
    
    //
    private func moveViewsDown(newPost: PostView) {
        
        for view in postViewsDisplaying {
            if view.isLeftSide == newPost.isLeftSide {
                UIView.animate(withDuration: 0.1, animations: {
                    view.frame.origin.y += (newPost.frame.height + Constants.CONVERSATION_POSTS_MARGIN)
                })
            }
        }
        
        isLeftSide = !isLeftSide
    }
    
    // Update the view wihile scrolling
    private func updateScrollView() {
        var contentRect = CGRect(x: 0.0, y: 0.0, width: self.view.frame.width, height: self.contentSizeStart.height)
        
        for view in self.scrollView.subviews {
            contentRect = contentRect.union(CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: 0, height: view.frame.height))
        }
        self.scrollView.contentSize = contentRect.size;
        
        // Workaround for a bug I can't figure out
        //self.scrollView.contentSize.height += 200.0
    }
    
    // Get the scroll from the user
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.panGestureRecognizer.velocity(in: self.scrollView).y < 0 {
            
            UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseOut, animations: {
                self.headerView.frame.size.height = 45.0
                //self.likeButtonImage.frame.origin.y = 15.0
                self.saveButton.frame.origin.y = 15.0
                self.numParticipantsLabel.frame.origin.y = 14.0
                self.eyeImageView.frame.origin.y = 20.0
                self.conversationTitleLabel.layer.opacity = 0.0
                self.headerView.layer.opacity = 0.5
                self.view.layoutIfNeeded()
            }, completion: { (finished) in
                //self.conversationTitleLabel.isHidden = true
            })
            
        } else if scrollView.panGestureRecognizer.velocity(in: self.scrollView).y > 0 {
            UIView.animate(withDuration: 0.4, delay: 0.0, options: .curveEaseOut, animations: {
                self.headerView.frame.size.height = 60.0
                //self.likeButtonImage.frame.origin.y = 30.0
                self.saveButton.frame.origin.y = 25.0
                self.numParticipantsLabel.frame.origin.y = 24.0
                self.eyeImageView.frame.origin.y = 30.0
                self.conversationTitleLabel.layer.opacity = 1.0
                self.headerView.layer.opacity = 1.0
            }, completion: { (finished) in
                //self.conversationTitleLabel.isHidden = false
            })
        }
    }
    
    // Animate the VC into view
    func animateIn() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            self.view.backgroundColor = self.backgroundColor
            self.view!.center = CGPoint(x: self.screenWidth * 0.5, y: self.view!.center.y)
        },
                       completion: { finished in
                        self.isOnChat = true
                        
                        self.isMessagesHidden = false
                        self.showNewPosts()
        })
    }
    
    // Switching between camera and conversation view
    func handlePan(recognizer: UIPanGestureRecognizer) {
        if isFromBubble {
            
            if recognizer.state == .ended {
                let location = recognizer.location(in: self.view!)
                if location.y > 500.0{
                    
                    let translation = recognizer.translation(in: self.view!)
                    swipeBubbles(swipe: translation.x)
                    return
                    
                }
                
            }
            
            if recognizer.state == .began || recognizer.state == .changed {
                
                let location = recognizer.location(in: self.view!)
                if location.y > 500.0 {
                    return
                }
            }
        }
        
        if (recognizer.state == UIGestureRecognizerState.ended) {
            if (self.view!.center.x <= 0) {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.view.backgroundColor = UIColor.clear
                    self.view!.center = CGPoint(x: self.screenWidth * -0.5, y: self.view!.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth/2 , y: self.tabBar.center.y)},
                               completion: { finished in
                                self.isOnChat = false
                                self.disableSwipe()
                                self.isMessagesHidden = true
                                self.showNewPosts()
                                self.hideBubbles()
                })
            }
            else {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.view!.center = CGPoint(x: UIScreen.main.bounds.width * 0.5, y: self.view!.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth * 1.5 , y: self.tabBar.center.y)
                },
                               completion: { finished in
                                self.captureSession.stopRunning()
                                self.isOnChat = true
                                self.enableSwipe()
                                self.isMessagesHidden = false
                                self.showNewPosts()
                                self.showBubbles()
                })
            }
        }
        
        if recognizer.state == .began || recognizer.state == .changed {
            let translation = recognizer.translation(in: self.view!)
            if (translation.x > 40) {
                self.view.backgroundColor = self.backgroundColor
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.view!.center = CGPoint(x: self.screenWidth * 0.5, y: self.view!.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth * 1.5 , y: self.tabBar.center.y)
                },
                               completion: { finished in
                                self.captureSession.stopRunning()
                                self.isOnChat = true
                                self.enableSwipe()
                                self.isMessagesHidden = false
                                self.showNewPosts()
                                self.hideBubbles()
                })
            }
            else if (translation.x < -40) {
                
                DispatchQueue.global(qos: .background).async {
                    _ = StartCamera(captureSession: self.captureSession)
                }
                
                
                self.disableSwipe()
                
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.view.backgroundColor = UIColor.clear
                    self.view!.center = CGPoint(x: self.screenWidth * -0.5, y: self.view!.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth/2 , y: self.tabBar.center.y)
                },
                               completion: { finished in
                                self.isOnChat = false
                                self.isMessagesHidden = true
                                self.showNewPosts()
                                self.hideBubbles()
                })
            }
            else if((translation.x < 0 || translation.x > 0 )
                && self.tabBar.center.x >= self.screenWidth * 0.5
                && (self.view?.center.x)! <= self.screenWidth * 0.5) {
                
                DispatchQueue.global(qos: .background).async {
                    _ = StartCamera(captureSession: self.captureSession)
                }
                
                self.view?.backgroundColor = self.backgroundColor
                self.view.backgroundColor = self.backgroundColor
                self.view!.center = CGPoint(x: self.view!.center.x + translation.x, y: self.view!.center.y)
                self.tabBar.center = CGPoint(x: self.tabBar.center.x + translation.x, y: self.tabBar.center.y)
                recognizer.setTranslation(CGPoint.zero, in: self.view!)
            }
            
        }
    }
    
    // Disable the swipe to camera
    func disableSwipe() {
        navController.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    // Enable the swipe to camera
    func enableSwipe() {
        navController.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    // To handle the swiping of the bubbles
    private func swipeBubbles(swipe: CGFloat) {
     /*
         previousSelectedBubble = selectedConversationBubble
         
         if swipe > 0 {
            if selectedConversationBubble > 0 {
                selectedConversationBubble -= 1
            } else {
                selectedConversationBubble = 0
            }
         
         } else if swipe < 0 {
            if selectedConversationBubble < savedConversations.count-1 {
                selectedConversationBubble += 1
            }
            else {
                selectedConversationBubble = savedConversations.count-1
            }
        }
        
        if previousSelectedBubble != selectedConversationBubble {
            self.loadNewConversation(conversationID: savedConversations[selectedConversationBubble].conversationID)
        }
 */
    }
    
    // Reload for a new conversation
    private func loadNewConversation(conversationID: String) {
        
        var direction:CGFloat = 0.0
        if previousSelectedBubble < selectedConversationBubble {
            direction = -1.0
        } else {
            direction = 1.0
        }
        
        let numMoves = abs(previousSelectedBubble - selectedConversationBubble)
        
        if numMoves > 0 {
            for _ in 0...numMoves-1 {
                moveBubbles(direction: direction)
            }
        }
        
        previousSelectedBubble = selectedConversationBubble
        
        let newConversation = savedConversations[selectedConversationBubble]
        
        self.scrollView.subviews.forEach({ (view) in
            if view is PostView {
                view.removeFromSuperview()
            }
        })
        
        self.posts = newConversation.posts
        self.conversationTitle = newConversation.title
        self.conversationID = newConversation.conversationID
        
        self.reloadData()
        self.isInitialOpen = false
        self.isLeftSide = true
        self.runSetups()
    }
    
    // Determine if the bubble was tapped
    public func bubbleTapped(conversationID: String) {
        
        selectedConversationBubble = savedConversations.index(where: { (conversation) in
            return conversation.conversationID == conversationID
        })!
        
        if previousSelectedBubble != selectedConversationBubble {
            loadNewConversation(conversationID: conversationID)
        }
    }
    
    // Move the bubbles when tapped
    private func moveBubbles(direction: CGFloat) {
        for bubble in savedConversationBubbles {
            UIView.animate(withDuration: 0.1, animations: {
                bubble.center = CGPoint(x: bubble.center.x + self.bubbleDistance * direction, y: bubble.center.y)
            })
        }
    }
    
    private func hideBubbles() {
        for bubble in self.savedConversationBubbles {
            bubble.isHidden = true
        }
    }
    
    private func showBubbles() {
        for bubble in self.savedConversationBubbles {
            bubble.isHidden = false
        }
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func cropToBounds(image: UIImage, width: Double, height: Double) -> UIImage {
        
        let contextImage: UIImage = UIImage(cgImage: image.cgImage!)
        
        let contextSize: CGSize = contextImage.size
        
        var posX: CGFloat = 0.0
        var posY: CGFloat = 0.0
        var cgwidth: CGFloat = CGFloat(width)
        var cgheight: CGFloat = CGFloat(height)
        
        // See what size is longer and create the center off of that
        if contextSize.width > contextSize.height {
            posX = ((contextSize.width - contextSize.height) / 2)
            posY = 0
            cgwidth = contextSize.height
            cgheight = contextSize.height
        } else {
            posX = 0
            posY = ((contextSize.height - contextSize.width) / 2)
            cgwidth = contextSize.width
            cgheight = contextSize.width
        }
        
        let rect: CGRect = CGRect(x: posX, y: posY, width: cgwidth, height: cgheight)
        
        // Create bitmap image from context using the rect
        let imageRef: CGImage = contextImage.cgImage!.cropping(to: rect)!
        
        // Create a new image based on the imageRef and rotate back to the original orientation
        let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }

}
