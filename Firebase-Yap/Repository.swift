//
//  Repository.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 1/5/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Andrew Cunningham
 Last Updated: 2 Febraury 2017
 Purpose: The Repo class is a singleton class that can be used throughout the entire project
 */

import Foundation
import MapKit

class Repo {
    //var conversations = [Conversation]()
    
    static let sharedInstance: Repo = {
        let instance = Repo()
        
        return instance
    }()
    
    // When the app delegate runs, userID and userLocation are set, so they can be used throughout the entire project
    
    
    lazy var userLocation = CLLocation()
    var mapVC = MapViewController()
    var currentConversationID = String()
    var isInitialOpen = true
    var didLoginFail = false
    var errorMessage:String?
    
    var userID = "" {
        didSet {
            mapVC.databaseAccess.loadSavedConversations(userID: self.userID)
            if savedConversations.count == 0 {
                mapVC.hideSavedConversationBubble()
            }
        }
    }
    
    var savedConversations = [Conversation]() {
        didSet {
            if (oldValue.count == 0 && savedConversations.count == 1) {
                mapVC.savedConversationCountDidChange()
            } else if (savedConversations.count == 0) {
                mapVC.hideSavedConversationBubble()
            }
            
            if savedConversations.count > 0 && oldValue.count < savedConversations.count {
                RepoDatabaseAccess.observeSavedConversation(conversationID: (savedConversations.last?.conversationID)!)
            }
        }
    }
    
    var bubbleImageDictionary = [String:UIImage?]() {
        didSet {
            
        }
    }
    
}

extension CLLocation {
    func toAnyObject() -> Any {
        return [
            "lat" : self.coordinate.latitude,
            "lon" : self.coordinate.longitude
        ]
    }
}
