//
//  ShadowMaker.swift
//  sportsHype
//
//  Created by Andrew Cunningham on 11/13/16.
//  Copyright © 2016 Jessica Lewis. All rights reserved.
//

import UIKit
import Foundation

class ShadowMaker {
    
    init() {
        
    }
    
    static func removeShadowImage(parentview:UIView) {
        parentview.layer.shadowColor = UIColor.clear.cgColor
    }
    
    static func addShadowImage(parentview:UIView){
        
        
        parentview.layer.shadowColor = UIColor.clear.cgColor
        parentview.layer.masksToBounds = false
        
        //for bottom shadow on view
        parentview.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        parentview.layer.shadowOpacity = 0.7
        parentview.layer.shadowRadius = 1.0
        
        
        //for bottom and right sides shadow on view
        parentview.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        parentview.layer.shadowOpacity = 1
        parentview.layer.shadowRadius = 3.0
        
        /*
        //for empty shadow on view
        parentview.layer.shadowOffset = CGSize(width: 0, height: 0)
        parentview.layer.shadowOpacity = 1
        parentview.layer.shadowRadius = 0
        
        //for bottom and right and left sides shadow on view
        parentview.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        parentview.layer.shadowOpacity = 1
        parentview.layer.shadowRadius = 2.0
        
        //for four sides shadow on view
        parentview.layer.shadowOffset = CGSize(width: 0, height: 0)
        parentview.layer.shadowOpacity = 1.0
        parentview.layer.shadowRadius = 5.0
        */
    }
    
}
