//
//  MapViewController.swift
//  Firebase-Yap
//
//  Created by Guilherme Pereira on 2/16/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Guilherme Pereira and Connor Grumling
Last updated: 3/2/2017
Purpose: This class deals with the map
 */


import UIKit
import MapKit
import CoreLocation
import Firebase
import Foundation

extension Double {
    /// Rounds the double to decimal places value
    func roundTo(places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

class MapViewController: UIViewController, CLLocationManagerDelegate {
   
    // Outlets
    @IBOutlet weak var mapView: MKMapView!
    
    // Navigating back to map
    @IBAction func unwindToMap(segue:UIStoryboardSegue) { }
    
    // Rows and columns for grid
    let rows = 5
    let cols = 4
    
    // Determines if is the first location loaded
    var isFirstLocation = true
    
    // Map span
    var mapSpan = MKCoordinateSpan()
    var oldSpan = MKCoordinateSpan()
    // Increse this precision value (5,6,...) to see if the map refreshes when zooming in
    let precision = 6
    
    // Location manager
    let manager = CLLocationManager()
    
    // NorthWest Coordinate, NorthEast, SouthWest, SouthEast
    var nwCoord = CLLocationCoordinate2D()
    var neCoord = CLLocationCoordinate2D()
    var swCoord = CLLocationCoordinate2D()
    var seCoord = CLLocationCoordinate2D()
    
    // Grouped conversations
    var groupedLocations = [[Conversation]]()
    var conversationsToSend = [Conversation]()
    
    // Databse
    var databaseAccess:MapDatabaseAccess!
    
    var bubbleMarginY = CGFloat()
    

    override func viewDidLoad() {
        
        databaseAccess = MapDatabaseAccess.init(mapView: self)
        Repo.sharedInstance.mapVC = self
        if Repo.sharedInstance.isInitialOpen {
            databaseAccess.logIn()
        }
        
        if Repo.sharedInstance.didLoginFail {
            let alert = UIAlertController(title: "Oops", message: Repo.sharedInstance.errorMessage, preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(defaultAction)
            self.present(alert, animated: true, completion: nil)
        }
        
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
        
        // Group th conversations
        for _ in 1...rows*cols{
            groupedLocations.append([Conversation]())
        }
        
        super.viewDidLoad()
        
        // Set up
        self.navigationController?.navigationBar.isHidden = true
        
        
        manager.requestAlwaysAuthorization()
        
        self.mapView.showsUserLocation = true
        
        setupGestureRecognizer()
        mapView.delegate = self
        
        manager.delegate = self
        
        self.mapView.showsUserLocation = true
        
        
        let bubble = Bubble()
        bubbleMarginY = (self.view.frame.height) - bubble.frame.height - Constants.BUBBLE_SNAP_DISTANCE
        self.addStatusBar()
        self.addTrendingBubble()
        self.addLocationBubble()
        self.addNewConversationBubble()
    }
    
    // Add status bar since we cover it with VC
    private func addStatusBar() {
        let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: UIScreen.main.bounds.size.width, height: Constants.STATUS_BAR_HEIGHT))
        let statusBarColor = Constants.OFF_WHITE
        view.backgroundColor = statusBarColor
        self.view.addSubview(view)
    }
    
    // Style the status bar
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    // Add favorites bubble to map
    private func addLocationBubble(){
        let bubble = MapBubble()
        
        bubble.setImage(image: UIImage(named: "clearView.png")!, color: Constants.BLUE)
        let locationWidth = bubble.frame.width/1.5
        let locationHeight = bubble.frame.height/1.5
        let locationImageView = UIImageView(frame: CGRect(x: (bubble.frame.width - locationWidth)/2.0 - 3.0, y: (bubble.frame.height - locationHeight)/2.0 - 3.0, width: locationWidth, height: locationHeight))
        locationImageView.image = #imageLiteral(resourceName: "location-4")
        locationImageView.clipsToBounds = true
        bubble.addSubview(locationImageView)
        bubble.isLocation = true
        bubble.setMapViewController(mapVC: self)
        bubble.frame.origin = CGPoint(x: -100.0, y: bubbleMarginY)
        mapView.addSubview(bubble)
        
        UIView.animate(withDuration: 0.3, delay: 1.5, options: .curveEaseIn, animations: {
            bubble.frame.origin = CGPoint(x: self.mapView.frame.width / 2.0, y: self.bubbleMarginY)
        }, completion: nil)
    }
    
    // Add trending bubble to map
    private func addTrendingBubble() {
        let bubble = MapBubble()
        
        bubble.setImage(image: UIImage(named: "clearView.png")!, color: Constants.ORANGE)
        let fireWidth = bubble.frame.width/1.5
        let fireHeight = bubble.frame.height/1.5
        let fireImageView = UIImageView(frame: CGRect(x: (bubble.frame.width - fireWidth)/2.0 + 2.0, y: (bubble.frame.height - fireHeight)/2.0 - 5.0, width: fireWidth, height: fireHeight))
        fireImageView.image = #imageLiteral(resourceName: "fire-3")
        fireImageView.clipsToBounds = true
        bubble.addSubview(fireImageView)
        
        //bubble.setImage(image: #imageLiteral(resourceName: "fire-2"), color: Constants.BLUE)
        bubble.isStar = true
        bubble.setMapViewController(mapVC: self)
        bubble.frame.origin = CGPoint(x: -100.0, y: bubbleMarginY)
        mapView.addSubview(bubble)
        
        UIView.animate(withDuration: 0.3, delay: 1.5, options: .curveEaseIn, animations: {
            bubble.frame.origin = CGPoint(x: self.mapView.frame.width / 2.0 - bubble.frame.width, y: self.bubbleMarginY)
        }, completion: nil)
    }
    
    // add New Conversation Bubble
    private func addNewConversationBubble() {
        let bubble = MapBubble()
        bubble.isNewConversation = true
        
        bubble.setImage(image: UIImage(named: "clearView.png")!, color: Constants.TEAL_BLUE)
        let addNewWidth = bubble.frame.width/1.5
        let addNewHeight = bubble.frame.height/1.75
        let addNewImageView = UIImageView(frame: CGRect(x: (bubble.frame.width - addNewWidth)/2.0, y: (bubble.frame.height - addNewHeight)/2.0 - 5.0, width: addNewWidth, height: addNewHeight))
        addNewImageView.image = #imageLiteral(resourceName: "plus-2")
        bubble.addSubview(addNewImageView)
        bubble.setMapViewController(mapVC: self)
        bubble.frame.origin = CGPoint(x: -100.0, y: bubbleMarginY)
        mapView.addSubview(bubble)
        
        UIView.animate(withDuration: 0.3, delay: 1.5, options: .curveEaseIn, animations: {
            bubble.frame.origin = CGPoint(x: self.mapView.frame.width / 2.0 + (bubble.frame.width), y: self.bubbleMarginY)
        }, completion: nil)
    }
    
    func newConversationBubbleTapped() {
        let touchPoint = self.mapView.center
        newConversationAlertController(touchPoint: touchPoint)
    }
    
    // To get long presses on the map to add a conversation
    private func setupGestureRecognizer() {
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(didLongPressMapView(gesture:)))
        self.mapView.addGestureRecognizer(longPressRecognizer)
    }
    
    // Checks if the database saved conversation count changed for the conversation
    func savedConversationCountDidChange() {
        addSavedConversationBubble(title: (Repo.sharedInstance.savedConversations.last?.title)!)
    }
    
    // Gets if the user did a long tap, create new conversation alert box
    func didLongPressMapView(gesture: UILongPressGestureRecognizer) {
        
        let touchPoint = gesture.location(in: self.mapView)
        newConversationAlertController(touchPoint: touchPoint)
        
    }
    
    func newConversationAlertController(touchPoint: CGPoint) {
        let alert = UIAlertController(title: "Create New Conversation",
                                      message: "Enter a title.",
                                      preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Create",
                                       style: .default) { _ in
                                        
                                        guard let textField = alert.textFields?.first,
                                            let text = textField.text else { return }
                                        
                                        self.createConversationPin(touchPoint: touchPoint, title: text)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default)
        
        alert.addTextField{
            $0.placeholder = "Write something"
            $0.addTarget(self, action: #selector(self.textFieldTextDidChange(_:)), for: .editingChanged)
        }
        saveAction.isEnabled = false
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        present(alert, animated: true, completion: nil)
    }
    
    // Tests if the text field on the new conversation alert box changed
    func textFieldTextDidChange(_ textField: UITextField) {
        if let alert = presentedViewController as? UIAlertController,
            let action = alert.actions.last,
            let text = textField.text {
            action.isEnabled = text.characters.count > 0
        }
    }
    
    // Create the conversation at the point touched by the user
    func createConversationPin(touchPoint: CGPoint, title: String) {
        let coords = self.mapView.convert(touchPoint, toCoordinateFrom: self.mapView)
        let newConversation = databaseAccess.createNewConversation(location: coords, title: title)
        var conversations = [Conversation]()
        conversations.append(newConversation)
        let annotation = ConversationPin(title: newConversation.title, coordinate: newConversation.location.coordinate,conversations:  conversations)
        self.mapView.addAnnotation(annotation)
        var newAnnotation = [MKAnnotation]()
        newAnnotation.append(annotation)
        self.mapView.showAnnotations(newAnnotation, animated: true)
        
        //add newConversation to grid
        let deltaX = (neCoord.longitude - nwCoord.longitude)/Double(cols)
        let deltaY = (neCoord.latitude - seCoord.latitude)/Double(rows)
        let a = annotation.coordinate.longitude - swCoord.longitude
        let b = annotation.coordinate.latitude - swCoord.latitude
        let gridX = Int(a/deltaX)
        let gridY = Int(b/deltaY)
        groupedLocations[gridX + cols*gridY].append(newConversation)
    }

    // Update the location every so often
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        manager.startUpdatingLocation()
        manager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    // If cannot find the users location
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
    // If the locationmanager succussfully found the users location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        Repo.sharedInstance.userLocation = locations[0]
        
        mapSpan = self.mapView.region.span
        let north = self.mapView.centerCoordinate.latitude + mapSpan.latitudeDelta/2
        let south = self.mapView.centerCoordinate.latitude - mapSpan.latitudeDelta/2
        let east = self.mapView.centerCoordinate.longitude + mapSpan.longitudeDelta/2
        let west = self.mapView.centerCoordinate.longitude - mapSpan.longitudeDelta/2
        nwCoord = CLLocationCoordinate2DMake(north, west)
        neCoord = CLLocationCoordinate2DMake(north, east)
        swCoord = CLLocationCoordinate2DMake(south, west)
        seCoord = CLLocationCoordinate2DMake(south, east)
        
        if (!mapSpan.latitudeDelta.roundTo(places: precision).isEqual(to: oldSpan.latitudeDelta.roundTo(places: precision)) && !mapSpan.longitudeDelta.roundTo(places: precision).isEqual(to: oldSpan.longitudeDelta.roundTo(places: precision))){
            //self.mapView.removeAnnotations(self.mapView.annotations)
            databaseAccess.findConversationsAround(nwBound: nwCoord, seBound: seCoord)
        }
        oldSpan = mapSpan
        
        if isFirstLocation {
            let span:MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
            let myLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(locations[0].coordinate.latitude, locations[0].coordinate.longitude)
            let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        
            self.mapView.setRegion(region, animated: true)
            isFirstLocation = false;
        }
    }
    
    // Get all the locations
    func getLocations(convArr: [Conversation]){
        
        let mappedLocations = mapOldLocationsToNewLocations(convArr: convArr)
        
        for i in 0...rows*cols-1{
            groupedLocations[i].removeAll()
        }
        let deltaX = (neCoord.longitude - nwCoord.longitude)/Double(cols)
        let deltaY = (neCoord.latitude - seCoord.latitude)/Double(rows)
        for l in convArr{
            let a = l.location.coordinate.longitude - swCoord.longitude
            let b = l.location.coordinate.latitude - swCoord.latitude
            let gridX = Int(a/deltaX)
            let gridY = Int(b/deltaY)
            if ((0 <= gridX + cols*gridY) && (gridX + cols*gridY < 20)){
                groupedLocations[gridX + cols*gridY].append(l)
            }
        }
        
        //populate pins
        placePins(oldLocations: mappedLocations)
    }
    
    func mapOldLocationsToNewLocations(convArr: [Conversation]) -> [String:CLLocation]{
        var mappedLocations = [String:CLLocation]()
        
        for conversation in convArr {
            for i in 0...rows*cols-1 {
                for groupConvos in groupedLocations[i] {
                    if conversation.conversationID == groupConvos.conversationID {
                        mappedLocations[conversation.conversationID] = groupedLocations[i][0].location
                    }
                }
            }
        }
        
        return mappedLocations
    }
    
    // Sort the conversations by number of participants
    func sortByNumParticipant(conArray: [Conversation])->[Conversation]{
        return conArray.sorted(by: {(conv1: Conversation, conv2: Conversation) -> Bool in
            return conv1.numParticipants > conv2.numParticipants
        })
    }
    
    func removeAllPins() {
        self.mapView.removeAnnotations(self.mapView.annotations)
    }
    
    // Put pins on the map
    func placePins(oldLocations: [String:CLLocation]) {
        removeAllPins()
        
        for item in groupedLocations {
            if(!item.isEmpty){
                let sortedConv = sortByNumParticipant(conArray: item)
                if let oldLocation = assignOldLocationCoordinate(conversation: sortedConv[0], oldLocations: oldLocations) {
                    let annotation = ConversationPin(title: sortedConv[0].title, coordinate: oldLocation.coordinate, conversations: item)
                    self.mapView.addAnnotation(annotation)
                    
                     //self.mapView.addAnnotation(annotation)
                    UIView.animate(withDuration: 0.0, animations: {
                        
                        annotation.coordinate = sortedConv[0].location.coordinate
                        
                        //let view = self.mapView.view(for: annotation)
                        //view?.frame.origin = CGPoint(x: (view?.frame.origin.x)! + 100, y: (view?.frame.origin.y)! + 100)
                    }, completion: { (finished) in
                        //print("ANIMATION COMPLETED")
                    })
                } else {
                    let annotation = ConversationPin(title: sortedConv[0].title, coordinate: sortedConv[0].location.coordinate, conversations: item)
                    self.mapView.addAnnotation(annotation)
                }
                
            }
        }
    }
    
    func fromCoordToScreen(coord: CLLocationCoordinate2D) -> CGPoint{
        let x = Double(coord.longitude - swCoord.longitude)
        let xRatio = x/Double(mapSpan.longitudeDelta)
        let y = Double(coord.latitude - swCoord.latitude)
        let yRatio = 1.0-(y/Double(mapSpan.latitudeDelta))
        return CGPoint(x: xRatio * Double(self.mapView.frame.width), y: yRatio * Double(self.mapView.frame.height))
    }
    
    func fromScreenToCoor(screenPos: CGPoint) -> CLLocationCoordinate2D{
        let xRatio = Double(screenPos.x/self.mapView.frame.width) * Double(mapSpan.longitudeDelta)
        let yRatio = (1.0-Double(screenPos.y/self.mapView.frame.height)) * Double(mapSpan.latitudeDelta)
        return CLLocationCoordinate2D(latitude: yRatio+swCoord.latitude, longitude: xRatio+swCoord.longitude)
    }
    
    func assignOldLocationCoordinate(conversation: Conversation, oldLocations: [String:CLLocation]) -> CLLocation? {
        return oldLocations[conversation.conversationID]
    }
    
    // Reload the conversations array - NOT BEING USED RIGHT NOW
    func reloadConversations(){
        self.mapView.removeAnnotations(self.mapView.annotations)
        // placePins()
    }
    
    // If the map could not reload
    func mapViewDidFailLoadingMap(_ mapView: MKMapView, withError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }
    
    //
    func mapView(_ mapView: MKMapView,
                 didSelect view: MKAnnotationView)
    {
        // 1
        if view.annotation is MKUserLocation
        {
            // Don't proceed with custom callout
            return
        }
        // 2
        let selectedAnnotation = view.annotation as! ConversationPin
        conversationsToSend = selectedAnnotation.conversations
        bringInTiles()
        //performSegue(withIdentifier: "mapToTile", sender: self)
    }
    
    func goToTrendingTileScreen(){
        var trendingConv = [Conversation]()
        for convPin in groupedLocations{
            for conv in convPin{
                trendingConv.append(conv)
            }
        }
        conversationsToSend = self.sortByNumParticipant(conArray: trendingConv)
        bringInTiles()
        //performSegue(withIdentifier: "mapToTile", sender: self)
    }
    
    func bringInTiles() {
        let tileVC = storyboard?.instantiateViewController(withIdentifier: "Tile") as! TileScreen
        tileVC.conversations = conversationsToSend
        tileVC.view.frame.origin.x = self.view.frame.width
        tileVC.navController = (self.navigationController)!
        
        
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.view.frame
        tileVC.view.insertSubview(blurEffectView, at: 0)
        tileVC.view.backgroundColor = UIColor.clear
        tileVC.collectionView?.backgroundColor = UIColor.clear
        self.mapView.addSubview(tileVC.view)
        
        UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 5.0, initialSpringVelocity: 8.0, options: .curveEaseIn, animations: {
            tileVC.view.frame.origin.x = self.view.frame.origin.x
        }, completion: nil)
        
        /*
        UIView.animate(withDuration: 0.5, animations: {
            tileVC.view.frame.origin.x = self.view.frame.origin.x
        })
        */
    }
    
    // Navigation to tile screen
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let child = (segue.destination as! TileScreen)
        child.conversations = conversationsToSend
        
    }
    
    // Add the saved conversation bubble
    public func addSavedConversationBubble(title: String) {
        
        for view in mapView.subviews {
            if view is MapBubble {
                let bubble = view as! MapBubble
                if !bubble.isStar && !bubble.isLocation && !bubble.isNewConversation {
                    view.removeFromSuperview()
                }
            }
        }
        
        let bubble = MapBubble()
        
        bubble.setImage(image: UIImage(named: "clearView.png")!, color: Constants.RED)
        let heartWidth = bubble.frame.width/1.5
        let heartHeight = bubble.frame.height/1.5
        let heartImageView = UIImageView(frame: CGRect(x: (bubble.frame.width - heartWidth)/2.0, y: (bubble.frame.height - heartHeight)/2.0 - 5.0, width: heartWidth, height: heartHeight))
        heartImageView.image = #imageLiteral(resourceName: "heart-2")
        heartImageView.clipsToBounds = true
        bubble.addSubview(heartImageView)
        
        //bubble.setImage(image: #imageLiteral(resourceName: "heart-2"), color: Constants.BLUE)
        bubble.setMapViewController(mapVC: self)
        bubble.frame.origin = CGPoint(x: -100.0, y: bubbleMarginY)
        mapView.addSubview(bubble)
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: {
            bubble.frame.origin = CGPoint(x: CGFloat(self.mapView.frame.width / 2.0) - bubble.frame.width*2.0, y: self.bubbleMarginY)
        }, completion: nil)
    }
    
    // Hide the status bar
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    // If tapped on a bubble load the conversation vc
    public func loadConversationBubbleScreen() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: Constants.INPUT_TAB_BAR) as! InputTabBarController
        let savedConversations = Repo.sharedInstance.savedConversations
        
        if savedConversations.count > 0 {
            vc.conversationTitle = (savedConversations.first?.title)!
            vc.posts = (savedConversations.first?.posts)!
            vc.conversationID = (savedConversations.first?.conversationID)!
            vc.isFromBubble = true
            vc.navigationItem.setHidesBackButton(true, animated: true)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // hide the conversation bubble
    public func hideSavedConversationBubble() {
        
        for view in mapView.subviews {
            if view is MapBubble {
                let bubble = view as! MapBubble
                if !bubble.isStar && !bubble.isLocation && !bubble.isNewConversation {
                    view.removeFromSuperview()
                }
            }
        }
    }
    
    // center the map on the users location
    public func centerOnUserLocation(){
        let span:MKCoordinateSpan = MKCoordinateSpanMake(0.01, 0.01)
        let myLocation:CLLocationCoordinate2D = Repo.sharedInstance.userLocation.coordinate
        let region:MKCoordinateRegion = MKCoordinateRegionMake(myLocation, span)
        self.mapView.setRegion(region, animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
