//
//  FocusButton.swift
//  RecordButton
//
//  Created by Jessica Lewis on 3/30/17.
//  Copyright © 2017 Jessica Lewis. All rights reserved.
//

import UIKit

class FocusRing: UIView {
    
    private var circleBorder: CALayer!

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.drawButton()
        self.alpha = 0.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.drawButton()
        self.alpha = 0.0
    }

    private func drawButton() {
        
        self.backgroundColor = UIColor.clear
        
        circleBorder = CALayer()
        circleBorder.backgroundColor = UIColor.clear.cgColor
        circleBorder.borderWidth = 1
        circleBorder.borderColor = buttonColor.cgColor
        circleBorder.bounds = CGRect(x: 0, y: 0, width: self.bounds.size.width - 1.5, height: self.bounds.size.height - 1.5)
        circleBorder.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        circleBorder.position = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        circleBorder.cornerRadius = self.frame.size.width / 2
        layer.insertSublayer(circleBorder, at: 0)
    }
    
    override func layoutSubviews() {
        circleBorder.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        circleBorder.position = CGPoint(x: self.bounds.midX, y: self.bounds.midY)
        super.layoutSubviews()
    }

    internal var buttonColor: UIColor! = .white{
        didSet {
            circleBorder.backgroundColor = buttonColor.cgColor
        }
    }
}
