//
//  Filters.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 2/22/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Jessica Lewis
 Last Updated: 2 March 2017
 Purpose: The Filter class contains functions that apply various filters to images. An unfiltered image is passed into one of these functions and the filtered image is returned.
 */

import Foundation

class Filter {

    // Filter options
    let filters = [Constants.NONE, Constants.PHOTO_EFFECT_PROCESS, Constants.PHOTO_EFFECT_CHROME, Constants.PHOTO_EFFECT_NOIR]
    
    // Keeps track of what filter is being displayed in the filters array
    var filterIndex = 0
    
    // keeps track of whether the location filter is on or off
    var isLocationOn = false
    
    // view that the filter will be applied to
    var cameraView: UIView = UIView()
    
    // Overlayview that is being displayed when the user applies a filter
    lazy var filterOverlayView: FilterNameOverlay = {
        let filterOverlayView = FilterNameOverlay()
        return filterOverlayView
    }()
    
    // Overlayview that is being displayed when the user adds the location
    lazy var locationOverlayView: LocationOverlay = {
        /*let halfSizeOfView = 25.0
        let insetSize = cameraView.bounds.insetBy(dx: CGFloat(Int(2 * halfSizeOfView)), dy: CGFloat(Int(2 * halfSizeOfView))).size
        var pointX = CGFloat(UInt(arc4random() % UInt32(UInt(insetSize.width))))
        var pointY = CGFloat(UInt(arc4random() % UInt32(UInt(insetSize.height))))*/
        let locationOverlayView = LocationOverlay(frame: CGRect(x: 50, y: 50, width: 50, height: 50))
        return locationOverlayView
    }()
    
    init() {
        cameraView = UIView()
    }
    
    // initialize with view from camera
    init(view: UIView) {
        cameraView = view
    }
    
    // Applies a photo effect filter to an image and returns the image; Applies filter depending on the swipe direction, if right swipe, it displays the next filter, if left swipe, it displays the previous filter
    func applyFilter(originalPhoto: UIImage, direction: UISwipeGestureRecognizerDirection, camType: String)-> UIImage {
        
        // determine the type of filter to be applied
        if (direction == UISwipeGestureRecognizerDirection.left) {
            if (filterIndex < filters.count-1) {filterIndex = filterIndex + 1}
            else {filterIndex = 0}
        }
        else {
            if (filterIndex > 0) {filterIndex = filterIndex - 1}
            else {filterIndex = filters.count-1}
        }

        // no filter
        if (filterIndex == 0) {
            return originalPhoto
        }
        
        // apply filter
        else {
            let coreImage = CIImage(cgImage: originalPhoto.cgImage!)
            let filter = CIFilter(name: filters[filterIndex])
            filter?.setValue(coreImage, forKey: kCIInputImageKey)
            
            //filterOverlayView.displayView(onView: cameraView, labelText: filters[filterIndex].replacingOccurrences(of: Constants.CI_PHOTO_EFFECT, with: ""))
            
            if let output = filter?.value(forKey: kCIOutputImageKey) as? CIImage {
                let imageContext = CIContext(options:nil)
                let temp:CGImage = imageContext.createCGImage(output, from: output.extent)!
                var filteredImage = UIImage()
                if (camType == Constants.FRONT) {
                    filteredImage = UIImage(cgImage: temp, scale: 1.0, orientation: .leftMirrored)
                } else {
                    filteredImage = UIImage(cgImage: temp, scale: 1.0, orientation: .right)
                }
                return filteredImage
            }
            else { return originalPhoto }
        }
    }
    
    func applySpeed(originalPhoto: UIImage) {
        
    }
    
    func applyWeather(originalPhoto: UIImage) {
        
    }
    
    func applyLocation(originalPhoto: UIImage, userLocationString: String) {
        if (!isLocationOn) {
            locationOverlayView.displayView(onView: cameraView, labelText: userLocationString)
        }
        else {
            locationOverlayView.hideView()
        }
        isLocationOn = !isLocationOn
    }
    
    func applyTime(originalPhoto: UIImage) {
        
    }
    
    func discardOverlays() {
        locationOverlayView.hideView()
    }
}
