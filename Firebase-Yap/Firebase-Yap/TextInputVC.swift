//
//  TextInputVC.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 1/28/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Jessica Lewis
 Last Updated: 10 Febraury 2017
 Purpose: The Gallery View Controller is part of the tab set up and can be navigated to from the tab bar. It allows the user to choose a photo from the gallery to upload.
 */

import UIKit
import MapKit

class TextInputVC: UIViewController, UITextViewDelegate {

    @IBOutlet weak var textView: UITextView!
    
    // UI variables
    let placeholderText = Constants.PLACEHOLDER_TEXT
    
    // DB variables
    var databaseAccess = TextInputDatabaseAccess()
    var conversationID = ""
    
    let userLocation = CLLocation()
    
    //var chatVC = ChatViewCollectionViewController()
    var chatVC:ConversationViewController = ConversationViewController()
    var navBar = UINavigationBar()
    var swipeDownGestureRecognizer : UISwipeGestureRecognizer = UISwipeGestureRecognizer()

    // label displaying number of characters left, declared outside viewDidLoad to access in other functions
    let characterCountLabel = UIBarButtonItem(
        title: Constants.CHARACTER_COUNT_LABEL,
        style: UIBarButtonItemStyle.plain,
        target: self,
        action: nil
    )
    
    // button allowing user to post text to conversation, declared outside viewDidLoad to access in other functions
    let postButton = UIBarButtonItem(
        title: Constants.POST_TEXT_BUTTON,
        style: .plain,
        target: self,
        action: #selector(post)
    )
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set textView delegate to self in order to implement textViewProtocol functions
        textView.delegate = self
        
        // To make the keyboard appear when the view is loaded
        textView.becomeFirstResponder()
        
        // get rid of the big margin at the top of text view
        textView.contentInset = UIEdgeInsetsMake(-50, 0, 0, 0)
        
        // Placeholder text
        self.textView.text = placeholderText
        self.textView.textColor = UIColor.lightGray
        self.textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
        //self.updateCharacterCount() - don't update because don't want label to include placeholder text

        // add and set up the toolbar above the keyboard
        setUpToolbar()
        
        swipeDownGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        swipeDownGestureRecognizer.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDownGestureRecognizer)
        
        // Add notification for opening of the keyboard
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.textView.setContentOffset(CGPoint.zero, animated: false)
    }
    
    func dismissKeyboard() {
        self.textView.text = placeholderText
        self.textView.textColor = UIColor.lightGray
        characterCountLabel.title = String(Constants.CHARACTER_LIMIT)
        self.tabBarController?.selectedIndex = 0
    }
    
    func setUpToolbar() {
        // define toolbar with light gray tint
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        keyboardToolbar.isTranslucent = false
        keyboardToolbar.barTintColor = self.tabBarController?.tabBar.barTintColor
        
        // post button - bold font, gray color, changes color when able to post, starts disabled
        postButton.setTitleTextAttributes([
            NSFontAttributeName : UIFont(name: Constants.POST_BUTTON_FONT, size: 14.0)!],
                                          for: UIControlState.normal
        )
        postButton.tintColor = UIColor.groupTableViewBackground
        postButton.isEnabled = false
        
        // character count label
        characterCountLabel.setTitleTextAttributes([
            NSFontAttributeName : UIFont.systemFont(ofSize: 14.0),
            NSForegroundColorAttributeName : UIColor.groupTableViewBackground],
                                                   for: UIControlState.normal
        )
        characterCountLabel.isEnabled = false
        
        // used to space out the items on the toolbar
        let flexible = UIBarButtonItem(
            barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace,
            target: self,
            action: nil
        )
        
        // set up toolbar
        let toolbarItems = [flexible, characterCountLabel, postButton]
        keyboardToolbar.items = toolbarItems
        
        // the layer with the width of the device
        // and height of 0.5 px
        let topBorder = CALayer()
        topBorder.frame = CGRect(x:0, y:0, width:view.frame.size.width, height:0.5)
        topBorder.backgroundColor = UIColor.lightGray.cgColor
        
        // adding the layer to the top of the toolBar
        keyboardToolbar.layer.addSublayer(topBorder)
        textView.inputAccessoryView = keyboardToolbar
    }
    
    // Move tab bar (above keyboard) based on keyboard pen notification
//    func keyboardWillShow(notification: NSNotification) {
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            let keyboardHeight = keyboardSize.height
//            let toolBarYPos = UIScreen.main.bounds.height - 400
//            self.tabBarController?.tabBar.center = CGPoint(x: (self.tabBarController?.tabBar.center.x)!, y: toolBarYPos)
//            //self.tabBarController?.tabBar.frame = CGRect(origin: CGPoint(x: 0,y: toolBarYPos), size: CGSize(width: (self.tabBarController?.tabBar.frame.width)!, height: (self.tabBarController?.tabBar.frame.height)!))
//            
//        }
//    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        UIView.animate(withDuration: 0.2, animations: {
//            self.tabBarController?.tabBar.frame = CGRect(origin: CGPoint(x: 0,y: UIScreen.main.bounds.height - (self.tabBarController?.tabBar.frame.height)!), size: CGSize(width: (self.tabBarController?.tabBar.frame.width)!, height: (self.tabBarController?.tabBar.frame.height)!))
//        })
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openCamera() {
        performSegue(withIdentifier: Constants.OPEN_CAMERA_SEGUE, sender: nil)
    }
    
    func post() {
        let userID = Repo.sharedInstance.userID
        
        _ = databaseAccess.sendPost(userID: userID, text: textView.text, location: Repo.sharedInstance.userLocation)
        
        self.tabBarController?.selectedIndex = 0
        self.tabBarController?.tabBar.center = CGPoint(x: UIScreen.main.bounds.width * 1.5, y: (self.tabBarController?.tabBar.center.y)!)
        
        textView.text = placeholderText
        textView.textColor = UIColor.gray

        chatVC.animateIn()
        //_ = navigationController?.popViewController(animated: true)
    }
    
    func updateCharacterCount() {
        characterCountLabel.title = "\(Constants.CHARACTER_LIMIT - self.textView.text.characters.count)"
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.updateCharacterCount()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        // Combine the textView text and the replacement text to
        // create the updated text string
        let nsCurrentText = textView.text as NSString?
        let updatedText = nsCurrentText?.replacingCharacters(in: range, with: text)
        //let updatedText = currentText?.replacingCharacters(in: range as Range, with: text)
        
        // If updated text view will be empty, add the placeholder
        // and set the cursor to the beginning of the text view
        if (updatedText?.isEmpty)! {
            textView.text = placeholderText
            textView.textColor = UIColor.lightGray
            characterCountLabel.title = "\(Constants.CHARACTER_LIMIT)"
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            postButton.tintColor = UIColor.gray
            postButton.isEnabled = false
            return false
        }
            
        // Else if the text view's placeholder is showing and the
        // length of the replacement string is greater than 0, clear
        // the text view and set its color to black to prepare for
        // the user's entry
        else if textView.textColor == UIColor.lightGray && !text.isEmpty {
            textView.text = nil
            textView.textColor = UIColor.black
            postButton.tintColor = UIColor.white
            postButton.isEnabled = true
            self.updateCharacterCount()
        }
        
        //return if the number of characters is less than the character limit
        return textView.text.characters.count +  (text.characters.count - range.length) <= Constants.CHARACTER_LIMIT
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.gray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = placeholderText
            textView.textColor = UIColor.gray
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
