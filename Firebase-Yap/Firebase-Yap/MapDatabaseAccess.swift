//
//  MapDatabaseAccess.swift
//  Firebase-Yap
//
//  Created by Guilherme Pereira on 2/20/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import Foundation
import Firebase
import MapKit
import CoreLocation

class MapDatabaseAccess {
    private let firebase = FIRDatabase.database().reference()
    private let ref = FIRDatabase.database().reference(withPath: "conversations")
    private let mapView:MapViewController
    
    init(mapView: MapViewController) {
        self.mapView = mapView
    }
    public func compareCoords(nw: CLLocationCoordinate2D, se: CLLocationCoordinate2D, target: CLLocationCoordinate2D) -> Bool{
        let latBound = target.latitude < nw.latitude && target.latitude > se.latitude
        let lonBound = target.longitude < se.longitude && target.longitude > nw.longitude
        
        return latBound && lonBound
    }
    public func findConversationsAround(nwBound: CLLocationCoordinate2D, seBound: CLLocationCoordinate2D){
        ref.observeSingleEvent(of: .value, with: {(snapshot) in
            var localConversations: [Conversation] = []
            for item in snapshot.children {
                let conversation = Conversation(snapshot: item as! FIRDataSnapshot)
                if self.compareCoords(nw: nwBound, se: seBound, target: conversation.location.coordinate) {
                    localConversations.append(conversation)
                }
                
            }
            self.mapView.getLocations(convArr: localConversations)
            
            
        })
        
    }
    
    public func createNewConversation(location: CLLocationCoordinate2D, title: String) -> Conversation {
        let conversation = Conversation(conversationID: UUID.init().description, location: CLLocation(latitude: location.latitude, longitude: location.longitude), ownerUserID: Repo.sharedInstance.userID, title: title, startTime: Date.init())
        
        let conversationRef = self.ref.child(conversation.conversationID)
        conversationRef.setValue(conversation.toAnyObject())
        _ = saveConversation(userID: Repo.sharedInstance.userID, conversationID: conversation.conversationID)
        return conversation
    }
    
    private func saveConversation(userID: String, conversationID: String) -> String{
        
        if (Repo.sharedInstance.savedConversations.contains(where: { (conversation) in
            return conversation.conversationID == conversationID
        })) {
            return ""
        }
        
        let usersRef = FIRDatabase.database().reference(withPath: "users").child(userID).child("saved_conversations")
        let activeConvoRef = usersRef.child(conversationID)
        activeConvoRef.setValue("")
        self.convertStringtoSavedConversation(conversationID: conversationID)
        
        return conversationID
    }
    
    public func loadSavedConversations(userID: String) {
        let userRef = FIRDatabase.database().reference(withPath: "users").child(userID)
        userRef.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            
            if snapshot.hasChild("saved_conversations") {
                let savedConversationsRef = snapshot.childSnapshot(forPath: "saved_conversations")
                var savedConversationsList = [String]()
                
                for item in savedConversationsRef.children {
                    let conversation = item as! FIRDataSnapshot
                    savedConversationsList.append(conversation.key)
                }
                
                for conversation in savedConversationsList {
                    self.convertStringtoSavedConversation(conversationID: conversation)
                }
            } else {
                self.mapView.hideSavedConversationBubble()
            }
        })
    }
    
    public func loadSavedConversation(conversationID: String) {
        convertStringtoSavedConversation(conversationID: conversationID)
    }
    
    private func convertStringtoSavedConversation(conversationID: String) {
        
        let conversationRef = FIRDatabase.database().reference(withPath: "conversations")
        
            
        conversationRef.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            if snapshot.hasChild(conversationID) {
                let conversationSnapshot = snapshot.childSnapshot(forPath: conversationID)
                let conversation = Conversation(snapshot: conversationSnapshot)
                Repo.sharedInstance.savedConversations.append(conversation)
                Repo.sharedInstance.bubbleImageDictionary[conversationID] = nil
            }
        })
    }
    
    public func logIn() {
        if let user = FIRAuth.auth()?.currentUser {
            Repo.sharedInstance.userID = user.uid
            Repo.sharedInstance.isInitialOpen = false
        } else {
            FIRAuth.auth()?.signInAnonymously(completion: {(user, error) in
                
                if error != nil {
                    Repo.sharedInstance.didLoginFail = true
                    Repo.sharedInstance.errorMessage = error?.localizedDescription
                } else {
                    self.firebase.child("users").child(user!.uid).child("num_flagged").setValue(0)
                    Repo.sharedInstance.userID = (user?.uid)!
                    Repo.sharedInstance.isInitialOpen = false
                }
            })
        }
    }
    
}
