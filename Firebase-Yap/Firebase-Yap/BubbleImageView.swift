//
//  BubbleImageView.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 3/13/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import Foundation

class BubbleImageView: UIImageView {
    let RADIUS:CGFloat = 60.0
    
    override init(image: UIImage?) {
        super.init(image: image)
        
        let cropper = ImageCropper()
        let squareImage = cropper.makeSquare(image: image!)
        
        self.image = squareImage
        frame.size.width = RADIUS
        frame.size.height = RADIUS
        
        layer.borderWidth = 2.0
        layer.masksToBounds = false
        layer.borderColor = Constants.BLUE.cgColor
        layer.cornerRadius = frame.size.height/2
        clipsToBounds = true
    }
    
    init(image: UIImage?, x: CGFloat, y: CGFloat) {
        super.init(image: image)
        
        let cropper = ImageCropper()
        //let squareImage = cropper.makeSquare(image: image!, withOrigin: CGPoint(x:x, y: y))
        let squareImage = cropper.makeRectangle(image: image!, width: (image?.size.width)!, height: (image?.size.height)!, withOrigin: CGPoint(x: x, y: y))
        
        self.image = squareImage
        frame.size.width = RADIUS
        frame.size.height = RADIUS
        
        layer.borderWidth = 2.0
        layer.masksToBounds = false
        layer.borderColor = UIColor.lightGray.cgColor
        layer.cornerRadius = frame.size.height/2
        clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
