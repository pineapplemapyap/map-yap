//
//  Conversation.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 1/5/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import Foundation
import Firebase
import MapKit

class Conversation: NSObject {
    
    let conversationID: String
    
    let ref: FIRDatabaseReference?
    let location: CLLocation
    let ownerUserID: String
    let title: String
    let startTime: Date
    
    var numParticipants = 0
    var posts = [Post]()
    
    init(conversationID: String, location: CLLocation, ownerUserID: String, title: String, startTime: Date) {
        self.conversationID = conversationID
        self.location = location
        self.startTime = startTime
        self.ownerUserID = ownerUserID
        self.title = title
        self.ref = nil
    }
    
    init(snapshot: FIRDataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        
        conversationID = snapshot.key
        let locationData = snapshotValue["location"] as! [String:AnyObject]
        let lat = locationData["lat"] as! Double
        let lon = locationData["lon"] as! Double
        
        location = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lon))
        title = snapshotValue["title"] as! String
        ownerUserID = snapshotValue["owner_user_id"] as! String
        
        let startTimeData = snapshotValue["start_time"] as! String
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZZZZZ"
        startTime = dateFormatter.date(from: startTimeData)!
        
        numParticipants = snapshotValue["num_participants"] as! Int
        
        var newPost = Post()
        let postsData = snapshotValue["posts"] as! [String:AnyObject]
        for (postID, data) in postsData {
            
            let dataDict = data as! [String:AnyObject]
            
            
            
            newPost = Post(content: dataDict["content"]! as! String,
                           userID: dataDict["user_id"] as! String,
                           time: dateFormatter.date(from: dataDict["post_time"] as! String)!,
                           postID: postID,
                           numLikes: dataDict["num_likes"] as! Int,
                           rating: dataDict["rating"] as! Double)
            
            posts.append(newPost)
        }
        
        
        
        
        ref = snapshot.ref
    }
    
    func toAnyObject() -> Any {
        return [
            "title": title,
            "owner_user_id": ownerUserID,
            "num_participants" : numParticipants,
            "location" : location.toAnyObject(),
            "posts" : "", // pushing no posts because conversation just started
            "start_time" : startTime.description
        ]
    }
}

extension CLLocation {
    func toAnyObject() -> Any {
        return [
            "lat" : self.coordinate.latitude,
            "lon" : self.coordinate.longitude
        ]
    }
}


