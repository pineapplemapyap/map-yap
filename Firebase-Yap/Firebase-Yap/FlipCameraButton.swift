//
//  FlipCameraButton.swift
//  RecordButton
//
//  Created by Jessica Lewis on 3/28/17.
//  Copyright © 2017 Jessica Lewis. All rights reserved.
//

import UIKit

class FlipCameraButton: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupButton()
    }

    internal func setupButton() {
        self.image = #imageLiteral(resourceName: "flipCam")
        self.isHidden = false
        self.isUserInteractionEnabled = true
        self.contentMode = .scaleAspectFill
    }
    
    func setButtonImage(image: UIImage) {
        self.image = image
    }
}
