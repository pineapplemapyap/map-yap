//
//  InputScreenDatabaseAccess.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 2/13/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import Foundation
import Firebase
import MapKit

class InputScreenDatabaseAccess {
    
    private let ref = FIRDatabase.database().reference(withPath: "conversations")
    private var conversationRef: FIRDatabaseReference
    private var postsRef: FIRDatabaseReference
    lazy var storageRef: FIRStorageReference = FIRStorage.storage().reference(forURL: "gs://fir-yap.appspot.com/")
    private let imageURLNotSetKey = "NOTSET"
    private let inputScreenVC:CameraViewController
    private var conversationID: String
    private var numParticipants: Int!
    private var isSavedConversation: Bool!
    
    init(inputScreenVC: CameraViewController) {
        self.inputScreenVC = inputScreenVC
        conversationRef = ref.child(self.inputScreenVC.conversationID)
        postsRef = conversationRef.child("posts")
        conversationID = Repo.sharedInstance.currentConversationID
    }
    
    // returns postID
    public func uploadPhoto(image: UIImage, userID:String, userLocation: CLLocation) -> String? {
        self.conversationID = Repo.sharedInstance.currentConversationID
        let post = Post(content: imageURLNotSetKey, userID: userID, time: Date.init(), userLocation: userLocation, isText: false, conversationID: conversationID)
    
        if let key = sendPhoto(post: post) {
            if let imageData = UIImageJPEGRepresentation(image, 1.0) {
                let imagePath = "conversations/\(conversationID)/img/\(post.postID).jpg"
                
                let metadata = FIRStorageMetadata()
                metadata.contentType = "image/jpeg"
                
                storageRef.child(imagePath).put(imageData, metadata: metadata) { (metadata, error) in
                    if let error = error {
                        print("Error uploading photo: \(error)")
                        return
                    }
                    self.setImageURL(self.storageRef.child((metadata?.path)!).description, forPhotoMessageWithKey: key)
                }
                
                return post.postID
            }
        }
        return nil
    }
    
    public func uploadVideo(videoURL: URL, userID: String, userLocation: CLLocation) -> String? {
        print("Here's the file URL: ", videoURL)
        
        let post = Post(content: imageURLNotSetKey, userID: userID, time: Date.init(), userLocation: userLocation, isText: false, conversationID: conversationID)
        if let key = sendPhoto(post: post) {
            // Where we'll store the video:
            let imagePath = "conversations/\(conversationID)/img/\(post.postID).mov"
            
            let metadata = FIRStorageMetadata()
            metadata.contentType = "video/mov"
            
            // Start the video storage process
            storageRef.child(imagePath).putFile(videoURL, metadata: metadata, completion: { (metadata, error) in
                if let error = error {
                    print("Error uploading photo: \(error)")
                    return
                }
                self.setImageURL(self.storageRef.child((metadata?.path)!).description, forPhotoMessageWithKey: key)
            })
        }
        return post.postID
    }
    
    private func sendPhoto(post: Post) -> String? {
        self.conversationID = post.conversationID
        conversationRef = ref.child(self.conversationID)
        postsRef = conversationRef.child("posts")
        let itemRef = postsRef.child(post.postID)
        itemRef.setValue(post.toAnyObject())
        post.geocodeLocation()
        
        return itemRef.key
    }
    
    private func setImageURL(_ url: String, forPhotoMessageWithKey key: String) {
        let itemRef = postsRef.child(key)
        itemRef.updateChildValues(["content": url])
    }
}
