//
//  ConversationTable.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 1/5/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import UIKit
import MapKit

class ConversationTable: UITableViewController {
    
    var databaseAccess:ConversationDatabaseAccess!
    var conversations = [Conversation]()
    var row = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        databaseAccess = ConversationDatabaseAccess(conversationTable: self)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        // For testing
        //self.view.accessibilityIdentifier = "ConversationTableVC"
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return conversations.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cCell", for: indexPath) as! ConversationCell

        // Configure the cell...
        cell.titleLabel.text = conversations[indexPath.row].title
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath {
        row = indexPath.row
        return indexPath
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        let child = segue.destination as! InputTabBarController
        
        // Pass the selected object to the new view controller.
        //let indexPath = self.tableView.indexPathForSelectedRow
        
        child.posts = conversations[row].posts
        child.navTitle = conversations[row].title
        child.conversationID = conversations[row].conversationID
        child.navBar = (self.navigationController?.navigationBar)!
        child.navigationItem.setHidesBackButton(true, animated:true);
    }
    
    @IBAction func newConversationButtonTap(_ sender: Any) {
        self.tableView.reloadData()
        let alert = UIAlertController(title: "New Conversation",
                                      message: "Add a Conversation",
                                      preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save",
                                       style: .default) { _ in
                                        guard let textField = alert.textFields?.first,
                                            let text = textField.text else { return }
                                        
                                        let user = User(deviceID: Repo.sharedInstance.userID)
                                        _ = self.databaseAccess.addNewConversation(title: text, ownerUserID: user, location: CLLocation.init())
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .default)
        
        alert.addTextField()
        
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
    }
}
