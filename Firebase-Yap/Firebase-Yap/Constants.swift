//
//  Constants.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 3/2/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Jessica Lewis
 Last Updated: 2 March 2017
 Purpose: All the constants
 */

import Foundation

class Constants {
    // ICONS
    static let FILTER_ICON = "filter.png"
    static let SPEED_ICON = "speed.png"
    static let WEATHER_ICON = "weather.png"
    static let LOCATION_ICON = "mapMarker"
    static let TIME_ICON = "time.png"
    static let SEND_ICON = "send.png"
    static let STOCK_ICON = "stock.jpg"
    static let CAMERA_ICON = "camera.png"
    static let TEXT2_ICON = "text2.png"
    static let GALLERY_ICON = "gallery.png"
    static let LIKE_EMPTY_ICON = "heartOff"
    static let LIKE_FILLED_ICON = "heartOn"
    static let EYE_ICON = "eye.png"
    static let TEXT_ICON = "text.png"
    static let X_ICON = "x_icon.png"
    static let FLIP_CAM_ICON = "flipCam2"
    static let HEART = "hearts.png"
    static let HEART_EMPTY = "hearts_empty.png"
    static let STAR = "star.png"
    static let CENTER = "center.png"
    static let VIDEO = "Video"
    static let X = "x"
    //static let REMOVE_LOCATION = "removeLocation.png"
    
    // Photo Effects
    static let NONE = "None"
    static let PHOTO_EFFECT_INSTANT = "CIPhotoEffectInstant"
    static let PHOTO_EFFECT_PROCESS = "CIPhotoEffectProcess"
    static let PHOTO_EFFECT_CHROME = "CIPhotoEffectChrome"
    static let PHOTO_EFFECT_NOIR = "CIPhotoEffectNoir"
    static let CI_PHOTO_EFFECT = "CIPhotoEffect"
    
    // Filter overlay
    static let FILTER_NAME_OVERLAY = "FilterNameOverlay"
    static let LOCATION_OVERLAY = "LocationOverlay"
    
    // Camera 
    static let FRONT = "front"
    static let BACK = "back"
    static let OPEN_CAMERA_SEGUE = "openCamera"
    static let VIDEO_QUEUE = "com.invasivecode.videoQueue"
    static let NEW_VIDEO = "newVideo"
    static let PICKED_IMAGE = "UIImagePickerControllerOriginalImage"

    // Text input screen
    static let PLACEHOLDER_TEXT = "What's Happening?"
    static let CHARACTER_COUNT_LABEL = "140"
    static let POST_TEXT_BUTTON = "POST"
    static let CHARACTER_LIMIT = 140
    static let POST_BUTTON_FONT = "HelveticaNeue-Bold"
    
    // Storyboard IDs
    static let POSTS = "Posts"
    static let CHAT_VIEW = "ChatCollectionView"
    static let INPUT_TAB_BAR = "inputScreen"
    static let CONVERSATION_VIEW = "ConversationVC"
    
    // Margins
    static let STATUS_BAR_HEIGHT:CGFloat = 20.0
    static let BUBBLE_SNAP_DISTANCE: CGFloat = 35.0
    static let CONVERSATION_HEADER_HEIGHT: CGFloat = 23.0
    static let CONVERSATION_POSTS_MARGIN: CGFloat = 10.0
    
    // Colors
    static let BLUE = UIColor(colorLiteralRed: 0.0/255.0, green: 122/255.0, blue: 255.0/255.0, alpha: 1.0)
    static let TEAL_BLUE = UIColor(colorLiteralRed: 90.0/255.0, green: 200.0/255.0, blue: 250.0/255.0, alpha: 1.0)
    static let OFF_WHITE = UIColor(colorLiteralRed: 255.0/255.0, green: 250.0/255.0, blue: 240.0/255.0, alpha: 1.0)
    static let RED = UIColor(colorLiteralRed: 255.0/255.0, green: 59.0/255.0, blue: 48.0/255.0, alpha: 1.0)
    static let ORANGE = UIColor(colorLiteralRed: 255.0/255.0, green: 149.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    static let GREEN = UIColor(colorLiteralRed: 76.0/255.0, green: 217.0/255.0, blue: 100.0/255.0, alpha: 1.0)
    static let YELLOW = UIColor(colorLiteralRed: 255.0/255.0, green: 204.0/255.0, blue: 0.0/255.0, alpha: 1.0)
    static let PURPLE = UIColor(colorLiteralRed: 88.0/255.0, green: 86.0/255.0, blue: 214.0/255.0, alpha: 1.0)
    static let PINK = UIColor(colorLiteralRed: 255.0/255.0, green: 45.0/255.0, blue: 85.0/255.0, alpha: 1.0)
    
}
