//
//  IconBubble.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 3/20/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import Foundation

class IconBubble: Bubble {
    
    var isBack = false
    var isMap = false
    var isFromBubble = false
    
    private var conversationVC = ConversationViewController()
    private var tileVC = TileScreen()
    
    func setupViewController(conversationVC: ConversationViewController) {
        self.conversationVC = conversationVC
    }
    
    func setupViewController(tileVC: TileScreen) {
        self.tileVC = tileVC
    }
    
    override func handleTap(gesture: UITapGestureRecognizer) {
        super.handleTap(gesture: gesture)
        
        if isBack {
            self.conversationVC.openTileScreen()
        } else if isMap {
            if isFromBubble {
                //self.conversationVC.openMapScreen()
            } else {
                self.tileVC.openMap()
            }
        } else {
            self.conversationVC.openInputScreen()
        }
        
    }
    
    override func handlePan(gesture: UIPanGestureRecognizer!) {
        if gesture.state == UIGestureRecognizerState.began {
            let locationInView = gesture.location(in: superview)
            dragStart = CGPoint(x:center.x, y: locationInView.y - center.y)
            
            layer.shadowOffset = CGSize(width: 0, height: 5)
            layer.shadowOpacity = 0.3
            layer.shadowRadius = 2
            
            return
        }
        
        if gesture.state == UIGestureRecognizerState.ended {
            dragStart = nil
            
            layer.shadowOffset = CGSize(width: 0, height: 0)
            layer.shadowOpacity = 0.0
            layer.shadowRadius = 0
            
            return
        }
        
        let locationInView = gesture.location(in: superview)
        
        UIView.animate(withDuration: 0.1) {
            var locationY = locationInView.y - self.dragStart!.y
            let locationX = self.dragStart!.x
            
            if ((locationY) - self.frame.height / 2.0) < Constants.STATUS_BAR_HEIGHT + Constants.BUBBLE_SNAP_DISTANCE {
                locationY = Constants.STATUS_BAR_HEIGHT + self.frame.height / 2.0
            } else if ((locationY) + self.frame.height / 2.0) > (self.superview?.frame.height)! - Constants.BUBBLE_SNAP_DISTANCE {
                locationY = (self.superview?.frame.height)! - (Constants.STATUS_BAR_HEIGHT + 5.0)
            }
            
            /*
            if ((locationX) - self.frame.width / 2.0) < Constants.BUBBLE_SNAP_DISTANCE {
                locationX = 0.0 + self.frame.width / 2.0
            } else if ((locationX) + self.frame.width/2.0) > (self.superview?.frame.width)! - Constants.BUBBLE_SNAP_DISTANCE {
                locationX = (self.superview?.frame.width)! - self.frame.width/2.0
            }
            */
            self.center = CGPoint(x: locationX,
                                  y: locationY)
            
            //self.conversationVC.handlePan(recognizer: gesture)
        }
    }
    
}
