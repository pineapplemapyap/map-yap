//
//  DatabaseAccess.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 2/13/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import Foundation
import Firebase
import MapKit

class TileScreenDatabaseAccess {
    
    internal let rootRef = FIRDatabase.database().reference()
    internal let conversationsRef = FIRDatabase.database().reference(withPath: "conversations")
    
    public let tileScreenVC:TileScreen
    
    init(tileScreen: TileScreen) {
        
        tileScreenVC = tileScreen
        
        conversationsRef.observe(.value, with: { snapshot in
//            var newConversations: [Conversation] = []
//
            for conversation in self.tileScreenVC.conversations {
                for post in conversation.posts {
                    if !post.isText && conversation.tileImage.size.width == 0 {
                        self.fetchImageDataAtURL(post: post, convoID: conversation.conversationID)
                        break
                    }
                }
            }
            
            self.tileScreenVC.conversations.sort {
                $0.numParticipants > $1.numParticipants
            }
            
            //self.tileScreenVC.conversations = newConversations
            self.tileScreenVC.numTiles = self.tileScreenVC.conversations.count
            if(self.tileScreenVC.numTiles > 7) {
                self.tileScreenVC.numTiles = 7
            }
            self.tileScreenVC.collectionView?.reloadData()
            
        })
    }
    
    private func fetchImageDataAtURL(post: Post, convoID: String) {
        let photoURL = post.content
        if(photoURL != "NOTSET") {
            let storageRef = FIRStorage.storage().reference(forURL: photoURL)
            
            
            storageRef.data(withMaxSize: INT64_MAX){ (data, error) in
                if let error = error {
                    print("Error downloading image data: \(error)")
                    return
                }
                
                storageRef.metadata(completion: { (metadata, metadataErr) in
                    if let error = metadataErr {
                        print("Error downloading metadata: \(error)")
                        return
                    }
                    
                    if (metadata?.contentType == "image/gif") {
                        //UIImage.animatedImageNamed(data?.description, duration: data.)
                        print("ERROR: image/gif")
                    } else {
                        let image = UIImage.init(data: data!)
                        for convo in self.tileScreenVC.conversations {
                            if(convo.conversationID == convoID) {
                                convo.tileImage = image!
                                self.tileScreenVC.collectionView?.reloadData()
                            }
                        }
                    }
                })
            }
    }
    }
    
    

}
