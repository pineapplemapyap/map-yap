//
//  VideoVC.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 3/13/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
// Reference: https://www.raywenderlich.com/94404/play-record-merge-videos-ios-swift

import UIKit
import AVFoundation
import MobileCoreServices
import AVKit

class VideoVC: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureFileOutputRecordingDelegate {

    var navBar = UINavigationBar()
    
    // DB variables
    //var databaseAccess = TextInputDatabaseAccess()
    var conversationID = ""
    
    //var previewLayer = AVCaptureVideoPreviewLayer()
    
    @IBOutlet weak var captureButton: UIView!
    @IBOutlet weak var cameraView: UIImageView!
    
    var isRecording = false
    var videoFileOutput = AVCaptureMovieFileOutput()
    //var captureDevice = AVCaptureDevice()
    
    lazy var cameraSession: AVCaptureSession = {
        let s = AVCaptureSession()
        s.sessionPreset = AVCaptureSessionPresetHigh
        return s
    }()
    
    lazy var previewLayer: AVCaptureVideoPreviewLayer = {
        let preview =  AVCaptureVideoPreviewLayer(session: self.cameraSession)
        preview?.bounds = CGRect(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height)
        preview?.position = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        preview?.videoGravity = AVLayerVideoGravityResize
        return preview!
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCameraSession()
        
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        cameraView.layer.addSublayer(previewLayer)
        setupButtons()
        
        cameraSession.startRunning()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*func beginSession() {
        do {
            let captureDeviceInput = try AVCaptureDeviceInput(device: captureDevice)
            cameraSession.addInput(captureDeviceInput)
        }
        catch _ {
            print("error")
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: cameraSession)
        self.cameraView.layer.addSublayer(previewLayer)
        self.cameraView.bringSubview(toFront: self.captureButton)
        previewLayer.frame = self.cameraView.layer.frame
        cameraSession.startRunning()
    }*/
    
    func setupCameraSession() {
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo) as AVCaptureDevice
        
        do {
            let deviceInput = try AVCaptureDeviceInput(device: captureDevice)
            
            cameraSession.beginConfiguration()
            
            if (cameraSession.canAddInput(deviceInput) == true) {
                cameraSession.addInput(deviceInput)
            }
            
            let dataOutput = AVCaptureVideoDataOutput()
            dataOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString) : NSNumber(value: kCVPixelFormatType_420YpCbCr8BiPlanarFullRange as UInt32)]
            dataOutput.alwaysDiscardsLateVideoFrames = true
            
            if (cameraSession.canAddOutput(dataOutput) == true) {
                cameraSession.addOutput(dataOutput)
            }
            
            cameraSession.commitConfiguration()
            
            let queue = DispatchQueue(label: Constants.VIDEO_QUEUE)
            dataOutput.setSampleBufferDelegate(self, queue: queue)
            
        }
        catch let error as NSError {
            NSLog("\(error), \(error.localizedDescription)")
        }
        
        self.cameraSession.addOutput(videoFileOutput)
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        // Here you collect each frame and process it
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didDrop sampleBuffer: CMSampleBuffer!, from connection: AVCaptureConnection!) {
        // Here you can count how many frames are dopped
    }
    
    func captureOutput(captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAtURL outputFileURL: NSURL!, fromConnections connections: [AnyObject]!, error: NSError!) {
        return
    }
    
    func captureOutput(captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAtURL fileURL: NSURL!, fromConnections connections: [AnyObject]!) {
        return
    }
    
    func capture(_: AVCaptureFileOutput!, didStartRecordingToOutputFileAt: URL!, fromConnections: [Any]!) {
        //Informs the delegate when the output has started writing to a file.
    }
    func capture(_: AVCaptureFileOutput!, willFinishRecordingToOutputFileAt: URL!, fromConnections: [Any]!, error: Error!) {
        //Informs the delegate when the output will stop writing new samples to a file.
    }
    func capture(_: AVCaptureFileOutput!, didFinishRecordingToOutputFileAt: URL!, fromConnections: [Any]!, error: Error!) {
        //Informs the delegate when all pending data has been written to an output file.
    }
    func capture(_: AVCaptureFileOutput!, didPauseRecordingToOutputFileAt: URL!, fromConnections: [Any]!) {
        //Called whenever the output is recording to a file and successfully pauses the recording at the request of a client.
    }
    func capture(_: AVCaptureFileOutput!, didResumeRecordingToOutputFileAt: URL!, fromConnections: [Any]!) {
        //Called whenever the output, at the request of the client, successfully resumes a file recording that was paused.
    }
    
    // Loads all the buttons for the camera screen
    func setupButtons() {
        // Set up capture button
        captureButton.layer.cornerRadius = captureButton.frame.size.width/2
        captureButton.clipsToBounds = true
        captureButton.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        captureButton.layer.borderWidth = 5.0
        captureButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action:#selector(recordVideo)))
        captureButton.isHidden = false
        isRecording = false
    }
    
    func recordVideo() {
        if (isRecording) {
            isRecording = false
            captureButton.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            videoFileOutput.stopRecording()
        } else {
            captureButton.layer.borderColor = UIColor.red.cgColor
            isRecording = true
            let recordingDelegate:AVCaptureFileOutputRecordingDelegate? = self
            
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let filePath = documentsURL.appendingPathComponent(Constants.NEW_VIDEO)
            
            videoFileOutput.startRecording(toOutputFileURL: filePath, recordingDelegate: recordingDelegate)
        }
    }
    
    // Called behind the scenes whenever the picture has been taken
    /*func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?, previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        if let error = error {
            print(error.localizedDescription)
        }
        if let sampleBuffer = photoSampleBuffer, let previewBuffer = previewPhotoSampleBuffer, let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
            
            let image = UIImage(data: dataImage)
    
            cameraSession.stopRunning()
            previewLayer.removeFromSuperlayer()
            //cameraView.contentMode = .scaleAspectFill
            
            // If using the front camera, the photo needs to be flipped
        } else {
            print("Error: photoSampleBuffer")
        }
    }
    
    // Called when the capture button is tapped
    func takePhoto() {
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
                             kCVPixelBufferWidthKey as String: 160,
                             kCVPixelBufferHeightKey as String: 160,
                             ]
        settings.previewPhotoFormat = previewFormat
        //self.sessionOutput.capturePhoto(with: settings, delegate: self)
        //updateButtons(isPhotoCaptured: true)
    }
    
    // Called when the x button is tapped
    func discardPhoto() {
        /*filter.discardOverlays()
        captureSession.startRunning()
        cameraView.layer.addSublayer(previewLayer);
        updateButtons(isPhotoCaptured: false)
        originalPhoto = UIImage()*/
    }
    
    
    func startCameraFromViewController(_ viewController: UIViewController, withDelegate delegate: UIImagePickerControllerDelegate & UINavigationControllerDelegate) -> Bool {
        if UIImagePickerController.isSourceTypeAvailable(.camera) == false {
            return false
        }
        
        let cameraController = UIImagePickerController()
        cameraController.sourceType = .camera
        cameraController.mediaTypes = [kUTTypeMovie as NSString as String]
        cameraController.allowsEditing = false
        cameraController.delegate = delegate
        
        //previewLayer = AVCaptureVideoPreviewLayer(session: captureSession);
        //previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        //previewLayer.connection.videoOrientation = AVCaptureVideoOrientation.portrait;
        
        present(cameraController, animated: false, completion: nil)
        return true
    }
    
    func video(_ videoPath: NSString, didFinishSavingWithError error: NSError?, contextInfo info: AnyObject) {
        var title = "Success"
        var message = "Video was saved"
        if let _ = error {
            title = "Error"
            message = "Video failed to save"
        }
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }*/
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}



// MARK: - UIImagePickerControllerDelegate

/*extension VideoVC: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        dismiss(animated: true, completion: nil)
        // Handle a movie capture
        if mediaType == kUTTypeMovie {
            guard let path = Optional((info[UIImagePickerControllerMediaURL] as! URL).path) else { return }
            if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(path) {
                UISaveVideoAtPathToSavedPhotosAlbum(path, self, #selector(VideoVC.video(_:didFinishSavingWithError:contextInfo:)), nil)
            }
        }
    }
}

// MARK: - UINavigationControllerDelegate

extension VideoVC: UINavigationControllerDelegate {
}*/
