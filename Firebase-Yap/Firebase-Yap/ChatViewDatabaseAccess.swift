//
//  ChatViewDatabaseAccess.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 2/13/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import Foundation
import Firebase
import MapKit
import JSQMessagesViewController

class ChatViewDatabaseAccess {
    private let imageURLNotSetKey = "NOTSET"
    private let ref = FIRDatabase.database().reference(withPath: "conversations")
    fileprivate lazy var storageRef: FIRStorageReference = FIRStorage.storage().reference(forURL: "gs://fir-yap.appspot.com")
    private let conversationRef:FIRDatabaseReference
    private let postsRef:FIRDatabaseReference
    private let chatVC:ChatViewController
    private let conversationID:String
    private var numParticipants = 0
    private var numLikes = 0
    
    init(chatView: ChatViewController) {
        chatVC = chatView
        conversationRef = ref.child(chatView.conversationID)
        postsRef = conversationRef.child("posts")
        conversationID = chatView.conversationID
    }
    
    // Synching with Database
    public func observeMessages() {
        
        postsRef.observe(.value, with: { (snapshot) -> Void in
            self.chatVC.messages.removeAll()
            var newPosts: [Post] = []
            
            for item in snapshot.children {
                let post = Post(snapshot: item as! FIRDataSnapshot)
                newPosts.append(post)
            }
            
            self.chatVC.posts = newPosts
            
            for post in self.chatVC.posts {
                if (post.isText) {
                    self.addMessage(withId: post.userID, name: post.userLocation.description, text: post.content)
                    self.chatVC.finishReceivingMessage()
                } else {
                    let photoURL = post.content
                    let mediaItem = JSQPhotoMediaItem(maskAsOutgoing: true)
                    self.addPhotoMessage(withId: post.userID, key: post.postID, mediaItem: mediaItem!)
                    if photoURL.hasPrefix("gs://") { // if the image is not uploaded to the DB yet
                        self.fetchImageDataAtURL(photoURL, forMediaItem: mediaItem!, clearsPhotoMessageMapOnSuccessForKey: nil)
                    }
                }
            }
            self.chatVC.finishReceivingMessage()
        })
    }
    
    // returns active user count
    public func incrementActiveUsers(userID: String) {
        
        addActiveUserToDatabase(userID: userID)
        
        addConvoIDToUser(userID: userID)
        
        updateNumParticipants()
    }
    
    private func addActiveUserToDatabase(userID: String) {
        let activeRef = conversationRef.child("active_users")
        let activeUserRef = activeRef.child(userID)
        activeUserRef.setValue("")
        activeUserRef.onDisconnectRemoveValue()
    }
    
    private func addConvoIDToUser(userID: String) {
        //adds to user the conversationID
        let userRef = FIRDatabase.database().reference(withPath: "users").child(userID).child("active_conversations")
        let activeConvoRef = userRef.child(conversationID)
        activeConvoRef.setValue("")
        activeConvoRef.onDisconnectRemoveValue()
    }
    
    private func updateNumParticipants() {
        
        let activeRef = conversationRef.child("active_users")
        
        //Updates num_participants
        let numParticipantsRef = conversationRef.child("num_participants")
        activeRef.observeSingleEvent(of: .value, with: { (snapshot) in
            self.numParticipants =  Int(snapshot.childrenCount)
            numParticipantsRef.setValue(self.numParticipants)
            numParticipantsRef.onDisconnectSetValue(self.numParticipants-1)
            self.chatVC.numParticipants = self.numParticipants
        }) { (error) in
            print(error.localizedDescription)
        }
        
    }
    
    // returns num_likes of that post
    public func incrementNumLikes(post: Post) -> Int {
        
        //get reference to post
        let numLikesRef = conversationRef.child("posts").child(post.postID).child("num_likes")
        
        //increment num_likes
        numLikesRef.observeSingleEvent(of: .value, with: { snapshot in
            let valString = snapshot.value
            var value = valString as! Int
            value = value + 1
            numLikesRef.setValue(value)
            self.numLikes = value
        })
        
        //check if user has already liked post
        
        return numLikes
    }
    
    // Add text message
    private func addMessage(withId id: String, name: String, text: String) {
        if let message = JSQMessage(senderId: id, displayName: name, text: text) {
            chatVC.messages.append(message)
        }
    }
    
    // Add photo message
    private func addPhotoMessage(withId id: String, key: String, mediaItem: JSQPhotoMediaItem) {
        if let message = JSQMessage(senderId: id, displayName: "", media: mediaItem) {
            chatVC.messages.append(message)
            
            if (mediaItem.image == nil) {
                chatVC.photoMessageMap[key] = mediaItem
            }
            
            chatVC.collectionView.reloadData()
        }
    }
    
    private func fetchImageDataAtURL(_ photoURL: String, forMediaItem mediaItem: JSQPhotoMediaItem, clearsPhotoMessageMapOnSuccessForKey key: String?) {
        let storageRef = FIRStorage.storage().reference(forURL: photoURL)
        
        storageRef.data(withMaxSize: INT64_MAX){ (data, error) in
            if let error = error {
                print("Error downloading image data: \(error)")
                return
            }
            
            storageRef.metadata(completion: { (metadata, metadataErr) in
                if let error = metadataErr {
                    print("Error downloading metadata: \(error)")
                    return
                }
                
                if (metadata?.contentType == "image/gif") {
                    //mediaItem.image = UIImage.gifWithData(data!)
                    print("ERROR: image/gif")
                } else {
                    mediaItem.image = UIImage.init(data: data!)
                }
                self.chatVC.collectionView.reloadData()

                guard key != nil else {
                    return
                }
                self.chatVC.photoMessageMap.removeValue(forKey: key!)
            })
        }
    }
}
