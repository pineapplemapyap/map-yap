// code from:
// https://www.raywenderlich.com/139322/firebase-tutorial-getting-started-2
/*
 * Copyright (c) 2015 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/*
 Authors: Andrew Cunningham
 Last Updated: 2 Febraury 2017
 Purpose: The User class contains all the information pertaining to the user.
 */

import Foundation
import Firebase

struct User {
    
    // ID of the user
    let userID: String
    
    // ID of the device the user is using
    let deviceID: String
    
    // The number of posts the user posted that were flagged inappropriate
    var numFlagged = 0
    
    // The list of conversations the user favorited
    var savedConversations = [String]()
    
    // all posts user has liked: ConversationID : PostID
    var likedPosts = [String:String]()
    
    init(deviceID: String) {
        self.deviceID = deviceID
        self.userID = deviceID
    }
    
    func toAnyObject() -> Any {
        return [ // conversations empty because they are only called at creation of the user
            "num_flagged" : numFlagged
        ]
    }
}
