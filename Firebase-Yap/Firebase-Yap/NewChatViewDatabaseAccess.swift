//
//  newChatViewDatabaseAccess.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 2/17/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import Foundation
import Firebase
import MapKit

class NewChatViewDatabaseAccess {
    private let imageURLNotSetKey = "NOTSET"
    private let ref = FIRDatabase.database().reference(withPath: "conversations")
    private let usersRef = FIRDatabase.database().reference(withPath: "users")
    fileprivate lazy var storageRef: FIRStorageReference = FIRStorage.storage().reference(forURL: "gs://fir-yap.appspot.com")
    private let conversationRef:FIRDatabaseReference
    private let numParticipantsRef:FIRDatabaseReference
    private let postsRef:FIRDatabaseReference
    private let chatVC:ChatViewCollectionViewController
    private let conversationID:String
    
    init(chatView: ChatViewCollectionViewController, conversationID: String) {
        chatVC = chatView
        conversationRef = ref.child(conversationID)
        postsRef = conversationRef.child("posts")
        self.conversationID = conversationID
        numParticipantsRef = conversationRef.child("num_participants")
    }
    
    // Observe numParticipants
    public func observeNumParticipants() {
        
        numParticipantsRef.observe(.value, with: { snapshot in
            let valString = snapshot.value
            let value = valString as! Int
            self.chatVC.updateNumParticipants(num: value)
        })
    }
    
    // returns num_participants of conversation
    public func incrementNumParticipants(conversationID: String) {
        
        //get reference to post
        let numParticipantsRef = conversationRef.child("num_participants")
        
        //increment num_likes
        numParticipantsRef.observeSingleEvent(of: .value, with: { snapshot in
            let valString = snapshot.value
            var value = valString as! Int
            value = value + 1
            numParticipantsRef.setValue(value)
        })
    }
    
    public func decrementNumParticipants(conversationID: String) {
        
        //get reference to post
        let numParticipantsRef = conversationRef.child("num_participants")
        
        //increment num_likes
        numParticipantsRef.observeSingleEvent(of: .value, with: { snapshot in
            let valString = snapshot.value
            var value = valString as! Int
            value = value - 1
            numParticipantsRef.setValue(value)
        })

    }
    
    // check if the conversation is saved
    public func checkIsSaved(userID: String) {
        
        if Repo.sharedInstance.savedConversations.contains(where: { (conversation) in
            return conversation.conversationID == self.conversationID
        }) {
            self.chatVC.setIsSaved(isSaved: true)
        } else {
            self.chatVC.setIsSaved(isSaved: false)
        }
        
        /*
        let usersRef = FIRDatabase.database().reference(withPath: "users").child(userID).child("saved_conversations")
        usersRef.observeSingleEvent(of: .value, with: { (snapshot) in
            let isSavedConversation = snapshot.hasChild(self.conversationID)
            self.chatVC.setIsSaved(isSaved: isSavedConversation)
        })
         */
    }
    
    // save conversation
    public func saveConversation(userID: String) -> String{
        
        if (Repo.sharedInstance.savedConversations.contains(where: { (conversation) in
            return conversation.conversationID == self.conversationID
        })) {
            return ""
        }
        
        let usersRef = FIRDatabase.database().reference(withPath: "users").child(userID).child("saved_conversations")
        let activeConvoRef = usersRef.child(conversationID)
        activeConvoRef.setValue("")
        self.convertStringtoSavedConversation(conversationID: conversationID)
        
        return conversationID
    }
    
    // unsave conversation
    public func unsaveConversation(userID:String) -> String {
        let usersRef = FIRDatabase.database().reference(withPath: "users").child(userID).child("saved_conversations")
        let activeConvoRef = usersRef.child(conversationID)
        
        // remove conversationID from saved_conversations array
        activeConvoRef.removeValue()
        if let conversationIndex = Repo.sharedInstance.savedConversations.index(where: {
            (conversation) in
            return conversation.conversationID == self.conversationID
        }) {
            Repo.sharedInstance.savedConversations.remove(at: conversationIndex)
        }
        
        
        return conversationID
    }
    
    // Synching with Database
    public func observeMessages() {
        
        postsRef.observe(.value, with: { (snapshot) -> Void in
            var newPosts: [Post] = []
            
            for item in snapshot.children {
                let post = Post(snapshot: item as! FIRDataSnapshot)
                newPosts.append(post)
            }
            
            self.chatVC.makePosts(posts: newPosts)
        })
    }
    
    public func addPhotoMessage(post: Post) {
        if post.content.hasPrefix("gs://") { // if the image is not uploaded to the DB yet
            self.fetchImageDataAtURL(post: post)
        } else {
            self.chatVC.makePhotoPostNotSet(post: post)
        }
    }
    
    // check if post is already like when conversation screen is loaded
    public func checkIsLiked(post: Post) {
        
        // get reference to user likes
        let userRef = usersRef.child(Repo.sharedInstance.userID)
        userRef.observeSingleEvent(of: .value, with: { snapshot in
            let snapshotValue = snapshot.value as! [String: AnyObject]
            if let likedPosts = snapshotValue["liked_posts"] as? [String: AnyObject] {
                if (likedPosts[post.postID] as? [String: AnyObject]) != nil {
                    self.chatVC.setPostIsLocalUserLiked(post: post)
                }
            }
            
        })
    }
    
    // increments num_likes of that post
    public func incrementNumLikes(post: Post) {
        
        //get reference to post
        let numLikesRef = conversationRef.child("posts").child(post.postID).child("num_likes")
        
        //increment num_likes
        numLikesRef.observeSingleEvent(of: .value, with: { snapshot in
            let valString = snapshot.value
            var value = valString as! Int
            value = value + 1
            numLikesRef.setValue(value)
        })
    }
    
    public func decrementNumLikes(post: Post) {
        let numLikesRef = conversationRef.child("posts").child(post.postID).child("num_likes")
        
        //increment num_likes
        numLikesRef.observeSingleEvent(of: .value, with: { snapshot in
            let valString = snapshot.value
            var value = valString as! Int
            value = value - 1
            numLikesRef.setValue(value)
        })
    }
    
    public func flagPost(postID: String, userID: String) {
        
        //get reference to post
        let numFlaggedRef = conversationRef.child("posts").child(postID).child("num_flagged")
        
        //increment num_flagged
        numFlaggedRef.observeSingleEvent(of: .value, with: { snapshot in
            let valString = snapshot.value
            var value = valString as! Int
            value = value + 1
            numFlaggedRef.setValue(value)
        })
        
        flagUser(userID: userID)
    }
    
    private func flagUser(userID: String) {
        let userFlaggedRef = FIRDatabase.database().reference(withPath: "users").child(userID).child("num_flagged")
        
        userFlaggedRef.observeSingleEvent(of: .value, with: { snapshot in
            let valString = snapshot.value
            var value = valString as! Int
            value = value + 1
            userFlaggedRef.setValue(value)
        })
    }
    
    private func fetchImageDataAtURL(post: Post) {
        let photoURL = post.content
        let storageRef = FIRStorage.storage().reference(forURL: photoURL)
       
        storageRef.data(withMaxSize: INT64_MAX){ (data, error) in
            if let error = error {
                print("Error downloading image data: \(error)")
                return
            }
            
            storageRef.metadata(completion: { (metadata, metadataErr) in
                if let error = metadataErr {
                    print("Error downloading metadata: \(error)")
                    return
                }
                
                if (metadata?.contentType == "image/gif") {
                    //UIImage.animatedImageNamed(data?.description, duration: data.)
                    print("ERROR: image/gif")
                } else {
                    let image = UIImage.init(data: data!)
                    self.chatVC.makePhotoPost(image: image!, post: post)
                }
            })
        }
    }
    
    private func convertStringtoSavedConversation(conversationID: String) {
        
        let conversationRef = FIRDatabase.database().reference(withPath: "conversations")
        
        
        conversationRef.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            if snapshot.hasChild(conversationID) {
                let conversationSnapshot = snapshot.childSnapshot(forPath: conversationID)
                let conversation = Conversation(snapshot: conversationSnapshot)
                Repo.sharedInstance.savedConversations.append(conversation)
                Repo.sharedInstance.bubbleImageDictionary[conversationID] = nil
            }
        })
    }
}
