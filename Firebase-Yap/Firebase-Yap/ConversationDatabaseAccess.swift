//
//  ConversationDatabaseAccess.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 3/15/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import Foundation
import Firebase
import AVFoundation

class ConversationDatabaseAccess {
    
    fileprivate let conversationVC:ConversationViewController!
    fileprivate let conversationID:String!
    fileprivate var userID = Repo.sharedInstance.userID
    
    private let ref = FIRDatabase.database().reference(withPath: "conversations")
    fileprivate let usersRef = FIRDatabase.database().reference(withPath: "users")
    fileprivate let numParticipantsRef:FIRDatabaseReference
    fileprivate let conversationRef:FIRDatabaseReference
    fileprivate let postsRef:FIRDatabaseReference
    
    init(vc: ConversationViewController) {
        self.conversationVC = vc
        self.conversationID = vc.conversationID
        conversationRef = ref.child(self.conversationID)
        postsRef = conversationRef.child("posts")
        numParticipantsRef = conversationRef.child("num_participants")
    }
    
    // Observe numParticipants
    public func observeNumParticipants() {
        
        numParticipantsRef.observe(.value, with: { snapshot in
            let valString = snapshot.value
            let value = valString as! Int
            self.conversationVC.updateNumParticipants(value: value)
        })
    }
    
    public func updateSaveConversation(isSaved: Bool) {
        if isSaved {
            saveConversation()
        } else {
            unsaveConversation()
        }
    }
    
    private func saveConversation() {
        let currentUserRef = usersRef.child(self.userID).child("saved_conversations")
        let currentConversationRef = currentUserRef.child(self.conversationID)
        currentConversationRef.setValue("")
        self.convertStringtoSavedConversation(conversationID: self.conversationID)
    }
    
    private func convertStringtoSavedConversation(conversationID: String) {
        self.ref.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            if snapshot.hasChild(conversationID) {
                let conversationSnapshot = snapshot.childSnapshot(forPath: conversationID)
                let conversation = Conversation(snapshot: conversationSnapshot)
                Repo.sharedInstance.savedConversations.append(conversation)
            }
        })
    }
    
    private func unsaveConversation() {
        let currentUserRef = usersRef.child(self.userID).child("saved_conversations").child(self.conversationID)
        
        // remove conversationID from saved_conversations array
        currentUserRef.removeValue()
        
        if let conversationIndex = Repo.sharedInstance.savedConversations.index(where: {
            (conversation) in
            return conversation.conversationID == self.conversationID
        }) {
            Repo.sharedInstance.savedConversations.remove(at: conversationIndex)
            self.conversationVC.updateSavedConversations()
        }
    }
    
    public func incrementNumParticipants() {
        
        //increment num_participants
        numParticipantsRef.observeSingleEvent(of: .value, with: { snapshot in
            let valString = snapshot.value
            var value = valString as! Int
            value = value + 1
            self.numParticipantsRef.setValue(value)
            self.conversationVC.updateNumParticipants(value: value)
        })
    }
    
    public func decrementNumParticipants() {
        
        //increment num_participants
        numParticipantsRef.observeSingleEvent(of: .value, with: { snapshot in
            let valString = snapshot.value
            var value = valString as! Int
            value = value - 1
            self.numParticipantsRef.setValue(value)
            self.conversationVC.updateNumParticipants(value: value)
        })
    }
    
    // Synching with Database
    public func observeMessages() {
        
        postsRef.observe(.value, with: { (snapshot) -> Void in
            var newPosts: [Post] = []
            
            for item in snapshot.children {
                if let post = try? Post(snapshot: item as! FIRDataSnapshot) {
                    newPosts.append(post)
                }
            }
            
            self.conversationVC.makePosts(posts: newPosts)
        })
    }
    
    public func addPhotoMessage(post: Post) {
        if post.content.hasPrefix("gs://") { // if the image is not uploaded to the DB yet
            self.fetchImageDataAtURL(post: post)
        } else {
            self.conversationVC.makePhotoPostNotSet(post: post)
        }
    }
    
    private func fetchImageDataAtURL(post: Post) {
        let photoURL = post.content
        let storageRef = FIRStorage.storage().reference(forURL: photoURL)
        
        storageRef.data(withMaxSize: INT64_MAX){ (data, error) in
            if let error = error {
                print("Error downloading image data: \(error)")
                return
            }
            
            storageRef.metadata(completion: { (metadata, metadataErr) in
                if let error = metadataErr {
                    print("Error downloading metadata: \(error)")
                    return
                }
                
                if (metadata?.contentType == "image/gif") {
                    //UIImage.animatedImageNamed(data?.description, duration: data.)
                    print("ERROR: image/gif")
                } else if (metadata?.contentType == "video/mov") {
                    let grabTime = 1.22
                    let img = self.generateThumnail(url: URL(fileURLWithPath: photoURL), fromTime: Float64(grabTime))
                    if(img != nil) {
                        self.conversationVC.makePhotoPost(image: img!, post: post)
                    }
                } else {
                    let image = UIImage.init(data: data!)
                    self.conversationVC.makePhotoPost(image: image!, post: post)
                }
            })
        }
    }

    fileprivate func generateThumnail(url : URL, fromTime:Float64) -> UIImage? {
        let asset :AVAsset = AVAsset(url: url)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        assetImgGenerate.requestedTimeToleranceAfter = kCMTimeZero;
        assetImgGenerate.requestedTimeToleranceBefore = kCMTimeZero;
        let time        : CMTime = CMTimeMakeWithSeconds(fromTime, 600)
        var img: CGImage?
        do {
            img = try assetImgGenerate.copyCGImage(at:time, actualTime: nil)
        } catch {
        }
        if img != nil {
            let frameImg    : UIImage = UIImage(cgImage: img!)
            return frameImg
        } else {
            return nil
        }
    }
    
    public func flagPost(postID: String, userID: String) {
        
        //get reference to post
        let numFlaggedRef = conversationRef.child("posts").child(postID).child("num_flagged")
        
        //increment num_flagged
        numFlaggedRef.observeSingleEvent(of: .value, with: { snapshot in
            let valString = snapshot.value
            var value = valString as! Int
            value = value + 1
            numFlaggedRef.setValue(value)
        })
        
        flagUser(userID: userID)
    }
    
    private func flagUser(userID: String) {
        let userFlaggedRef = FIRDatabase.database().reference(withPath: "users").child(userID).child("num_flagged")
        
        userFlaggedRef.observeSingleEvent(of: .value, with: { snapshot in
            let valString = snapshot.value
            var value = valString as! Int
            value = value + 1
            userFlaggedRef.setValue(value)
        })
    }
    
    // check if post is already like when conversation screen is loaded
    public func checkIsLiked(post: Post) {
        
        // get reference to user likes
        let userRef = usersRef.child(Repo.sharedInstance.userID)
        userRef.observeSingleEvent(of: .value, with: { snapshot in
            let snapshotValue = snapshot.value as! [String: AnyObject]
            if let likedPosts = snapshotValue["liked_posts"] as? [String: AnyObject] {
                if (likedPosts[post.postID] as? [String: AnyObject]) != nil {
                    //self.conversationVC.setPostIsLocalUserLiked(post: post)
                }
            }
            
        })
    }
    
    // increments num_likes of that post
    public func incrementNumLikes(post: Post) {
        
        //get reference to post
        let numLikesRef = conversationRef.child("posts").child(post.postID).child("num_likes")
        
        //increment num_likes
        numLikesRef.observeSingleEvent(of: .value, with: { snapshot in
            let valString = snapshot.value
            var value = valString as! Int
            value = value + 1
            numLikesRef.setValue(value)
        })
    }
    
    public func decrementNumLikes(post: Post) {
        let numLikesRef = conversationRef.child("posts").child(post.postID).child("num_likes")
        
        //increment num_likes
        numLikesRef.observeSingleEvent(of: .value, with: { snapshot in
            let valString = snapshot.value
            var value = valString as! Int
            value = value - 1
            numLikesRef.setValue(value)
        })
    }

}
