//
//  LocationOverlay.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 3/5/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import UIKit

class LocationOverlay: UIView {
    
    // Our custom view from the XIB file
    @IBOutlet var view: UIView!
    
    // label to display the text
    @IBOutlet weak var locationLabel: UILabel!
    
    // keeps track of the last location where the user dragged the view
    var lastLocation:CGPoint = CGPoint(x: 0.0, y: 0.0)
    
    // Loads a view instance from the xib file - returns: loaded view
    func loadViewFromXibFile() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: Constants.LOCATION_OVERLAY, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    // Initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        
        // Initialization code
        let panRecognizer = UIPanGestureRecognizer(target:self, action: #selector(detectPan))
        self.gestureRecognizers = [panRecognizer]
        
        //randomize view color
        let blueValue = CGFloat(Int(arc4random() % 255)) / 255.0
        let greenValue = CGFloat(Int(arc4random() % 255)) / 255.0
        let redValue = CGFloat(Int(arc4random() % 255)) / 255.0
        
        view.backgroundColor = UIColor(red:redValue, green: greenValue, blue: blueValue, alpha: 1.0)
    }
    
    // Initializer
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    // Sets up the view by loading it from the xib file and setting its frame
    func setupView() {
        view = loadViewFromXibFile()
        view.frame = bounds
        //view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        
        translatesAutoresizingMaskIntoConstraints = false
        
        /// Adds a shadow to our view
        /*view.layer.cornerRadius = 4.0
         view.layer.shadowColor = UIColor.black.cgColor
         view.layer.shadowOpacity = 0.2
         view.layer.shadowRadius = 4.0
         view.layer.shadowOffset = CGSize(width: 0.0, height: 0.8)*/
        
        //visualEffectView.layer.cornerRadius = 4.0
    }
    
    // Updates constraints for the view. Specifies the height and width for the view
    override func updateConstraints() {
        super.updateConstraints()
        
        /*addConstraint(NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 170.0))
         addConstraint(NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 200.0))
         addConstraint(NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0))
         addConstraint(NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0))
         addConstraint(NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0.0))
         addConstraint(NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0.0))*/
    }
    
    // Called from the InputScreen VC, displays the view on top of the camera
    func displayView(onView: UIView, labelText: String) {
        locationLabel.text = NSLocalizedString(labelText, comment: "")
        
        self.alpha = 0.0
        onView.addSubview(self)
        
        onView.addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: onView, attribute: .centerY, multiplier: 1.0, constant: 0)) // move it to the bottom of the screen
        onView.addConstraint(NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: onView, attribute: .centerX, multiplier: 1.0, constant: 0))
        onView.needsUpdateConstraints()
        
        // display the view
        transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.alpha = 1.0
            // Scale while animating
            self.transform = CGAffineTransform.identity
        })
    }
    
    // Hide the view
    func hideView() {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            // Scale while animating
            self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }) { (finished) -> Void in
            self.removeFromSuperview()
        }
    }
    
    func detectPan(recognizer:UIPanGestureRecognizer) {
        let translation  = recognizer.translation(in: self.superview!)
        self.center = CGPoint(x: lastLocation.x + translation.x, y: lastLocation.y + translation.y)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // Promote the touched view
        self.superview?.bringSubview(toFront: self)
        
        // Remember original location
        lastLocation = self.center
    }
}
