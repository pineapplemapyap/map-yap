//
//  InputScreen.swift
//  Firebase-Yap
//
//  Created by Tyler Mulley on 1/27/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Tyler Mulley (Camera Implementation), Jessica Lewis (Filters), Andrew Cunningham (Database)
 Last Updated: 1 March 2017
 Purpose: The InputScreen View Controller is the camera view. This is the screen that appears when the 
 user swipes left on the conversation screen. This screen is part of a tab bar screen, and it always 
 the first tab displayed. The user can also switch to the Text and Gallery screens on the tab bar from 
 this screen. Database access is needed to load the conversation screen while the user swipes right. 
 After the user takes a picture, the photo editing toolbar appears which allows the user to add filters.
 */

import UIKit
import AVFoundation
import Photos

class InputScreen: UIViewController, AVCapturePhotoCaptureDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // References on storyboard
    @IBOutlet weak var cameraView: UIImageView!
    
    // Button to take picture
    @IBOutlet weak var captureButton: UIView!
    
    // Button to discard picture
    @IBOutlet weak var xButton: UIImageView!
    
    // Button to flip camera
    @IBOutlet weak var flipButton: UIImageView!
    
    // Button to send the picture to the database
    @IBOutlet weak var sendButton: UIImageView!
    
    // Toolbar that appears after picture has been taken, can add filtering
    @IBOutlet weak var photoEditingToolbar: UIToolbar!
   
    // Button for applying location
    @IBOutlet weak var locationButton: UIImageView!
    
    // For taking pictures
    var captureSession = AVCaptureSession()
    var sessionOutput = AVCapturePhotoOutput()
    var sessionInput = AVCaptureDeviceInput()
    var sessionOutputSetting = AVCapturePhotoSettings(format: [AVVideoCodecKey:AVVideoCodecJPEG])
    var previewLayer = AVCaptureVideoPreviewLayer()
    let screenSize = UIScreen.main.bounds
    let screenWidth = UIScreen.main.bounds.width
    
    private let sessionQueue = DispatchQueue(label: "session queue", attributes: [], target: nil) // Communicate with the session and other session objects on this queue.
    
    private let videoDeviceDiscoverySession = AVCaptureDeviceDiscoverySession(deviceTypes: [.builtInWideAngleCamera, .builtInDualCamera], mediaType: AVMediaTypeVideo, position: .unspecified)!
    
    // For swipe gesture back to conversation screen
    var panGestureRecognizer = UIPanGestureRecognizer()
    
    // For swiping through filters 
    var swipeLeftGestureRecognizer = UISwipeGestureRecognizer()
    var swipeRightGestureRecognizer = UISwipeGestureRecognizer()
    
    // For swiping up stickers
    var swipeUpGestureRecognizer = UISwipeGestureRecognizer()
    
    // For database
    var databaseAccess:InputScreenDatabaseAccess!
    var postID:String?
    var conversationID = ""
    var conversationTitle = ""
    let userID = ""
    var userLocation = CLLocation()
    var isFromBubble = false
    var userLocationString = "Pittsburgh, PA" // should be set through segue
    
    // To load the chat view controller while swiping
    var posts = [Post]()
    var navBar : UINavigationBar = UINavigationBar()
    var navController = UINavigationController()
    var chatVC:ConversationViewController = ConversationViewController()
    var mapBubble = UIView()
    var tabBar = UITabBar()
    
    // If the camType is front, the picture needs to be flipped
    var camType = Constants.FRONT
    
    // Keeps the original unfiltered photo
    var originalPhoto: UIImage = UIImage()
    
    // Keeps track of the filtered photo
    var filteredPhoto: UIImage = UIImage()
    
    // Class that has filtering functions
    var filter = Filter()
    
    override func viewWillAppear(_ animated: Bool) {
        tabBar = (self.tabBarController?.tabBar)!
        DispatchQueue.global(qos: .background).async {
            _ = StartCamera(captureSession: self.captureSession)
        }

        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set the view to this view
        filter = Filter(view: view)
        
        // Set up the camera
        setupCamera()
        
        //  Set up the DB access
        //databaseAccess = InputScreenDatabaseAccess(inputScreenVC: self)
        
        // load chat screen so when the user swipes back to conversation screen, vc is loaded with conversations
        loadConversationScreen()
        //disableSwipe()
        
        // set up x, camera flip, and send buttons
        setupButtons()
        
        // Add the pan gesture
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(recognizer:)))
        panGestureRecognizer.require(toFail: (self.navController.interactivePopGestureRecognizer)!)
        self.view.addGestureRecognizer(panGestureRecognizer)
        
        // Add the swipe left gesture
        swipeLeftGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.applyFilter(direction:)))
        swipeLeftGestureRecognizer.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeftGestureRecognizer)
        
        // Add the swipe right gesture
        swipeLeftGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.applyFilter(direction:)))
        swipeLeftGestureRecognizer.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeLeftGestureRecognizer)
        
        // Add the swipe up gesture
        swipeUpGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.applyLocation))
        swipeUpGestureRecognizer.direction = UISwipeGestureRecognizerDirection.up
        self.view.addGestureRecognizer(swipeUpGestureRecognizer)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        if(chatVC.isOnChat) {
            chatVC.decrementNumParticipants()
        }
    }
    
    // Loads the conversation screen now so when swiping back, data is already loaded
    func loadConversationScreen() {
        let vc = storyboard?.instantiateViewController(withIdentifier: Constants.CONVERSATION_VIEW) as! ConversationViewController
        Repo.sharedInstance.currentConversationID = self.conversationID
        vc.conversationID = conversationID
        vc.posts = posts
        vc.conversationTitle = self.conversationTitle
        vc.tabBar = (self.tabBarController?.tabBar)!
        vc.navController = self.navController
        vc.captureSession = captureSession
        vc.isFromBubble = self.isFromBubble
        vc.mapBubble = self.mapBubble
        //vc.inputScreen = self.tabBarController as! InputTabBarController
        vc.view.layer.bounds = UIScreen.main.bounds
        self.tabBarController?.view.addSubview(vc.view)
        self.tabBarController?.tabBar.layer.zPosition = -1;
        self.tabBarController?.tabBar.center = CGPoint(x:UIScreen.main.bounds.width * 1.5,y: (self.tabBarController?.tabBar.center.y)!)
        chatVC = vc
        
        let numControllers = self.tabBarController?.viewControllers?.count
        for i in 0...numControllers! - 1 {
            if self.tabBarController?.viewControllers?[i] is TextInputVC {
                let textVC = self.tabBarController?.viewControllers?[i] as! TextInputVC
                textVC.chatVC = vc
            }
        }

    }
    
    // Loads all the buttons for the camera screen
    func setupButtons() {
        // Set up capture button
        captureButton.layer.cornerRadius = captureButton.frame.size.width/2
        captureButton.clipsToBounds = true
        captureButton.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        captureButton.layer.borderWidth = 5.0
        captureButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action:#selector(takePhoto)))
        captureButton.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleButtonPan(gesture:))))
        
        // Set up flip button
        flipButton.image = UIImage(named: Constants.FLIP_CAM_ICON)
        let tapFlipRecognizer = UITapGestureRecognizer(target: self, action: #selector(flipCamera))
        flipButton.isUserInteractionEnabled = true
        flipButton.addGestureRecognizer(tapFlipRecognizer)
        
        // set up 'x' button
        xButton.image = UIImage(named: Constants.X)
        xButton.contentMode = .scaleAspectFill
        let tapXRecognizer = UITapGestureRecognizer(target: self, action: #selector(discardPhoto))
        xButton.isUserInteractionEnabled = true
        xButton.addGestureRecognizer(tapXRecognizer)
        xButton.isHidden = true
        
        // set up send button
        sendButton.contentMode = .scaleAspectFill
        let tapSendRecognizer = UITapGestureRecognizer(target: self, action: #selector(uploadPhoto))
        sendButton.isUserInteractionEnabled = true
        sendButton.addGestureRecognizer(tapSendRecognizer)
        sendButton.isHidden = true
        
        // set up location button
        locationButton.image = UIImage(named: Constants.LOCATION_ICON)
        locationButton.contentMode = .scaleAspectFill
        let tapLocationRecognizer = UITapGestureRecognizer(target: self, action: #selector(applyLocation))
        locationButton.isUserInteractionEnabled = true
        locationButton.addGestureRecognizer(tapLocationRecognizer)
        locationButton.isHidden = true
    }
    
    /* Start: Functions for interating with camera */
    // Called behind the scenes whenever the picture has been taken
    func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?, previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        if let error = error {
            print(error.localizedDescription)
        }
        if let sampleBuffer = photoSampleBuffer, let previewBuffer = previewPhotoSampleBuffer, let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
            
            let image = UIImage(data: dataImage)
            let flippedImage = UIImage(cgImage: (image?.cgImage)!, scale: 1.0, orientation: .leftMirrored)
            
            captureSession.stopRunning()
            previewLayer.removeFromSuperlayer()
            cameraView.contentMode = .scaleAspectFill
            
            // If using the front camera, the photo needs to be flipped
            if(camType == Constants.FRONT) {
                cameraView.image = flippedImage
                originalPhoto = flippedImage
                filteredPhoto = flippedImage
            }
            else {
                cameraView.image = image
                originalPhoto = image!
                filteredPhoto = flippedImage
            }
        } else {
            print("Error: photoSampleBuffer")
        }
    }
    
    // Update the buttons on the camera depending on whether a picture has been taken or not
    func updateButtons(isPhotoCaptured: Bool) {
        // If the photo was just taken, unhide the xButton, sendButton and photoEditing toolbar, otherwise unhide the capture button and flip button
        if (isPhotoCaptured) {
            xButton.isHidden = false
            sendButton.isHidden = false
            locationButton.isHidden = false
            //photoEditingToolbar.isHidden = false
            captureButton.isHidden = true
            flipButton.isHidden = true
            panGestureRecognizer.isEnabled = false
        } else {
            xButton.isHidden = true
            sendButton.isHidden = true
            locationButton.isHidden = true
            photoEditingToolbar.isHidden = true
            captureButton.isHidden = false
            flipButton.isHidden = false
            panGestureRecognizer.isEnabled = true
        }
    }
    
    // Called when the capture button is tapped
    func takePhoto() {
        let settings = AVCapturePhotoSettings()
        let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
        let previewFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
                             kCVPixelBufferWidthKey as String: 160,
                             kCVPixelBufferHeightKey as String: 160,
                             ]
        settings.previewPhotoFormat = previewFormat
        self.sessionOutput.capturePhoto(with: settings, delegate: self)
        updateButtons(isPhotoCaptured: true)
    }
    
    // Called when the x button is tapped
    func discardPhoto() {
        filter.discardOverlays()
        captureSession.startRunning()
        cameraView.layer.addSublayer(previewLayer);
        updateButtons(isPhotoCaptured: false)
        originalPhoto = UIImage()
        filteredPhoto = UIImage()
    }
    
    // Called when the send photo button is tapped
    func uploadPhoto() {
        //print("Bitmap \(filteredPhoto.cgImage?.bitmapInfo)")
        postID = databaseAccess.uploadPhoto(image: filteredPhoto, userID: Repo.sharedInstance.userID, userLocation: Repo.sharedInstance.userLocation)
        
        // If the postID is not nil, the post was sucessful, discard the original photo and animate back to the conversations screen
        if postID != nil {
            discardPhoto()
            //chatVC.animateIn()
        } else {
            print("Unsuccessful post")
        }
    }
    
    var dragStart : CGPoint?
    func handleButtonPan(gesture: UIPanGestureRecognizer) {
        if gesture.state == UIGestureRecognizerState.began {
            let locationInView = gesture.location(in: self.view)
            dragStart = CGPoint(x: locationInView.x - captureButton.center.x, y: locationInView.y - captureButton.center.y)
            
            captureButton.layer.shadowOffset = CGSize(width: 0, height: 5)
            captureButton.layer.shadowOpacity = 0.3
            captureButton.layer.shadowRadius = 2
            
            return
        }
        
        if gesture.state == UIGestureRecognizerState.ended {
            dragStart = nil
            
            captureButton.layer.shadowOffset = CGSize(width: 0, height: 0)
            captureButton.layer.shadowOpacity = 0.0
            captureButton.layer.shadowRadius = 0
            
            return
        }
        
        let locationInView = gesture.location(in: self.view)
        
        UIView.animate(withDuration: 0.1) {
            var locationY = locationInView.y - self.dragStart!.y
            var locationX = locationInView.x - self.dragStart!.x
            
            if ((locationY) - self.captureButton.frame.height / 2.0) < Constants.STATUS_BAR_HEIGHT + Constants.BUBBLE_SNAP_DISTANCE {
                locationY = Constants.STATUS_BAR_HEIGHT + self.captureButton.frame.height / 2.0 + 10.0
            } else if ((locationY) + self.captureButton.frame.height / 2.0) > (self.captureButton.superview?.frame.height)! - Constants.BUBBLE_SNAP_DISTANCE {
                locationY = (self.view.frame.height) - (Constants.STATUS_BAR_HEIGHT + 5.0 + 10.0)
            }
            
            if ((locationX) - self.captureButton.frame.width / 2.0) < Constants.BUBBLE_SNAP_DISTANCE {
                locationX = 0.0 + self.captureButton.frame.width / 2.0 + 10.0
            } else if ((locationX) + self.captureButton.frame.width/2.0) > (self.view.frame.width) - Constants.BUBBLE_SNAP_DISTANCE {
                locationX = (self.view.frame.width) - (self.captureButton.frame.width/2.0 + 10.0)
            }
            self.captureButton.center = CGPoint(x: locationX,
                                  y: locationY)
        }
    }
    
    func flipCamera() {
        xButton.isUserInteractionEnabled = false
        sendButton.isUserInteractionEnabled = false
        locationButton.isUserInteractionEnabled = false
        captureButton.isUserInteractionEnabled = false
        flipButton.isUserInteractionEnabled = false
        //captureModeControl.isEnabled = false
        
        sessionQueue.async { [unowned self] in
            let currentCameraDevice = self.sessionInput.device
            let currentPosition = currentCameraDevice!.position
            
            let preferredPosition: AVCaptureDevicePosition
            let preferredDeviceType: AVCaptureDeviceType
            
            switch currentPosition {
            case .unspecified, .front:
                preferredPosition = .back
                preferredDeviceType = .builtInDualCamera
                self.camType = Constants.BACK
            case .back:
                preferredPosition = .front
                preferredDeviceType = .builtInWideAngleCamera
                self.camType = Constants.FRONT
        }
            
            let devices = self.videoDeviceDiscoverySession.devices!
            var newVideoDevice: AVCaptureDevice? = nil
            
            // First, look for a device with both the preferred position and device type. Otherwise, look for a device with only the preferred position.
            if let device = devices.filter({ $0.position == preferredPosition && $0.deviceType == preferredDeviceType }).first {
                newVideoDevice = device
            }
            else if let device = devices.filter({ $0.position == preferredPosition }).first {
                newVideoDevice = device
            }
            
            if let videoDevice = newVideoDevice {
                do {
                    let videoDeviceInput = try AVCaptureDeviceInput(device: videoDevice)
                    
                    self.captureSession.beginConfiguration()
                    
                    // Remove the existing device input first, since using the front and back camera simultaneously is not supported.
                    self.captureSession.removeInput(self.sessionInput)
                    
                    if self.captureSession.canAddInput(videoDeviceInput) {
                        /*NotificationCenter.default.removeObserver(self, name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: currentCameraDevice!)
                        
                        NotificationCenter.default.addObserver(self, selector: #selector(self.subjectAreaDidChange), name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: videoDeviceInput.device)*/
                        
                        self.captureSession.addInput(videoDeviceInput)
                        self.sessionInput = videoDeviceInput
                    }
                    else {
                        self.captureSession.addInput(self.sessionInput);
                    }
                    
                    /*if let connection = self.movieFileOutput?.connection(withMediaType: AVMediaTypeVideo) {
                        if connection.isVideoStabilizationSupported {
                            connection.preferredVideoStabilizationMode = .auto
                        }
                    }*/
                    
                    /*
                     Set Live Photo capture enabled if it is supported. When changing cameras, the
                     `isLivePhotoCaptureEnabled` property of the AVCapturePhotoOutput gets set to NO when
                     a video device is disconnected from the session. After the new video device is
                     added to the session, re-enable Live Photo capture on the AVCapturePhotoOutput if it is supported.
                     */
                    self.sessionOutput.isLivePhotoCaptureEnabled = self.sessionOutput.isLivePhotoCaptureSupported;
                    
                    self.captureSession.commitConfiguration()
                }
                catch {
                    print("Error occured while creating video device input: \(error)")
                }
            }
            
            DispatchQueue.main.async { [unowned self] in
                self.xButton.isUserInteractionEnabled = true
                self.sendButton.isUserInteractionEnabled = true
                self.locationButton.isUserInteractionEnabled = true
                self.captureButton.isUserInteractionEnabled = true
                self.flipButton.isUserInteractionEnabled = true
                //self.captureModeControl.isEnabled = true
            }
        }
    }
    
    func setupCamera() {
        do {
            var defaultVideoDevice: AVCaptureDevice?
            
            // Choose the back dual camera if available, otherwise default to a wide angle camera.
            if let dualCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInDualCamera, mediaType: AVMediaTypeVideo, position: .back) {
                defaultVideoDevice = dualCameraDevice
                camType = Constants.BACK
            }
            else if let backCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .back) {
                // If the back dual camera is not available, default to the back wide angle camera.
                defaultVideoDevice = backCameraDevice
                camType = Constants.BACK
            }
            else if let frontCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .front) {
                // In some cases where users break their phones, the back wide angle camera is not available. In this case, we should default to the front wide angle camera.
                defaultVideoDevice = frontCameraDevice
                camType = Constants.FRONT
            }
            
            let videoDeviceInput = try AVCaptureDeviceInput(device: defaultVideoDevice)
            
            if captureSession.canAddInput(videoDeviceInput) {
                captureSession.addInput(videoDeviceInput)
                self.sessionInput = videoDeviceInput
                
                previewLayer.bounds = self.cameraView.frame
                self.cameraView.layer.bounds = self.cameraView.frame
                
                self.cameraView.layer.masksToBounds = true
                self.previewLayer.masksToBounds = true
                
                self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession);
                self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
                self.previewLayer.connection.videoOrientation = AVCaptureVideoOrientation.portrait;

                self.cameraView.layer.addSublayer(self.previewLayer);
                
               /* DispatchQueue.main.async {
                    /*
                     Why are we dispatching this to the main queue?
                     Because AVCaptureVideoPreviewLayer is the backing layer for PreviewView and UIView
                     can only be manipulated on the main thread.
                     Note: As an exception to the above rule, it is not necessary to serialize video orientation changes
                     on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
                     
                     Use the status bar orientation as the initial video orientation. Subsequent orientation changes are
                     handled by CameraViewController.viewWillTransition(to:with:).
                     */
                    //let statusBarOrientation = UIApplication.shared.statusBarOrientation
                    //var initialVideoOrientation: AVCaptureVideoOrientation = .portrait
                    //initialVideoOrientation = .portrait
                    /*if statusBarOrientation != .unknown {
                        if let videoOrientation = statusBarOrientation.videoOrientation {
                            initialVideoOrientation = videoOrientation
                        }
                    }*/
                    
                    self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession);
                    self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
                    self.previewLayer.connection.videoOrientation = AVCaptureVideoOrientation.portrait;
                    
                    self.cameraView.layer.addSublayer(self.previewLayer);
                    
                    //self.previewLayer.connection.videoOrientation = initialVideoOrientation
                }*/
            }
            else {
                print("Could not add video device input to the session")
                //setupResult = .configurationFailed
                //session.commitConfiguration()
                return
            }
        }
        catch {
            print("Could not create video device input: \(error)")
            //setupResult = .configurationFailed
            //session.commitConfiguration()
            return
        }
        
        // Add photo output.
        if captureSession.canAddOutput(sessionOutput)
        {
            captureSession.addOutput(sessionOutput)
            
            sessionOutput.isHighResolutionCaptureEnabled = true
            sessionOutput.isLivePhotoCaptureEnabled = sessionOutput.isLivePhotoCaptureSupported
            //livePhotoMode = sessionOutput.isLivePhotoCaptureSupported ? .on : .off
        }
        else {
            print("Could not add photo output to the session")
            //setupResult = .configurationFailed
            //session.commitConfiguration()
            return
        }
    }
    
    /* Start: Functions for applying filters */
    func applyFilter(direction: UISwipeGestureRecognizerDirection) {
        let filteredImage = filter.applyFilter(originalPhoto: originalPhoto, direction: direction, camType: camType)
        cameraView.image = filteredImage
        filteredPhoto = filteredImage
        //filterOverlayView.displayView(onView: view, labelText: filter.getCurrentFilterName())
    }
    
    func applySpeed() {
        filter.applySpeed(originalPhoto: originalPhoto)
    }
    
    func applyWeather() {
        filter.applyWeather(originalPhoto: originalPhoto)
    }
    
    func applyLocation() {
        filter.applyLocation(originalPhoto: originalPhoto, userLocationString: userLocationString)
        //locationOverlayView.displayView(onView: view, labelText: userLocationString)
        locationButton.isHighlighted = true
    }
    
    func applyTime() {
        filter.applyTime(originalPhoto: originalPhoto)
    }
    /* End: Functions for applying filters */
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        captureSession.stopRunning()
        UIView.animate(withDuration: 0.2, animations: {
            self.tabBarController?.tabBar.frame = CGRect(origin: CGPoint(x: 0,y: UIScreen.main.bounds.height - (self.tabBarController?.tabBar.frame.height)!), size: CGSize(width: (self.tabBarController?.tabBar.frame.width)!, height: (self.tabBarController?.tabBar.frame.height)!))
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        previewLayer.frame = cameraView.bounds
    }
    
    // Switching between camera and conversation view
    func handlePan(recognizer: UIPanGestureRecognizer) {
        if (recognizer.state == UIGestureRecognizerState.ended) {
            if (self.chatVC.view!.center.x <= 0) {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                    //self.chatVC.collectionView?.backgroundColor = UIColor.clear
                    self.chatVC.view.backgroundColor = UIColor.clear
                    self.chatVC.view!.center = CGPoint(x: self.screenWidth * -0.5, y: self.chatVC.view!.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth/2 , y: self.tabBar.center.y)},
                               completion: { finished in
                                self.chatVC.isOnChat = false
                                self.disableSwipe()
                                self.chatVC.isMessagesHidden = true
                                //self.chatVC.collectionView?.reloadData()
                                self.chatVC.showNewPosts()
                })
            }
            else {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.chatVC.view!.center = CGPoint(x: UIScreen.main.bounds.width * 0.5, y: self.chatVC.view!.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth * 1.5 , y: self.tabBar.center.y)
                },
                               completion: { finished in
                                self.captureSession.stopRunning()
                                self.chatVC.isOnChat = true
                                self.enableSwipe()
                                self.chatVC.isMessagesHidden = false
                                self.chatVC.showNewPosts()
                })
            }
        }
        
        if recognizer.state == .began || recognizer.state == .changed {
            let translation = recognizer.translation(in: chatVC.view!)
            print(translation.x)
            
            if (translation.x > 40) {
                
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.chatVC.view!.center = CGPoint(x: self.screenWidth * 0.5, y: self.chatVC.view!.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth * 1.5 , y: self.tabBar.center.y)
                },
                               completion: { finished in
                                self.captureSession.stopRunning()
                                self.chatVC.isOnChat = true
                                self.enableSwipe()
                                self.chatVC.isMessagesHidden = false
                                //self.chatVC.collectionView?.reloadData()
                                self.chatVC.showNewPosts()
                                self.chatVC.backgroundColor  = self.chatVC.backgroundColor
                })
            }
            else if (translation.x < -40) {
                
                DispatchQueue.global(qos: .background).async {
                    _ = StartCamera(captureSession: self.captureSession)
                }
            
                self.disableSwipe()
                
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                    //self.chatVC.collectionView?.backgroundColor = UIColor.clear
                    self.chatVC.view.backgroundColor = UIColor.clear
                    self.chatVC.view!.center = CGPoint(x: self.screenWidth * -0.5, y: self.chatVC.view!.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth/2 , y: self.tabBar.center.y)},
                               completion: { finished in
                                self.chatVC.isOnChat = false
                                self.chatVC.isMessagesHidden = true
                                //self.chatVC.collectionView?.reloadData()
                                self.chatVC.showNewPosts()
                })
            }
            else if((translation.x < 0 || translation.x > 0) && tabBar.center.x >= self.screenWidth * 0.5 && (chatVC.view?.center.x)! <= self.screenWidth * 0.5) {
            
                DispatchQueue.global(qos: .background).async {
                    _ = StartCamera(captureSession: self.captureSession)
                }
                
                chatVC.view?.backgroundColor = chatVC.backgroundColor
                chatVC.view.backgroundColor = chatVC.backgroundColor
                chatVC.view!.center = CGPoint(x: chatVC.view!.center.x + translation.x, y: chatVC.view!.center.y)
                tabBar.center = CGPoint(x: tabBar.center.x + translation.x, y: tabBar.center.y)
                recognizer.setTranslation(CGPoint.zero, in: chatVC.view!)
            }
            
        }
    }
    
    func disableSwipe() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    func enableSwipe() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
}
