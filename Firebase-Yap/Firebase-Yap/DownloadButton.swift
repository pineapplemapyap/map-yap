//
//  DownloadButton.swift
//  RecordButton
//
//  Created by Jessica Lewis on 3/28/17.
//  Copyright © 2017 Jessica Lewis. All rights reserved.
//

import UIKit

class DownloadButton: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupButton()
    }
    
    internal func setupButton() {
        self.image = #imageLiteral(resourceName: "download")
        self.isHidden = true
        self.isUserInteractionEnabled = false
        self.contentMode = .scaleAspectFill
    }
    
    func setButtonImage(image: UIImage) {
        self.image = image
    }
}
