//
//  TileView.swift
//  TileAnimTest
//
//  Created by Tyler Mulley on 12/20/16.
//  Copyright © 2016 Tyler Mulley. All rights reserved.
//
/*
 Authors: Tyler Mulley
 Last Updated: 3 February 2017
 Purpose: This file implements the functionality of the tile screen.
 */

import UIKit


class TileScreen: UICollectionViewController, UICollectionViewDelegateFlowLayout{
    
    let width = UIScreen.main.bounds.width
    let height = UIScreen.main.bounds.height
    var tileSquareWidth : Double = 0
    var tileRectWidth : Double = 0
    var tileSmallWidth : Double = 0
    var tileHeight : Double = 0
    var numTiles : Int = 0
    var row : Int = 0
    var isSucking : Bool = false
    var imageDictionary = [String: UIImage?]()
    var navController = UINavigationController()
    
    var mapBubble = UIView()
    
    var gestureRecognizer = UIGestureRecognizer()
    var pinchGesture = UIPinchGestureRecognizer()
    var swipeRightGestureRecognizer = UISwipeGestureRecognizer()
    
    var databaseAccess:TileScreenDatabaseAccess!
    var conversations = [Conversation]()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tileSquareWidth = Double((self.width/2.0) - 12.0)
        tileSmallWidth = Double((self.width/4.0) - 8.0)
        tileRectWidth = Double((self.width) - 22.0)
        tileHeight = Double(((self.height)/3) - (48.0/3))
        
        // add gesture for collection view tiles
        //let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.suckInTiles))
        pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(self.suckInTiles))
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(pinchGesture)
        
        swipeRightGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.dismissTiles))
        swipeRightGestureRecognizer.direction = UISwipeGestureRecognizerDirection.right
        self.collectionView?.addGestureRecognizer(swipeRightGestureRecognizer)
        
        self.addMapBubble(bubble: Bubble())
        
        
//        let chatCollectionView = storyboard?.instantiateViewController(withIdentifier: "ChatCollectionView") as! ChatViewCollectionViewController
//        
//        imageDictionary = chatCollectionView.getImageDictionary()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //Repo.sharedInstance.mapVC.reloadConversations()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        databaseAccess = TileScreenDatabaseAccess(tileScreen: self)
        self.collectionView?.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Pulls tiles into the middle of the screen and releases them back
    func suckInTiles(_ sender: Any) {
        if(!isSucking) {
            isSucking = true
            let location = (sender as AnyObject).location(in: view)
            let tiles : [UICollectionViewCell] = (self.collectionView?.visibleCells)!
            
            for i in 0...(numTiles - 1) {
                let xDiff = location.x - tiles[i].layer.position.x
                let yDiff = location.y - tiles[i].layer.position.y
                tiles[i].layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                    tiles[i].transform = tiles[i].transform.translatedBy(x: xDiff, y: yDiff)
                    tiles[i].transform = tiles[i].transform.scaledBy(x: 0.001, y: 0.001)
                },
                               completion: { finished in
                                UIView.animate(withDuration: 0.5, delay: 0.1, options: .curveEaseInOut, animations: {
                                    tiles[i].transform = CGAffineTransform.identity
                                },
                                           completion: { finished in
                                            self.isSucking = false
                                })
                })
            }
        }
    }
    
    // This function scales the tiles to the screen size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(indexPath.row == 0) {
            return CGSize(width: self.tileRectWidth, height: self.tileHeight)
        }
        else if (indexPath.row > 2) {
            return CGSize(width: self.tileSmallWidth, height: self.tileHeight)
        }
        else {
            return CGSize(width: self.tileSquareWidth, height: self.tileHeight)
        }
        
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numTiles
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tile", for: indexPath) as! TileCellSquare
        
        cell.originalCenter = cell.center
        cell.row = indexPath.row
        cell.index = indexPath
        
        cell.tileImageView.image = conversations[indexPath.row].tileImage
        
        cell.activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0.0, y: 0.0, width: cell.frame.width, height: cell.frame.height))
        cell.activityIndicator.hidesWhenStopped = true
        cell.activityIndicator.color = UIColor.white
        cell.addSubview(cell.activityIndicator)
        
        for post in conversations[indexPath.row].posts {
            if !post.isText {
                if let image = imageDictionary[post.postID] {
                    cell.tileImageView.image = image!
                }
            }
        }
        
        if((cell.tileImageView.image?.size.width)! > 0.0) {
            cell.activityIndicator.stopAnimating()
        }
        else {
            cell.activityIndicator.startAnimating()

        }
        
        cell.layer.cornerRadius = 3
                cell.tileImageView.contentMode = .scaleAspectFill
        
        cell.eyeImageView.image = UIImage(named:"eye")
    
        cell.titleLabel.layer.shadowColor = UIColor.black.cgColor
        cell.titleLabel.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        cell.titleLabel.layer.shadowOpacity = 1.0
        cell.titleLabel.layer.shadowRadius = 1.0
        cell.titleLabel.layer.backgroundColor = UIColor.clear.cgColor
        cell.titleLabel.text = conversations[indexPath.row].title
        
        cell.numParticipantsLabel.layer.shadowColor = UIColor.black.cgColor
        cell.numParticipantsLabel.layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        cell.numParticipantsLabel.layer.shadowOpacity = 1.0
        cell.numParticipantsLabel.layer.shadowRadius = 1.0
        cell.numParticipantsLabel.layer.backgroundColor = UIColor.clear.cgColor
        cell.numParticipantsLabel.text = String(conversations[indexPath.row].numParticipants)
        
        
        gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handleSwipe(recognizer:)))
        cell.addGestureRecognizer(gestureRecognizer)
        gestureRecognizer.require(toFail: pinchGesture)
        gestureRecognizer.require(toFail: swipeRightGestureRecognizer)

        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(goToInput(recognizer:)))
        cell.addGestureRecognizer(tapGestureRecognizer)
        
        if (self.navigationController != nil) {
            gestureRecognizer.require(toFail: (self.navigationController?.interactivePopGestureRecognizer)!)
        }
        
        
        return cell
    }
    
    func dismissTiles() {
        print("here")
        UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 5.0, initialSpringVelocity: 8.0, options: .curveEaseIn, animations: {
            self.view.frame.origin.x = self.view.frame.width
        }, completion: { finished in self.view.removeFromSuperview() })
        
    }
    func goToInput(recognizer: UITapGestureRecognizer) {
        let inputScreen = storyboard?.instantiateViewController(withIdentifier: "inputScreen") as! CameraViewController
        let cellClicked = recognizer.view as! TileCellSquare
        
        //Pass the selected object to the new view controller.
        inputScreen.conversationID = conversations[cellClicked.row].conversationID
        inputScreen.conversationTitle = conversations[cellClicked.row].title
        inputScreen.isFromBubble = false
        //inputScreen.navigationItem.setHidesBackButton(true, animated:true);
        inputScreen.navController = self.navController
        inputScreen.tabBarController?.selectedIndex = 0
        inputScreen.mapBubble = self.mapBubble
        
        //navController.pushViewController(inputScreen, animated: true)
        self.present(inputScreen, animated: true, completion: nil)
        //inputScreen.view.frame.origin.x = self.view.frame.width - 100
        //self.collectionView?.addSubview(inputScreen.view)
        
        UIView.animate(withDuration: 0.6, delay: 0.0, usingSpringWithDamping: 5.0, initialSpringVelocity: 8.0, options: .curveEaseIn, animations: {
            inputScreen.view.frame.origin.x = self.view.frame.origin.x
            self.mapBubble.frame.origin.x -= self.mapBubble.frame.width
        }, completion: nil)
        
        
    }
    
    func handleSwipe(recognizer: UIPanGestureRecognizer) {
        let savedCell = recognizer.view as! TileCellSquare
        if (recognizer.state == UIGestureRecognizerState.ended && !savedCell.swiped) {
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                  recognizer.view!.center = savedCell.originalCenter
            })
        }
        
        if recognizer.state == .began || recognizer.state == .changed {
            let translation = recognizer.translation(in: self.view)
            let vel = recognizer.velocity(in: self.collectionView!)
            let velLen = (sqrt(vel.x * vel.x + vel.y * vel.y))
            print(velLen)
            if (velLen > 1500) {
                savedCell.swiped = true

                UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseInOut, animations: {
                    recognizer.view!.center = CGPoint(x: recognizer.view!.center.x  + vel.x/8, y: recognizer.view!.center.y + vel.y/8)
                }, completion: { finished in
                    savedCell.removeFromSuperview()
                    savedCell.center = savedCell.originalCenter
                    self.resetArrays(swipedIndex: savedCell.index)
                    savedCell.swiped = false
                    
                })
            }
        }
    }
    
    func openMap() {
        self.dismissTiles()
    }
    
    func resetArrays(swipedIndex: IndexPath) {
        let swipedIndexRow = swipedIndex.row
        
        var lastIndex : Int = 0
        
        if (self.conversations.count > 7) {
            lastIndex = 7
        }
        else {
            lastIndex = self.conversations.count - 1
        }
        
        self.conversations.append(self.conversations[swipedIndexRow])
        self.conversations[swipedIndexRow] = self.conversations[lastIndex]
        self.conversations.remove(at: lastIndex)
        self.collectionView!.reloadItems(at: [swipedIndex])

        //
        let cell = collectionView!.cellForItem(at: swipedIndex) as! TileCellSquare
        cell.transform = cell.transform.scaledBy(x: 0.1, y: 0.1)
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut,
                       animations: {
                        cell.transform = CGAffineTransform.identity
        },
                       completion: { finished in
                        //print("scaled up")
        })
    }
    
    func addMapBubble(bubble: Bubble) {
        let mapBubble = IconBubble()
        mapBubble.setImage(image: UIImage(named: "clearView.png")!, color: Constants.BLUE)
        mapBubble.setupViewController(tileVC: self)
        mapBubble.isMap = true
        let mapWidth = mapBubble.frame.width/1.5
        let mapHeight = mapBubble.frame.height/1.5
        let mapImageView = UIImageView(frame: CGRect(x: (bubble.frame.width - mapWidth)/2.0, y: (bubble.frame.height - mapHeight)/2.0 - 7.0, width: mapWidth, height: mapHeight))
        mapImageView.image = #imageLiteral(resourceName: "map-2")
        mapImageView.clipsToBounds = true
        mapBubble.addSubview(mapImageView)
        self.view.addSubview(mapBubble)
        mapBubble.center = CGPoint(x: mapBubble.frame.width/2.0, y: self.view.frame.height/2.0)
        self.mapBubble = mapBubble
    }
}
