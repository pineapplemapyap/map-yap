//
//  FilterNameOverlay.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 2/26/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Jessica Lewis
 Last Updated: 2 March 2017
 Purpose: The Filter overlay view is a view with only a text label that is used to display the name of the filter to the user.
 */

import UIKit

class FilterNameOverlay: UIView {

    // Our custom view from the XIB file
    var view: UIView!
    
    /// Visual effectView for having a blurry background
    //@IBOutlet weak var visualEffectView: UIVisualEffectView!
    
    /// Label to show the label text
    @IBOutlet weak var titleLabel: UILabel!
    
    // Loads a view instance from the xib file - returns: loaded view
    func loadViewFromXibFile() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: Constants.FILTER_NAME_OVERLAY, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }

    // Initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    // Initializer
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    // Sets up the view by loading it from the xib file and setting its frame
    func setupView() {
        view = loadViewFromXibFile()
        view.frame = bounds
        //view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        
        translatesAutoresizingMaskIntoConstraints = false
        
        /// Adds a shadow to our view
        /*view.layer.cornerRadius = 4.0
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.2
        view.layer.shadowRadius = 4.0
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.8)*/
        
        //visualEffectView.layer.cornerRadius = 4.0
    }
    
    // Updates constraints for the view. Specifies the height and width for the view
    override func updateConstraints() {
        super.updateConstraints()
        
        /*addConstraint(NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 170.0))
        addConstraint(NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 200.0))
        addConstraint(NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0))
        addConstraint(NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0))
        addConstraint(NSLayoutConstraint(item: view, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0.0))
        addConstraint(NSLayoutConstraint(item: view, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0.0))*/
    }

    // Called from the InputScreen VC, displays the view on top of the camera
    func displayView(onView: UIView, labelText: String) {
        titleLabel.text = NSLocalizedString(labelText, comment: "")
        
        self.alpha = 0.0
        onView.addSubview(self)
        
        onView.addConstraint(NSLayoutConstraint(item: self, attribute: .centerY, relatedBy: .equal, toItem: onView, attribute: .centerY, multiplier: 1.0, constant: 300.0)) // move it to the bottom of the screen
        onView.addConstraint(NSLayoutConstraint(item: self, attribute: .centerX, relatedBy: .equal, toItem: onView, attribute: .centerX, multiplier: 1.0, constant: 0))
        onView.needsUpdateConstraints()
        
        // display the view
        transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.alpha = 1.0
            // Scale while animating
            //self.transform = CGAffineTransform.identity
        }) { (finished) -> Void in
            // When finished wait 1.5 seconds, than hide it
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                self.hideView()
            }
        }
    }
    
    // Hide the view
    private func hideView() {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            // Scale while animating
            //self.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        }) { (finished) -> Void in
            self.removeFromSuperview()
        }
    }
}
