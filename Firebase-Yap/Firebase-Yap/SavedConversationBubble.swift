//
//  SavedConversationBubble.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 3/16/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import UIKit

class SavedConversationBubble: Bubble {
    
    var conversationVC = ConversationViewController()
    var conversationID = ""
    
    override func handleTap(gesture: UITapGestureRecognizer) {
        super.handleTap(gesture: gesture)
        
        conversationVC.bubbleTapped(conversationID: self.conversationID)
    }
    
    override func handlePan(gesture: UIPanGestureRecognizer!) {
    }
    
    func setupViewController(conversationVC: ConversationViewController) {
        self.conversationVC = conversationVC
        conversationID = conversationVC.conversationID
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
