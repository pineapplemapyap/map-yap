//
//  InputTabBarDatabaseAccess.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 2/16/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import Foundation
import Firebase
import MapKit

class InputTabBarDatabaseAccess {
    
    private let ref = FIRDatabase.database().reference(withPath: "conversations")
    private let conversationRef: FIRDatabaseReference
    private let inputTabBarVC:InputTabBarController
    private var conversationID: String
    private var numParticipants: Int!
    private var isSavedConversation: Bool!
    
    init(inputTabBarVC: InputTabBarController) {
        self.inputTabBarVC = inputTabBarVC
        conversationRef = ref.child(self.inputTabBarVC.conversationID)
        conversationID = Repo.sharedInstance.currentConversationID
    }
    
    public func getNumParticipants(){
        conversationID = Repo.sharedInstance.currentConversationID
        let numParticipantsRef = conversationRef.child("num_participants")
        numParticipantsRef.observe(.value, with: { (snapshot) in
            self.numParticipants = snapshot.value as! Int
            self.inputTabBarVC.updateNumParticipantsLabel(numParticipants: self.numParticipants)
        })
    }
    
    public func isSavedConversation(userID: String) {
        conversationID = Repo.sharedInstance.currentConversationID
        let usersRef = FIRDatabase.database().reference(withPath: "users").child(userID).child("saved_conversations")
        usersRef.observeSingleEvent(of: .value, with: { (snapshot) in
            let isSavedConversation = snapshot.hasChild(self.conversationID)
            self.inputTabBarVC.setUpFavoritesIcon(isSaved: isSavedConversation)
        })
    }
    
    public func saveConversation(userID: String) -> String? {
        conversationID = Repo.sharedInstance.currentConversationID
        let usersRef = FIRDatabase.database().reference(withPath: "users").child(userID).child("saved_conversations")
        let activeConvoRef = usersRef.child(conversationID)
        activeConvoRef.setValue("")
        
        return conversationID
    }
    
    public func unsaveConversation(userID: String) -> String? {
        conversationID = Repo.sharedInstance.currentConversationID
        let usersRef = FIRDatabase.database().reference(withPath: "users").child(userID).child("saved_conversations")
        let activeConvoRef = usersRef.child(conversationID)
        
        // remove conversationID from saved_conversations array
        activeConvoRef.removeValue()
        
        return conversationID
    }
}
