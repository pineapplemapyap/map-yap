//
//  ViewController.swift
//  RecordButton
//
//  Created by Jessica Lewis on 3/27/17.
//  Copyright © 2017 Jessica Lewis. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Photos

class CameraViewController: UIViewController, AVCapturePhotoCaptureDelegate, AKPickerViewDataSource, AKPickerViewDelegate, AVCaptureFileOutputRecordingDelegate, AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    // MARK: View Controller Life Cycle
    
    // To load the chat view controller while swiping
    var chatVC:ConversationViewController = ConversationViewController()
    var conversationID = ""
    var conversationTitle = ""
    var navTitle = ""
    var posts = [Post]()
    var navBar = UINavigationBar()
    var navController = UINavigationController()
    var databaseAccess:InputScreenDatabaseAccess!
    var isFromBubble = false
    var mapBubble = UIView()
    let screenSize = UIScreen.main.bounds
    let screenWidth = UIScreen.main.bounds.width
    
    // Gestures
    var tapToFocusGesture : UITapGestureRecognizer = UITapGestureRecognizer()
    var pinchToZoomGesture : UIPinchGestureRecognizer = UIPinchGestureRecognizer()
    var panToConversationsGesture = UIPanGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // load chat screen so when the user swipes back to conversation screen, vc is loaded with conversations
        loadConversationScreen()
        
        //  Set up the DB access
        databaseAccess = InputScreenDatabaseAccess(inputScreenVC: self)
        
        setupButtons()
        setupVoiceRecording()
        
        // For haptics in AKPicker
        feedbackGenerator = UISelectionFeedbackGenerator()
        
        videoPlayerLayer = AVPlayerLayer(player: videoQueuePlayer)
        videoPlayerLayer.frame = self.view.bounds
        self.cameraView.layer.addSublayer((videoPlayerLayer))
        
        panToConversationsGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePan(recognizer:)))
        self.view.addGestureRecognizer(panToConversationsGesture)
        
        // Disable UI. The UI is enabled if and only if the session starts running.
        disableUI()
        
        // Set up the video preview view.
        cameraView.session = session
        
        /*
         Check video authorization status. Video access is required and audio
         access is optional. If audio access is denied, audio is not recorded
         during movie recording.
         */
        switch AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) {
        case .authorized:
            // The user has previously granted access to the camera.
            break
            
        case .notDetermined:
            /*
             The user has not yet been presented with the option to grant
             video access. We suspend the session queue to delay session
             setup until the access request has completed.
             
             Note that audio access will be implicitly requested when we
             create an AVCaptureDeviceInput for audio during session setup.
             */
            sessionQueue.suspend()
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { [unowned self] granted in
                if !granted {
                    self.setupResult = .notAuthorized
                }
                self.sessionQueue.resume()
            })
            
        default:
            // The user has previously denied access.
            setupResult = .notAuthorized
        }
        
        /*
         Setup the capture session.
         In general it is not safe to mutate an AVCaptureSession or any of its
         inputs, outputs, or connections from multiple threads at the same time.
         
         Why not do all of this on the main queue?
         Because AVCaptureSession.startRunning() is a blocking call which can
         take a long time. We dispatch session setup to the sessionQueue so
         that the main queue isn't blocked, which keeps the UI responsive.
         */
        sessionQueue.async { [unowned self] in
            self.configureSession()
        }
        
        
        tapToFocusGesture = UITapGestureRecognizer(target: self, action: #selector(focusAndExposeTap(gestureRecognizer:)))
        self.view.addGestureRecognizer(tapToFocusGesture)
        
        pinchToZoomGesture = UIPinchGestureRecognizer(target: self, action: #selector(zoomOnCamera(gestureRecognizer:)))
        self.view.addGestureRecognizer(pinchToZoomGesture)
    }
    
    // Switching between camera and conversation view
    func handlePan(recognizer: UIPanGestureRecognizer) {
        if (recognizer.state == UIGestureRecognizerState.ended) {
            if (self.chatVC.view!.center.x <= 0) {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                    //self.chatVC.collectionView?.backgroundColor = UIColor.clear
                    self.chatVC.view.backgroundColor = UIColor.clear
                    self.chatVC.view!.center = CGPoint(x: self.screenWidth * -0.5, y: self.chatVC.view!.center.y)
                    self.chatVC.isOnChat = false
                    self.disableSwipe()
                    self.chatVC.isMessagesHidden = true
                    //self.chatVC.collectionView?.reloadData()
                    self.chatVC.showNewPosts()
                    //self.session.startRunning()
                })
            }
            else {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.chatVC.view!.center = CGPoint(x: self.view.bounds.width * 0.5, y: self.chatVC.view!.center.y)
                    },
                   completion: { finished in
                        self.session.stopRunning()
                        self.chatVC.isOnChat = true
                        self.enableSwipe()
                        self.chatVC.isMessagesHidden = false
                        self.chatVC.showNewPosts()
                })
            }
        }
        
        if recognizer.state == .began || recognizer.state == .changed {
            let translation = recognizer.translation(in: chatVC.view!)
            print(translation.x)
            // swipe back to conversation
            if (translation.x > 40) {
                
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.chatVC.view!.center = CGPoint(x: self.screenWidth * 0.5, y: self.chatVC.view!.center.y)
                },
                       completion: { finished in
                        self.session.stopRunning()
                        self.chatVC.isOnChat = true
                        self.enableSwipe()
                        self.chatVC.isMessagesHidden = false
                        //self.chatVC.collectionView?.reloadData()
                        self.chatVC.showNewPosts()
                        self.chatVC.backgroundColor  = self.chatVC.backgroundColor
                })
            }
            else if (translation.x < -40) {
                DispatchQueue.global(qos: .background).async {
                    //_ = StartCamera(captureSession: self.captureSession)
                    //self.configureSession()
                    self.cameraView.session?.startRunning()
                }
                
                self.disableSwipe()
                
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                    //self.chatVC.collectionView?.backgroundColor = UIColor.clear
                    self.chatVC.view.backgroundColor = UIColor.clear
                    self.chatVC.view!.center = CGPoint(x: self.screenWidth * -0.5, y: self.chatVC.view!.center.y)
                    self.chatVC.isOnChat = false
                    self.chatVC.isMessagesHidden = true
                    //self.chatVC.collectionView?.reloadData()
                    self.chatVC.showNewPosts()
                })
            }
            // swipe back to conversation
            else if((translation.x < 0 || translation.x > 0) /*&& cameraView.center.x >= self.screenWidth * 0.5*/ && (chatVC.view?.center.x)! <= self.screenWidth * 0.5) {
                
                DispatchQueue.global(qos: .background).async {
                    //_ = StartCamera(captureSession: self.captureSession)
                    self.cameraView.session?.startRunning()
                    //self.configureSession()
                }
                
                chatVC.view?.backgroundColor = chatVC.backgroundColor
                chatVC.view.backgroundColor = chatVC.backgroundColor
                chatVC.view!.center = CGPoint(x: chatVC.view!.center.x + translation.x, y: chatVC.view!.center.y)
                //tabBar.center = CGPoint(x: tabBar.center.x + translation.x, y: tabBar.center.y)
                recognizer.setTranslation(CGPoint.zero, in: chatVC.view!)
            }
            
        }
    }
    
    // Loads the conversation screen now so when swiping back, data is already loaded
    func loadConversationScreen() {
        let vc = storyboard?.instantiateViewController(withIdentifier: Constants.CONVERSATION_VIEW) as! ConversationViewController
        Repo.sharedInstance.currentConversationID = self.conversationID
        vc.conversationID = conversationID
        vc.posts = posts
        vc.conversationTitle = self.conversationTitle
        //vc.tabBar = (self.tabBarController?.tabBar)!
        vc.navController = self.navController
        //vc.captureSession = session
        vc.isFromBubble = self.isFromBubble
        vc.mapBubble = self.mapBubble
        //vc.inputScreen = self.tabBarController as! InputTabBarController
        vc.view.layer.bounds = UIScreen.main.bounds
        
        //self.tabBarController?.view.addSubview(vc.view)
        //self.tabBarController?.tabBar.layer.zPosition = -1;
        //self.tabBarController?.tabBar.center = CGPoint(x:UIScreen.main.bounds.width * 1.5,y: (self.tabBarController?.tabBar.center.y)!)
        self.view.addSubview(vc.view)
        chatVC = vc
        
        /*let numControllers = self.tabBarController?.viewControllers?.count
        for i in 0...numControllers! - 1 {
            if self.tabBarController?.viewControllers?[i] is TextInputVC {
                let textVC = self.tabBarController?.viewControllers?[i] as! TextInputVC
                textVC.chatVC = vc
            }
        }*/
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        sessionQueue.async {
            switch self.setupResult {
            case .success:
                // Only setup observers and start the session running if setup succeeded.
                self.addObservers()
                self.session.startRunning()
                self.isSessionRunning = self.session.isRunning
                
            case .notAuthorized:
                DispatchQueue.main.async { [unowned self] in
                    let message = NSLocalizedString("AVCam doesn't have permission to use the camera, please change privacy settings", comment: "Alert message when the user has denied access to the camera")
                    let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("Settings", comment: "Alert button to open Settings"), style: .`default`, handler: { action in
                        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
                    }))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
                
            case .configurationFailed:
                DispatchQueue.main.async { [unowned self] in
                    let message = NSLocalizedString("Unable to capture media", comment: "Alert message when something goes wrong during capture session configuration")
                    let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil))
                    
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    func disableSwipe() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    func enableSwipe() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        sessionQueue.async { [unowned self] in
            if self.setupResult == .success {
                self.session.stopRunning()
                self.isSessionRunning = self.session.isRunning
                self.removeObservers()
            }
        }
        super.viewWillDisappear(animated)
    }
    
    override var shouldAutorotate: Bool {
        // Disable autorotation of the interface when recording is in progress.
        if let movieFileOutput = movieFileOutput {
            return !movieFileOutput.isRecording
        }
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        if let videoPreviewLayerConnection = cameraView.videoPreviewLayer.connection {
            let deviceOrientation = UIDevice.current.orientation
            guard let newVideoOrientation = deviceOrientation.videoOrientation, deviceOrientation.isPortrait || deviceOrientation.isLandscape else {
                return
            }
            
            videoPreviewLayerConnection.videoOrientation = newVideoOrientation
        }
    }
    
    // MARK: Session Management
    
    private enum SessionSetupResult {
        case success
        case notAuthorized
        case configurationFailed
    }
    
    private let session = AVCaptureSession()
    
    private var isSessionRunning = false
    
    private let sessionQueue = DispatchQueue(label: "session queue", attributes: [], target: nil) // Communicate with the session and other session objects on this queue.
    
    private var setupResult: SessionSetupResult = .success
    
    var videoDeviceInput: AVCaptureDeviceInput!
    
    @IBOutlet weak var cameraView: CameraView! // View before and after camera captures a picture
    
    // Call this on the session queue.
    private func configureSession() {
        if setupResult != .success {
            return
        }
        
        session.beginConfiguration()
        
        /*
         We do not create an AVCaptureMovieFileOutput when setting up the session because the
         AVCaptureMovieFileOutput does not support movie recording with AVCaptureSessionPresetPhoto.
         */
        session.sessionPreset = AVCaptureSessionPresetPhoto
        
        // Add video input.
        do {
            var defaultVideoDevice: AVCaptureDevice?
            
            // Choose the back dual camera if available, otherwise default to a wide angle camera.
            if let dualCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInDualCamera, mediaType: AVMediaTypeVideo, position: .back) {
                defaultVideoDevice = dualCameraDevice
            }
            else if let backCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .back) {
                // If the back dual camera is not available, default to the back wide angle camera.
                defaultVideoDevice = backCameraDevice
            }
            else if let frontCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: .builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .front) {
                // In some cases where users break their phones, the back wide angle camera is not available. In this case, we should default to the front wide angle camera.
                defaultVideoDevice = frontCameraDevice
            }
            
            let videoDeviceInput = try AVCaptureDeviceInput(device: defaultVideoDevice)
            
            if session.canAddInput(videoDeviceInput) {
                session.addInput(videoDeviceInput)
                self.videoDeviceInput = videoDeviceInput
                
                DispatchQueue.main.async {
                    /*
                     Why are we dispatching this to the main queue?
                     Because AVCaptureVideoPreviewLayer is the backing layer for PreviewView and UIView
                     can only be manipulated on the main thread.
                     Note: As an exception to the above rule, it is not necessary to serialize video orientation changes
                     on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
                     
                     Use the status bar orientation as the initial video orientation. Subsequent orientation changes are
                     handled by CameraViewController.viewWillTransition(to:with:).
                     */
                    let statusBarOrientation = UIApplication.shared.statusBarOrientation
                    var initialVideoOrientation: AVCaptureVideoOrientation = .portrait
                    if statusBarOrientation != .unknown {
                        if let videoOrientation = statusBarOrientation.videoOrientation {
                            initialVideoOrientation = videoOrientation
                        }
                    }
                    
                    self.cameraView.videoPreviewLayer.connection.videoOrientation = initialVideoOrientation
                    self.cameraView.videoPreviewLayer.bounds = self.view.bounds
                    //self.cameraView.videoPreviewLayer.masksToBounds = true
                    self.cameraView.videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
                    self.cameraView.videoPreviewLayer.connection.videoOrientation = AVCaptureVideoOrientation.portrait;
                }
            }
            else {
                print("Could not add video device input to the session")
                setupResult = .configurationFailed
                session.commitConfiguration()
                return
            }
        }
        catch {
            print("Could not create video device input: \(error)")
            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }
        
        // Add audio input.
        do {
            let audioDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeAudio)
            let audioDeviceInput = try AVCaptureDeviceInput(device: audioDevice)
            
            if session.canAddInput(audioDeviceInput) {
                session.addInput(audioDeviceInput)
            }
            else {
                print("Could not add audio device input to the session")
            }
        }
        catch {
            print("Could not create audio device input: \(error)")
        }
        
        // Add photo output.
        if session.canAddOutput(photoOutput)
        {
            session.addOutput(photoOutput)
            
            photoOutput.isHighResolutionCaptureEnabled = true
            photoOutput.isLivePhotoCaptureEnabled = photoOutput.isLivePhotoCaptureSupported
            livePhotoMode = photoOutput.isLivePhotoCaptureSupported ? .on : .off
        }
        else {
            print("Could not add photo output to the session")
            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }
        
        session.commitConfiguration()
    }
    
    @IBAction private func resumeInterruptedSession(_ resumeButton: UIButton)
    {
        sessionQueue.async { [unowned self] in
            /*
             The session might fail to start running, e.g., if a phone or FaceTime call is still
             using audio or video. A failure to start the session running will be communicated via
             a session runtime error notification. To avoid repeatedly failing to start the session
             running, we only try to restart the session running in the session runtime error handler
             if we aren't trying to resume the session running.
             */
            self.session.startRunning()
            self.isSessionRunning = self.session.isRunning
            if !self.session.isRunning {
                DispatchQueue.main.async { [unowned self] in
                    let message = NSLocalizedString("Unable to resume", comment: "Alert message when unable to resume the session running")
                    let alertController = UIAlertController(title: "AVCam", message: message, preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: NSLocalizedString("OK", comment: "Alert OK button"), style: .cancel, handler: nil)
                    alertController.addAction(cancelAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            else {
                DispatchQueue.main.async { [unowned self] in
                    self.recordButton.isHidden = true
                }
            }
        }
    }
    
    private enum CaptureMode: Int {
        case photo = 0
        case movie = 1
    }
    
    private func toggleCaptureMode(captureMode: String) {
        disableUI()
        if captureMode == "CAMERA" {
            
            sessionQueue.async { [unowned self] in
                /*
                 Remove the AVCaptureMovieFileOutput from the session because movie recording is
                 not supported with AVCaptureSessionPresetPhoto. Additionally, Live Photo
                 capture is not supported when an AVCaptureMovieFileOutput is connected to the session.
                 */
                self.session.beginConfiguration()
                self.session.removeOutput(self.movieFileOutput)
                self.session.sessionPreset = AVCaptureSessionPresetPhoto
                self.session.commitConfiguration()
                
                self.movieFileOutput = nil
                
                if self.photoOutput.isLivePhotoCaptureSupported {
                    self.photoOutput.isLivePhotoCaptureEnabled = true
                    
                    DispatchQueue.main.async {
                        //self.livePhotoModeButton.isEnabled = true
                        //self.livePhotoModeButton.isHidden = false
                    }
                }
            }
        }
        else if captureMode == "VIDEO"
        {
            //livePhotoModeButton.isHidden = true
            
            sessionQueue.async { [unowned self] in
                let movieFileOutput = AVCaptureMovieFileOutput()
                
                if self.session.canAddOutput(movieFileOutput) {
                    self.session.beginConfiguration()
                    self.session.addOutput(movieFileOutput)
                    self.session.sessionPreset = AVCaptureSessionPresetHigh
                    if let connection = movieFileOutput.connection(withMediaType: AVMediaTypeVideo) {
                        if connection.isVideoStabilizationSupported {
                            connection.preferredVideoStabilizationMode = .auto
                        }
                    }
                    self.session.commitConfiguration()
                    
                    self.movieFileOutput = movieFileOutput
                    
                    DispatchQueue.main.async { [unowned self] in
                        self.enableUI()
                    }
                }
            }
        } else if captureMode == "VOICE RECORDING" {
        }
        enableUI()
    }
    
    // MARK: Device Configuration
    
    private let videoDeviceDiscoverySession = AVCaptureDeviceDiscoverySession(deviceTypes: [.builtInWideAngleCamera, .builtInDualCamera], mediaType: AVMediaTypeVideo, position: .unspecified)!
    
    //let tapToFocusGesture = UITapGestureRecognizer(target: self, action: #selector(focusAndExposeTap(gestureRecognizer:)))
    
    func focusAndExposeTap(gestureRecognizer: UITapGestureRecognizer) {
        
        let devicePoint = self.cameraView.videoPreviewLayer.captureDevicePointOfInterest(for: gestureRecognizer.location(in: gestureRecognizer.view))
        focus(with: .autoFocus, exposureMode: .autoExpose, at: devicePoint, monitorSubjectAreaChange: true)
        
        if (isSessionRunning) {
            // Set the position
            let tapLocation = gestureRecognizer.location(ofTouch: 0, in: gestureRecognizer.view)
            focusRing.center = CGPoint(x: tapLocation.x, y: tapLocation.y)
            self.focusRing.fadeIn(completion: {
                (finished: Bool) -> Void in
                self.focusRing.fadeOut()
            })
        }
    }
    
    private func focus(with focusMode: AVCaptureFocusMode, exposureMode: AVCaptureExposureMode, at devicePoint: CGPoint, monitorSubjectAreaChange: Bool) {
        sessionQueue.async { [unowned self] in
            if let device = self.videoDeviceInput.device {
                do {
                    try device.lockForConfiguration()
                    
                    /*
                     Setting (focus/exposure)PointOfInterest alone does not initiate a (focus/exposure) operation.
                     Call set(Focus/Exposure)Mode() to apply the new point of interest.
                     */
                    if device.isFocusPointOfInterestSupported && device.isFocusModeSupported(focusMode) {
                        device.focusPointOfInterest = devicePoint
                        device.focusMode = focusMode
                    }
                    
                    if device.isExposurePointOfInterestSupported && device.isExposureModeSupported(exposureMode) {
                        device.exposurePointOfInterest = devicePoint
                        device.exposureMode = exposureMode
                    }
                    
                    device.isSubjectAreaChangeMonitoringEnabled = monitorSubjectAreaChange
                    device.unlockForConfiguration()
                }
                catch {
                    print("Could not lock device for configuration: \(error)")
                }
            }
        }
    }
    
    let minimumZoom: CGFloat = 1.0
    let maximumZoom: CGFloat = 3.0
    var lastZoomFactor: CGFloat = 1.0
    
    // Zooming
    func zoomOnCamera(gestureRecognizer: UIPinchGestureRecognizer) {
        
        guard let device = videoDeviceInput.device else { return }
        
        // Return zoom value between the minimum and maximum zoom values
        func minMaxZoom(_ factor: CGFloat) -> CGFloat {
            return min(min(max(factor, minimumZoom), maximumZoom), device.activeFormat.videoMaxZoomFactor)
        }
        
        let newScaleFactor = minMaxZoom(gestureRecognizer.scale * lastZoomFactor)
        
        switch gestureRecognizer.state {
        case .began: fallthrough
        case .changed: updateZoom(scale: newScaleFactor)
        case .ended:
            lastZoomFactor = minMaxZoom(newScaleFactor)
            updateZoom(scale: lastZoomFactor)
        default: break
        }
    }
    
    func updateZoom(scale factor: CGFloat) {
        guard let device = videoDeviceInput.device else { return }
        
        do {
            try device.lockForConfiguration()
            defer { device.unlockForConfiguration() }
            device.videoZoomFactor = factor
        } catch {
            print("\(error.localizedDescription)")
        }
    }
    
    // MARK: Capturing Photos
    
    private let photoOutput = AVCapturePhotoOutput()
    
    private var inProgressPhotoCaptureDelegates = [Int64 : PhotoCaptureDelegate]()
    
    private enum LivePhotoMode {
        case on
        case off
    }
    
    private var livePhotoMode: LivePhotoMode = .off
    
    @IBOutlet private weak var livePhotoModeButton: UIButton!
    
    @IBAction private func toggleLivePhotoMode(_ livePhotoModeButton: UIButton) {
        sessionQueue.async { [unowned self] in
            self.livePhotoMode = (self.livePhotoMode == .on) ? .off : .on
            let livePhotoMode = self.livePhotoMode
            
            DispatchQueue.main.async { [unowned self] in
                if livePhotoMode == .on {
                    self.livePhotoModeButton.setTitle(NSLocalizedString("Live Photo Mode: On", comment: "Live photo mode button on title"), for: [])
                }
                else {
                    self.livePhotoModeButton.setTitle(NSLocalizedString("Live Photo Mode: Off", comment: "Live photo mode button off title"), for: [])
                }
            }
        }
    }
    
    private var inProgressLivePhotoCapturesCount = 0
    
    @IBOutlet var capturingLivePhotoLabel: UILabel!
    
    func capturePhoto() {
        /*let settings = AVCapturePhotoSettings()
         let previewPixelType = settings.availablePreviewPhotoPixelFormatTypes.first!
         let previewFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
         kCVPixelBufferWidthKey as String: 160,
         kCVPixelBufferHeightKey as String: 160,
         ]
         settings.previewPhotoFormat = previewFormat
         self.photoOutput.capturePhoto(with: settings, delegate: self)*/
        
        /*
         Retrieve the video preview layer's video orientation on the main queue before
         entering the session queue. We do this to ensure UI elements are accessed on
         the main thread and session configuration is done on the session queue.
         */
        let videoPreviewLayerOrientation = cameraView.videoPreviewLayer.connection.videoOrientation
        
        sessionQueue.async {
            // Update the photo output's connection to match the video orientation of the video preview layer.
            if let photoOutputConnection = self.photoOutput.connection(withMediaType: AVMediaTypeVideo) {
                photoOutputConnection.videoOrientation = videoPreviewLayerOrientation
            }
            
            // Capture a JPEG photo with flash set to auto and high resolution photo enabled.
            let photoSettings = AVCapturePhotoSettings()
            photoSettings.flashMode = self.flashButton.flashMode
            photoSettings.isHighResolutionPhotoEnabled = true
            if photoSettings.availablePreviewPhotoPixelFormatTypes.count > 0 {
                photoSettings.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String : photoSettings.availablePreviewPhotoPixelFormatTypes.first!]
            }
            let previewPixelType = photoSettings.availablePreviewPhotoPixelFormatTypes.first!
            let previewFormat = [kCVPixelBufferPixelFormatTypeKey as String: previewPixelType,
                                 kCVPixelBufferWidthKey as String: 160,
                                 kCVPixelBufferHeightKey as String: 160,
                                 ]
            if self.livePhotoMode == .on && self.photoOutput.isLivePhotoCaptureSupported { // Live Photo capture is not supported in movie mode.
                let livePhotoMovieFileName = NSUUID().uuidString
                let livePhotoMovieFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent((livePhotoMovieFileName as NSString).appendingPathExtension("mov")!)
                photoSettings.livePhotoMovieFileURL = URL(fileURLWithPath: livePhotoMovieFilePath)
            }
            
            // Use a separate object for the photo capture delegate to isolate each capture life cycle.
            let photoCaptureDelegate = PhotoCaptureDelegate(with: photoSettings, willCapturePhotoAnimation: {
                DispatchQueue.main.async { [unowned self] in
                    self.cameraView.videoPreviewLayer.opacity = 0
                    UIView.animate(withDuration: 0.25) { [unowned self] in
                        self.cameraView.videoPreviewLayer.opacity = 1
                    }
                }
            }, capturingLivePhoto: { capturing in
                /*
                 Because Live Photo captures can overlap, we need to keep track of the
                 number of in progress Live Photo captures to ensure that the
                 Live Photo label stays visible during these captures.
                 */
                self.sessionQueue.async { [unowned self] in
                    if capturing {
                        self.inProgressLivePhotoCapturesCount += 1
                    }
                    else {
                        self.inProgressLivePhotoCapturesCount -= 1
                    }
                    
                    let inProgressLivePhotoCapturesCount = self.inProgressLivePhotoCapturesCount
                    DispatchQueue.main.async { [unowned self] in
                        if inProgressLivePhotoCapturesCount > 0 {
                            //self.capturingLivePhotoLabel.isHidden = false
                        }
                        else if inProgressLivePhotoCapturesCount == 0 {
                            //self.capturingLivePhotoLabel.isHidden = true
                        }
                        else {
                            print("Error: In progress live photo capture count is less than 0");
                        }
                    }
                }
            }, completed: { [unowned self] photoCaptureDelegate in
                // When the capture is complete, remove a reference to the photo capture delegate so it can be deallocated.
                self.sessionQueue.async { [unowned self] in
                    self.inProgressPhotoCaptureDelegates[photoCaptureDelegate.requestedPhotoSettings.uniqueID] = nil
                }
                }
            )
            
            photoSettings.previewPhotoFormat = previewFormat
            self.photoOutput.capturePhoto(with: photoSettings, delegate: self)
            
            /*
             The Photo Output keeps a weak reference to the photo capture delegate so
             we store it in an array to maintain a strong reference to this object
             until the capture is completed.
             */
            self.inProgressPhotoCaptureDelegates[photoCaptureDelegate.requestedPhotoSettings.uniqueID] = photoCaptureDelegate
            //self.photoOutput.capturePhoto(with: photoSettings, delegate: photoCaptureDelegate)
        }
    }
    
    // MARK: Recording Movies
    
    private var movieFileOutput: AVCaptureMovieFileOutput? = nil
    
    private var backgroundRecordingID: UIBackgroundTaskIdentifier? = nil
    
    func toggleMovieRecording() {
        guard let movieFileOutput = self.movieFileOutput else {
            return
        }
        
        /*
         Disable the Camera button until recording finishes, and disable
         the Record button until recording starts or finishes.
         
         See the AVCaptureFileOutputRecordingDelegate methods.
         */
        hideUI()
        disableUI()
        
        /*
         Retrieve the video preview layer's video orientation on the main queue
         before entering the session queue. We do this to ensure UI elements are
         accessed on the main thread and session configuration is done on the session queue.
         */
        let videoPreviewLayerOrientation = cameraView.videoPreviewLayer.connection.videoOrientation
        
        sessionQueue.async { [unowned self] in
            if !movieFileOutput.isRecording {
                if UIDevice.current.isMultitaskingSupported {
                    /*
                     Setup background task.
                     This is needed because the `capture(_:, didFinishRecordingToOutputFileAt:, fromConnections:, error:)`
                     callback is not received until AVCam returns to the foreground unless you request background execution time.
                     This also ensures that there will be time to write the file to the photo library when AVCam is backgrounded.
                     To conclude this background execution, endBackgroundTask(_:) is called in
                     `capture(_:, didFinishRecordingToOutputFileAt:, fromConnections:, error:)` after the recorded file has been saved.
                     */
                    self.backgroundRecordingID = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)
                }
                
                // Update the orientation on the movie file output video connection before starting recording.
                let movieFileOutputConnection = self.movieFileOutput?.connection(withMediaType: AVMediaTypeVideo)
                movieFileOutputConnection?.videoOrientation = videoPreviewLayerOrientation
                
                // Start recording to a temporary file.
                let outputFileName = NSUUID().uuidString
                let outputFilePath = (NSTemporaryDirectory() as NSString).appendingPathComponent((outputFileName as NSString).appendingPathExtension("mov")!)
                movieFileOutput.startRecording(toOutputFileURL: URL(fileURLWithPath: outputFilePath), recordingDelegate: self)
            }
            else {
                movieFileOutput.stopRecording()
                self.stopRecordingAnimation()
            }
        }
    }
    
    func capture(_ captureOutput: AVCaptureFileOutput!, didStartRecordingToOutputFileAt fileURL: URL!, fromConnections connections: [Any]!) {
        // Enable the Record button to let the user stop the recording.
        DispatchQueue.main.async { [unowned self] in
            self.recordButton.isHidden = false
            self.recordButton.isEnabled = true
            self.startRecordingAnimation()
        }
    }
    
    //var videoPlayer: AVPlayer = AVPlayer()
    //let playerLayer = AVPlayerLayer(player: player)
    var videoQueuePlayer: AVQueuePlayer = AVQueuePlayer()
    var videoPlayerLayer = AVPlayerLayer()
    
    func capture(_ captureOutput: AVCaptureFileOutput!, didFinishRecordingToOutputFileAt outputFileURL: URL!, fromConnections connections: [Any]!, error: Error!) {
        /*
         Note that currentBackgroundRecordingID is used to end the background task
         associated with this recording. This allows a new recording to be started,
         associated with a new UIBackgroundTaskIdentifier, once the movie file output's
         `isRecording` property is back to false — which happens sometime after this method
         returns.
         
         Note: Since we use a unique file path for each recording, a new recording will
         not overwrite a recording currently being saved.
         */
        
        func cleanup() {
            let path = outputFileURL.path
            if FileManager.default.fileExists(atPath: path) {
                do {
                    try FileManager.default.removeItem(atPath: path)
                }
                catch {
                    print("Could not remove file at url: \(outputFileURL)")
                }
            }
            
            if let currentBackgroundRecordingID = backgroundRecordingID {
                backgroundRecordingID = UIBackgroundTaskInvalid
                
                if currentBackgroundRecordingID != UIBackgroundTaskInvalid {
                    UIApplication.shared.endBackgroundTask(currentBackgroundRecordingID)
                }
            }
        }
        
        var success = true
        
        if error != nil {
            print("Movie file finishing error: \(error)")
            success = (((error as NSError).userInfo[AVErrorRecordingSuccessfullyFinishedKey] as AnyObject).boolValue)!
        }
        
        if success {
            videoURL = outputFileURL
        
            //clear current queue if playing straight away
            videoQueuePlayer.removeAllItems()

            for _ in 1...100 {
                 //get url of track
                let playerItem = AVPlayerItem.init(url: videoURL)
                //you can use the after property to insert it at a specific location or leave it nil
                videoQueuePlayer.insert(playerItem, after: nil)
            }
           
            videoQueuePlayer.play()
            cameraView.bringSubview(toFront: discardButton)
            cameraView.bringSubview(toFront: downloadButton)
            cameraView.bringSubview(toFront: sendButton)
            
            /*NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: videoQueuePlayer.currentItem, queue: nil, using: { (_) in
                DispatchQueue.main.async {
                    self.videoQueuePlayer.removeAllItems()
                    self.videoQueuePlayer.insert(playerItem, after: nil)
                    self.videoQueuePlayer.seek(to: kCMTimeZero)
                    self.videoQueuePlayer.play()
                }
            })*/
        }
        else {
            cleanup()
        }
        
        // Enable the Camera and Record buttons to let the user switch camera and start another recording.
        DispatchQueue.main.async { [unowned self] in
            // Only enable the ability to change camera if the device has more than one camera.
            self.stopRecordingAnimation()
            self.updateButtons(isPhotoCaptured: true)
        }
    }
    
    func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingLivePhotoToMovieFileAt outputFileURL: URL, duration: CMTime, photoDisplay photoDisplayTime: CMTime, resolvedSettings: AVCaptureResolvedPhotoSettings, error: Error?) {
        //print("did finish processing live hoto to movefile at")
    }
    
    // Keeps the original unfiltered photo
    var originalPhoto: UIImage = UIImage()
    
    // Keeps track of the filtered photo
    var filteredPhoto: UIImage = UIImage()
    
    // Keeps track of the video
    var videoURL : URL = URL(string: "https://www.apple.com")!
    
    // Called behind the scenes whenever the picture has been taken
    func capture(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhotoSampleBuffer photoSampleBuffer: CMSampleBuffer?, previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        if let error = error {
            print(error.localizedDescription)
        }
        if let sampleBuffer = photoSampleBuffer, let previewBuffer = previewPhotoSampleBuffer, let dataImage = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: sampleBuffer, previewPhotoSampleBuffer: previewBuffer) {
            
            let image = UIImage(data: dataImage)
            let flippedImage = UIImage(cgImage: (image?.cgImage)!, scale: 1.0, orientation: .rightMirrored)
            
            originalPhoto = image!
            filteredPhoto = image!
            print("IMAGE: \(filteredPhoto)")
            
            session.stopRunning()
            //cameraView.videoPreviewLayer.isHidden = true
            
            //cameraView.contentMode = .scaleAspectFill
            
            // If using the front camera, the photo needs to be flipped
            if(camType == Constants.FRONT) {
                //cameraView.image = flippedImage
                originalPhoto = image!
                filteredPhoto = image!
            }
            else {
                //cameraView.image = image
                originalPhoto = image!
                filteredPhoto = image!
            }
            
            updateButtons(isPhotoCaptured: true)
            isSessionRunning = false
            
        } else {
            print("Error: photoSampleBuffer")
        }
    }
    
    // MARK: Voice Recording
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    
    func setupVoiceRecording() {
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async { }
            }
        } catch {
            // failed to record!
        }
    }
    
    func startVoiceRecording() {
        hideUI()
        disableUI()
        
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            DispatchQueue.main.async { [unowned self] in
                self.recordButton.isHidden = false
                self.recordButton.isEnabled = true
                self.startRecordingAnimation()
            }
        } catch {
            finishVoiceRecording(success: false)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    var audioPlayer : AVAudioPlayer?
    
    func finishVoiceRecording(success: Bool) {
        audioRecorder.stop()
        let voiceRecordingURL = audioRecorder.url
        audioRecorder = nil
        self.stopRecordingAnimation()
        updateButtons(isPhotoCaptured: true)
        
        if success {
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: voiceRecordingURL)
                audioPlayer?.numberOfLoops = 100
                audioPlayer?.delegate = self
                audioPlayer?.play()
            } catch {
                print("error")
            }
        } else {
            print("recording failed")
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishVoiceRecording(success: false)
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
    }
    
    func audioRecorderDidFinishRecording(recorder: AVAudioRecorder!, successfully flag: Bool) {
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
    }
    
    // MARK: KVO and Notifications
    
    private var sessionRunningObserveContext = 0
    
    private func addObservers() {
        session.addObserver(self, forKeyPath: "running", options: .new, context: &sessionRunningObserveContext)
        
        NotificationCenter.default.addObserver(self, selector: #selector(subjectAreaDidChange), name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: videoDeviceInput.device)
        NotificationCenter.default.addObserver(self, selector: #selector(sessionRuntimeError), name: Notification.Name("AVCaptureSessionRuntimeErrorNotification"), object: session)
        
        /*
         A session can only run when the app is full screen. It will be interrupted
         in a multi-app layout, introduced in iOS 9, see also the documentation of
         AVCaptureSessionInterruptionReason. Add observers to handle these session
         interruptions and show a preview is paused message. See the documentation
         of AVCaptureSessionWasInterruptedNotification for other interruption reasons.
         */
        NotificationCenter.default.addObserver(self, selector: #selector(sessionWasInterrupted), name: Notification.Name("AVCaptureSessionWasInterruptedNotification"), object: session)
        NotificationCenter.default.addObserver(self, selector: #selector(sessionInterruptionEnded), name: Notification.Name("AVCaptureSessionInterruptionEndedNotification"), object: session)
        
        // If the user moved the app to running in the background
        NotificationCenter.default.addObserver(self, selector: #selector(movedAppToBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        // If the user moved the app to running in the foreground
        NotificationCenter.default.addObserver(self, selector: #selector(movedAppToForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
    }
    
    private func removeObservers() {
        NotificationCenter.default.removeObserver(self)
        
        session.removeObserver(self, forKeyPath: "running", context: &sessionRunningObserveContext)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &sessionRunningObserveContext {
            //let newValue = change?[.newKey] as AnyObject?
            //guard let isSessionRunning = newValue?.boolValue else { return }
            //let isLivePhotoCaptureSupported = photoOutput.isLivePhotoCaptureSupported
            //let isLivePhotoCaptureEnabled = photoOutput.isLivePhotoCaptureEnabled
            
            DispatchQueue.main.async { [unowned self] in
                // Only enable the ability to change camera if the device has more than one camera.
                self.enableUI()
            }
        }
        else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    func subjectAreaDidChange(notification: NSNotification) {
        let devicePoint = CGPoint(x: 0.5, y: 0.5)
        focus(with: .autoFocus, exposureMode: .continuousAutoExposure, at: devicePoint, monitorSubjectAreaChange: false)
    }
    
    func sessionRuntimeError(notification: NSNotification) {
        guard let errorValue = notification.userInfo?[AVCaptureSessionErrorKey] as? NSError else {
            return
        }
        
        let error = AVError(_nsError: errorValue)
        print("Capture session runtime error: \(error)")
        
        /*
         Automatically try to restart the session running if media services were
         reset and the last start running succeeded. Otherwise, enable the user
         to try to resume the session running.
         */
        if error.code == .mediaServicesWereReset {
            sessionQueue.async { [unowned self] in
                if self.isSessionRunning {
                    self.session.startRunning()
                    self.isSessionRunning = self.session.isRunning
                }
                else {
                    DispatchQueue.main.async { [unowned self] in
                        self.recordButton.isHidden = false
                    }
                }
            }
        }
        else {
            recordButton.isHidden = false
        }
    }
    
    func sessionWasInterrupted(notification: NSNotification) {
        /*
         In some scenarios we want to enable the user to resume the session running.
         For example, if music playback is initiated via control center while
         using AVCam, then the user can let AVCam resume
         the session running, which will stop music playback. Note that stopping
         music playback in control center will not automatically resume the session
         running. Also note that it is not always possible to resume, see `resumeInterruptedSession(_:)`.
         */
        if let userInfoValue = notification.userInfo?[AVCaptureSessionInterruptionReasonKey] as AnyObject?, let reasonIntegerValue = userInfoValue.integerValue, let reason = AVCaptureSessionInterruptionReason(rawValue: reasonIntegerValue) {
            print("Capture session was interrupted with reason \(reason)")
            videoQueuePlayer.pause()
        }
    }
    
    func sessionInterruptionEnded(notification: NSNotification) {
        print("Capture session interruption ended")
        videoQueuePlayer.play()
    }
    
    func movedAppToBackground() {
        print("background")
    }
    
    func movedAppToForeground() {
        print("foreground")
    }
    
    // Button defintions
    var recordButton : RecordButton!
    var flipCameraButton : FlipCameraButton!
    var flashButton : FlashButton!
    var discardButton : DiscardButton!
    var downloadButton : DownloadButton!
    var sendButton : SendButton!
    var focusRing: FocusRing!
    var pickerView: AKPickerView!
    var originalButtonCenter: CGPoint!
    var pickerIndicator : UIImageView!
    var textButton: TextButton!
    var textView: TextView!
    
    let cameraTypes = ["CAMERA", "VIDEO", "VOICE RECORDING"]
    var selectedCamera : String = "CAMERA"
    
    // Initalizes and sets up the camera buttons
    internal func setupButtons() {
        
        // set up picker
        pickerView = AKPickerView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50))
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.font = UIFont(name: "HelveticaNeue-Light", size: 15)!
        pickerView.highlightedFont = UIFont(name: "HelveticaNeue", size: 15)!
        pickerView.pickerViewStyle = .wheel
        pickerView.maskDisabled = false
        pickerView.reloadData()
        pickerView.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.maxY - pickerView.bounds.height/2)
        cameraView.addSubview(pickerView)
        
        // picker indicator
        pickerIndicator = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        pickerIndicator.image = #imageLiteral(resourceName: "picker")
        pickerIndicator.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.maxY - 5.0)
        cameraView.addSubview(pickerIndicator)
        
        // set up recorder button
        recordButton = RecordButton(frame: CGRect(x: 0,y: 0, width: 70, height: 70))
        recordButton.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.maxY - pickerView.bounds.height - recordButton.bounds.height/2)
        recordButton.buttonColor = .white
        recordButton.progressColor = .red
        recordButton.closeWhenFinished = false
        recordButton.addTarget(self, action: #selector(self.tapRecordButton), for: .touchUpInside)
        recordButton.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handleButtonPan(gesture:))))
        recordButton.center.x = self.view.center.x
        originalButtonCenter = recordButton.center
        cameraView.addSubview(recordButton)
        
        // set up the flip camera buttonColor
        flipCameraButton = FlipCameraButton(frame: CGRect(x: 0,y: 0, width: 35, height: 35))
        flipCameraButton.center = CGPoint(x: self.view.bounds.midX + 2 * recordButton.bounds.width, y: self.view.bounds.maxY - pickerView.bounds.height - recordButton.bounds.height/2)
        flipCameraButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(flipCamera)))
        cameraView.addSubview(flipCameraButton)
        
        // set up the flash button
        flashButton = FlashButton(frame: CGRect(x: 0,y: 0, width: 35, height: 35))
        flashButton.center = CGPoint(x: self.view.bounds.midX - 2 * recordButton.bounds.width, y: self.view.bounds.maxY - pickerView.bounds.height - recordButton.bounds.height/2)
        flashButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(switchFlash)))
        cameraView.addSubview(flashButton)
        
        // set up the discard button
        discardButton = DiscardButton(frame: CGRect(x: 0,y: 0, width: 35, height: 35))
        discardButton.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.maxY - discardButton.bounds.height)
        discardButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(discard)))
        cameraView.addSubview(discardButton)
        
        // set up the download button
        downloadButton = DownloadButton(frame: CGRect(x: 0,y: 0, width: 35, height: 35))
        downloadButton.center = CGPoint(x: self.view.bounds.minX + downloadButton.bounds.width, y: self.view.bounds.maxY - downloadButton.bounds.height)
        downloadButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(download)))
        cameraView.addSubview(downloadButton)
        
        // set up the text button
        textButton = TextButton(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        textButton.center = CGPoint(x: self.view.bounds.maxX - textButton.bounds.width, y: self.view.bounds.maxY - textButton.bounds.height)
        textButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addText)))
        cameraView.addSubview(textButton)
        
        // set up the send button
        sendButton = SendButton(frame: CGRect(x: 0,y: 0, width: 35, height: 35))
        sendButton.center = CGPoint(x: self.view.bounds.maxX - sendButton.bounds.width, y: self.view.bounds.maxY - sendButton.bounds.height)
        sendButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(upload)))
        cameraView.addSubview(sendButton)
        
        // set up the focus button
        focusRing = FocusRing(frame: CGRect(x: 0,y: 0, width: 35, height: 35))
        focusRing.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        cameraView.addSubview(focusRing)
        
        // set up text view
        textView = TextView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50))
        textView.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        cameraView.addSubview(textView)
    }
    
    // MARK: - AKPickerViewDataSource
    
    func numberOfItemsInPickerView(_ pickerView: AKPickerView) -> Int {
        return self.cameraTypes.count
    }
    
    /*
     
     Image Support
     -------------
     Please comment '-pickerView:titleForItem:' entirely and
     uncomment '-pickerView:imageForItem:' to see how it works.
     
     */
    func pickerView(_ pickerView: AKPickerView, titleForItem item: Int) -> String {
        return self.cameraTypes[item]
    }
    
    /*func pickerView(_ pickerView: AKPickerView, imageForItem item: Int) -> UIImage {
     return UIImage(named: self.titles[item])!
     }*/
    
    // MARK: - AKPickerViewDelegate
    
    var feedbackGenerator : UISelectionFeedbackGenerator? = nil
    
    func pickerView(_ pickerView: AKPickerView, didSelectItem item: Int) {
        
        // Prepare the generator when the gesture begins.
        feedbackGenerator?.prepare()
        
        selectedCamera = self.cameraTypes[item]
        if (selectedCamera == "VIDEO") {
            recordButton.setButtonImage(image: #imageLiteral(resourceName: "video"))
            toggleCaptureMode(captureMode: "VIDEO")
        } else if (selectedCamera == "VOICE RECORDING") {
            recordButton.setButtonImage(image: #imageLiteral(resourceName: "voiceRecording"))
            toggleCaptureMode(captureMode: "VOICE RECORDING")
            //cameraView.backgroundColor = UIColor.black
            //cameraView.layer.isHidden = true
            //cameraView.bringSubview(toFront: recordButton)
        } else {
            recordButton.setButtonImage(image: #imageLiteral(resourceName: "camera"))
            toggleCaptureMode(captureMode: "CAMERA")
        }
        
        feedbackGenerator?.selectionChanged()
    }
    
    /*
     
     Label Customization
     -------------------r
     You can customize labels by their any properties (except for fonts,)
     and margin around text.
     These methods are optional, and ignored when using images.
     
     */
    
    
    func pickerView(_ pickerView: AKPickerView, configureLabel label: UILabel, forItem item: Int) {
        label.textColor = UIColor(colorLiteralRed: 255, green: 255, blue: 255, alpha: 0.5)
        label.highlightedTextColor = UIColor.white
    }
    
    func pickerView(_ pickerView: AKPickerView, marginForItem item: Int) -> CGSize {
        return CGSize(width: 20, height: 10)
    }

    
    /*
     
     UIScrollViewDelegate Support
     ----------------------------
     AKPickerViewDelegate inherits UIScrollViewDelegate.
     You can use UIScrollViewDelegate methods
     by simply setting pickerView's delegate.
     
     */
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print("\(scrollView.contentOffset.x)")
        feedbackGenerator?.prepare()
    }

    internal func disableUI() {
        recordButton.isEnabled = false
        flipCameraButton.isUserInteractionEnabled = false
        flashButton.isUserInteractionEnabled = false
        pickerView.isUserInteractionEnabled = false
    }
    
    internal func enableUI() {
        recordButton.isEnabled = true
        flipCameraButton.isUserInteractionEnabled = true
        flashButton.isUserInteractionEnabled = true
        pickerView.isUserInteractionEnabled = true
    }
    
    internal func hideUI() {
        recordButton.isHidden = true
        flipCameraButton.isHidden = true
        flashButton.isHidden = true
        pickerView.isHidden = true
        pickerIndicator.isHidden = true
    }
    
    internal func updateButtons(isPhotoCaptured: Bool) {

        if (isPhotoCaptured) {
            recordButton.isEnabled = false
            flipCameraButton.isUserInteractionEnabled = false
            flashButton.isUserInteractionEnabled = false
            pickerView.isUserInteractionEnabled = false
        
            discardButton.isUserInteractionEnabled = true
            downloadButton.isUserInteractionEnabled = true
            //textButton.isUserInteractionEnabled = true
            sendButton.isUserInteractionEnabled = true
            
            recordButton.isHidden = true
            flipCameraButton.isHidden = true
            flashButton.isHidden = true
            pickerView.isHidden = true
            pickerIndicator.isHidden = true
            
            discardButton.isHidden = false
            downloadButton.isHidden = false
            //textButton.isHidden = false
            sendButton.isHidden = false
        } else {
            recordButton.isEnabled = true
            flipCameraButton.isUserInteractionEnabled = true
            flashButton.isUserInteractionEnabled = true
            pickerView.isUserInteractionEnabled = true
            
            discardButton.isUserInteractionEnabled = false
            downloadButton.isUserInteractionEnabled = false
            //textButton.isUserInteractionEnabled = false
            textView.isUserInteractionEnabled = false
            sendButton.isUserInteractionEnabled = false
            
            recordButton.isHidden = false
            flipCameraButton.isHidden = false
            flashButton.isHidden = false
            pickerView.isHidden = false
            pickerIndicator.isHidden = false
            
            discardButton.isHidden = true
            downloadButton.isHidden = true
            //textButton.isHidden = true
            textView.isHidden = true
            sendButton.isHidden = true
            
            downloadButton.setButtonImage(image: #imageLiteral(resourceName: "download"))
            updateZoom(scale: 1.0)
            lastZoomFactor = 1.0
        }
    }
    
    // Called when the send photo button is tapped
    internal func upload() {
        if (selectedCamera == "CAMERA") {
            let postID = databaseAccess.uploadPhoto(image: filteredPhoto, userID: Repo.sharedInstance.userID, userLocation: Repo.sharedInstance.userLocation)
            
            // If the postID is not nil, the post was sucessful, discard the original photo and animate back to the conversations screen
            if postID != nil {
                discard()
                chatVC.animateIn()
            } else {
                print("Unsuccessful post")
            }
        } else if (selectedCamera == "VIDEO") {
            let postID = databaseAccess.uploadVideo(videoURL: videoURL, userID: Repo.sharedInstance.userID, userLocation: Repo.sharedInstance.userLocation)
            
            // If the postID is not nil, the post was sucessful, discard the original photo and animate back to the conversations screen
            if postID != nil {
                discard()
                chatVC.animateIn()
            } else {
                print("Unsuccessful post")
            }
        } else {
            
        }
    }
    
    internal func tapRecordButton(){
        
        if (selectedCamera == "CAMERA") {
            capturePhoto()
        } else if (selectedCamera == "VIDEO") {
            toggleMovieRecording()
            recordButton.isUserInteractionEnabled = true
        } else if (selectedCamera == "VOICE RECORDING"){
            if audioRecorder == nil {
                startVoiceRecording()
            } else {
                finishVoiceRecording(success: true)
            }
            recordButton.isUserInteractionEnabled = true
        }
    }
    
    // MARK: Timer for Video and Voice Recording
    var progressTimer : Timer!
    var progress : CGFloat! = 0
    
    func startRecordingAnimation() {
        recordButton.buttonState = .Recording
        self.progressTimer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(CameraViewController.updateRecordingAnimation), userInfo: nil, repeats: true)
    }
    
    func updateRecordingAnimation() {
        
        let maxDuration = CGFloat(10) // Max duration of the recordButton
        
        progress = progress + (CGFloat(0.05) / maxDuration)
        recordButton.setProgress(newProgress: progress)
        
        if progress >= 1 {
            progressTimer.invalidate()
            if (selectedCamera == "VIDEO") {
                toggleMovieRecording()
            } else if (selectedCamera == "VOICE RECORDING") {
                
            }
        }
    }
    
    func stopRecordingAnimation() {
        if(recordButton.closeWhenFinished) {
            recordButton.setProgress(newProgress: 1)
            
            UIView.animate(withDuration: 0.3, animations: {
                self.recordButton.buttonState = .Hidden
            }, completion: { completion in
                self.recordButton.setProgress(newProgress: 0)
            })
        } else {
            self.recordButton.buttonState = .Idle
            self.progressTimer.invalidate()
            self.progress = 0
        }
    }
    
    // If the camType is front, the picture needs to be flipped
    var camType = Constants.FRONT
    
    func flipCamera() {
        disableUI()
        
        sessionQueue.async { [unowned self] in
            let currentVideoDevice = self.videoDeviceInput.device
            let currentPosition = currentVideoDevice!.position
            
            let preferredPosition: AVCaptureDevicePosition
            let preferredDeviceType: AVCaptureDeviceType
            
            switch currentPosition {
            case .unspecified, .front:
                preferredPosition = .back
                preferredDeviceType = .builtInDualCamera
                self.camType = Constants.BACK
            case .back:
                preferredPosition = .front
                preferredDeviceType = .builtInWideAngleCamera
                self.camType = Constants.FRONT
            }
            
            let devices = self.videoDeviceDiscoverySession.devices!
            var newVideoDevice: AVCaptureDevice? = nil
            
            // First, look for a device with both the preferred position and device type. Otherwise, look for a device with only the preferred position.
            if let device = devices.filter({ $0.position == preferredPosition && $0.deviceType == preferredDeviceType }).first {
                newVideoDevice = device
            }
            else if let device = devices.filter({ $0.position == preferredPosition }).first {
                newVideoDevice = device
            }
            
            if let videoDevice = newVideoDevice {
                do {
                    let videoDeviceInput = try AVCaptureDeviceInput(device: videoDevice)
                    
                    self.session.beginConfiguration()
                    
                    // Remove the existing device input first, since using the front and back camera simultaneously is not supported.
                    self.session.removeInput(self.videoDeviceInput)
                    
                    if self.session.canAddInput(videoDeviceInput) {
                        NotificationCenter.default.removeObserver(self, name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: currentVideoDevice!)
                        
                        NotificationCenter.default.addObserver(self, selector: #selector(self.subjectAreaDidChange), name: Notification.Name("AVCaptureDeviceSubjectAreaDidChangeNotification"), object: videoDeviceInput.device)
                        
                        self.session.addInput(videoDeviceInput)
                        self.videoDeviceInput = videoDeviceInput
                    }
                    else {
                        self.session.addInput(self.videoDeviceInput);
                    }
                    
                    if let connection = self.movieFileOutput?.connection(withMediaType: AVMediaTypeVideo) {
                        if connection.isVideoStabilizationSupported {
                            connection.preferredVideoStabilizationMode = .auto
                        }
                    }
                    
                    /*
                     Set Live Photo capture enabled if it is supported. When changing cameras, the
                     `isLivePhotoCaptureEnabled` property of the AVCapturePhotoOutput gets set to NO when
                     a video device is disconnected from the session. After the new video device is
                     added to the session, re-enable Live Photo capture on the AVCapturePhotoOutput if it is supported.
                     */
                    self.photoOutput.isLivePhotoCaptureEnabled = self.photoOutput.isLivePhotoCaptureSupported;
                    
                    self.session.commitConfiguration()
                    self.enableUI()
                }
                catch {
                    print("Error occured while creating video device input: \(error)")
                }
            }
            
            DispatchQueue.main.async { [unowned self] in
                self.enableUI()
            }
        }

    }
    
    func switchFlash() {
        if (flashButton.flashMode == .on) {
            flashButton.flashMode = .off
            flashButton.setButtonImage(image: #imageLiteral(resourceName: "flashOff"))
        } else if (flashButton.flashMode == .off) {
            flashButton.flashMode = .auto
            flashButton.setButtonImage(image: #imageLiteral(resourceName: "flashAuto"))
        } else {
            flashButton.flashMode = .on
            flashButton.setButtonImage(image: #imageLiteral(resourceName: "flashOn"))
        }
    }
    
    func discard() {
        if (selectedCamera == "CAMERA") {
            //filter.discardOverlays()
            session.startRunning()
            isSessionRunning = true
            //cameraView.layer.addSublayer(previewLayer);
            updateButtons(isPhotoCaptured: false)
            //originalPhoto = UIImage()
            //filteredPhoto = UIImage()
        } else if (selectedCamera == "VIDEO") {
            videoQueuePlayer.removeAllItems()
            videoPlayerLayer.isHidden = true
            updateButtons(isPhotoCaptured: false)
        } else if (selectedCamera == "VOICE RECORDING") {
            audioPlayer?.stop()
            updateButtons(isPhotoCaptured: false)
        }
    }
    
    func download() {
        if (selectedCamera == "CAMERA") {
            UIImageWriteToSavedPhotosAlbum(originalPhoto, nil, nil, nil);
        }
        else if (selectedCamera == "VIDEO") {
            PHPhotoLibrary.requestAuthorization { status in
                if status == .authorized {
                    // Save the movie file to the photo library and cleanup.
                    PHPhotoLibrary.shared().performChanges({
                        let options = PHAssetResourceCreationOptions()
                        options.shouldMoveFile = true
                        let creationRequest = PHAssetCreationRequest.forAsset()
                        creationRequest.addResource(with: .video, fileURL: self.videoURL, options: options)
                    }, completionHandler: { success, error in
                        if !success {
                            print("Could not save movie to photo library: \(String(describing: error))")
                        }
                    }
                    )
                }
            }
        }
        downloadButton.setButtonImage(image: #imageLiteral(resourceName: "saved"))
        downloadButton.isUserInteractionEnabled = false
    }
    
    func addText() {
        textView.isHidden = false
        textView.isUserInteractionEnabled = true
    }
    
    func handleButtonPan(gesture: UIPanGestureRecognizer) {
        
        let locationInView = gesture.location(in: self.view)
        
        if gesture.state == UIGestureRecognizerState.began {
            recordButton.move(locationInView: locationInView)
        }
        
        if gesture.state == UIGestureRecognizerState.ended {
            recordButton.stopMove()
            if(locationInView.x > 100 && locationInView.x < 300 && locationInView.y > 500) {
                UIView.animate(withDuration: 0.1) {
                    self.recordButton.center = self.originalButtonCenter
                }
                return
            }
            
        }
        
        recordButton.animateMove(locationInView: locationInView, view: self.view)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension UIDeviceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
        case .portrait: return .portrait
        case .portraitUpsideDown: return .portraitUpsideDown
        case .landscapeLeft: return .landscapeRight
        case .landscapeRight: return .landscapeLeft
        default: return nil
        }
    }
}

extension UIInterfaceOrientation {
    var videoOrientation: AVCaptureVideoOrientation? {
        switch self {
        case .portrait: return .portrait
        case .portraitUpsideDown: return .portraitUpsideDown
        case .landscapeLeft: return .landscapeLeft
        case .landscapeRight: return .landscapeRight
        default: return nil
        }
    }
}

extension AVCaptureDeviceDiscoverySession {
    func uniqueDevicePositionsCount() -> Int {
        var uniqueDevicePositions = [AVCaptureDevicePosition]()
        
        for device in devices {
            if !uniqueDevicePositions.contains(device.position) {
                uniqueDevicePositions.append(device.position)
            }
        }
        
        return uniqueDevicePositions.count
    }
}
