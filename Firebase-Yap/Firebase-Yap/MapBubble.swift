//
//  MapBubble.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 3/16/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import UIKit

class MapBubble: Bubble {
    
    var mapVC = MapViewController()
    var isStar = false
    var isLocation = false
    var isNewConversation = false
    
    public func setMapViewController(mapVC: MapViewController) {
        self.mapVC = mapVC
    }
    
    override func handleTap(gesture: UITapGestureRecognizer) {
        super.handleTap(gesture: gesture)
        
        if isNewConversation {
            mapVC.newConversationBubbleTapped()
        }
        
        if !isStar && !isLocation && Repo.sharedInstance.savedConversations.count > 0 {
            loadBubbleConversationScreen()
        }
        if isLocation{
            mapVC.centerOnUserLocation()
        }
        if isStar {
            mapVC.goToTrendingTileScreen()
        }
    }
    
    private func loadBubbleConversationScreen() {
        mapVC.loadConversationBubbleScreen()
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
