//
//  InputScreen.swift
//  Firebase-Yap
//
//  Created by Tyler Mulley on 1/27/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Tyler Mulley 
 Last Updated: 10 Febraury 2017
 Purpose: The Gallery View Controller is part of the tab set up and can be navigated to from the tab bar. It allows the user to choose a photo from the gallery to upload.
 */

import UIKit

class GalleryVC: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var conversationID = ""
    var navBar = UINavigationBar()
    
    // Gallery
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var sendButton: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        /*sendButton.contentMode = .scaleAspectFill
        let tapSendRecognizer = UITapGestureRecognizer(target: self, action: #selector(uploadPhoto))
        sendButton.isUserInteractionEnabled = true
        sendButton.addGestureRecognizer(tapSendRecognizer)
        sendButton.isHidden = true*/
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum;
            imagePicker.allowsEditing = false
            self.view.addSubview(imagePicker.view)
            
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        imagePicker.view!.removeFromSuperview()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.view!.removeFromSuperview()
        self.tabBarController?.selectedIndex = 0
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[Constants.PICKED_IMAGE] as? UIImage {
            let camera = self.tabBarController?.viewControllers?[0] as! InputScreen
            let flippedImage = UIImage(cgImage: (pickedImage.cgImage)!, scale: 1.0, orientation: .right)
            
            camera.previewLayer.removeFromSuperlayer()
            camera.cameraView.contentMode = .scaleAspectFill
            
            camera.originalPhoto = pickedImage
            //camera.filteredPhoto = flippedImage
            camera.cameraView.image = pickedImage
            //camera.camType = Constants.BACK
            camera.updateButtons(isPhotoCaptured: true)
            //camera.flipCamera()
            if(camera.camType == Constants.FRONT) {
                camera.cameraView.image = flippedImage
                camera.originalPhoto = flippedImage
                camera.filteredPhoto = flippedImage
            }
            else {
                camera.cameraView.image = pickedImage
                camera.originalPhoto = pickedImage
                camera.filteredPhoto = flippedImage
            }
            
            
            self.tabBarController?.selectedIndex = 0
            
        }
        
    }
    
    /*func uploadPhoto() {
        
        imagePicker.view!.removeFromSuperview()
    }*/
}
