//
//  textView.swift
//  RecordButton
//
//  Created by Jessica Lewis on 4/7/17.
//  Copyright © 2017 Jessica Lewis. All rights reserved.
//

import UIKit

class TextView: UITextView {
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        self.setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }
    
    internal func setupView() {
        self.isHidden = true
        self.isUserInteractionEnabled = false
        self.backgroundColor = UIColor(colorLiteralRed: 0.0, green: 0.0, blue: 0.0, alpha: 0)
        self.textColor = UIColor.black
    }
}
