//
//  TextInputDatabaseAccess.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 2/14/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import Foundation
import Firebase
import MapKit

class TextInputDatabaseAccess {
    let rootRef = FIRDatabase.database().reference()
    let ref = FIRDatabase.database().reference(withPath: "conversations")
    let usersRef = FIRDatabase.database().reference(withPath: "users")
    
    public func sendPost(userID: String, text: String, location: CLLocation) -> String {
        let conversationID = Repo.sharedInstance.currentConversationID
        let convoItemRef = self.ref.child(conversationID)
        
        let post = Post(content: text, userID: userID, time: Date.init(), userLocation: location, isText: true, conversationID: conversationID)
        let postItemRef = convoItemRef.child("posts")
        let newPostRef = postItemRef.child(post.postID)
        newPostRef.setValue(post.toAnyObject())
        post.geocodeLocation()
        
        return post.postID
    }
}
