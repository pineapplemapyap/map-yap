//
//  VCMapView.swift
//  Firebase-Yap
//
//  Created by Connor Grumling on 2/24/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Guilherme Pereira and Connor Grumling
 Last updated: 3/2/2017
 Purpose: This class deals with the map
 */

import MapKit

let PIN_IDENTIFIER = "pin"
let DEFAULT_PIN_IDENTIFIER = "DefaultPinView"


extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation.isKind(of: MKUserLocation.self) {  //Handle user location annotation..
            let annotationView = self.mapView.view(for: annotation)
            annotationView?.isUserInteractionEnabled = false
            annotationView?.canShowCallout = false
            return nil  //Default is to let the system handle it.
        }
        
        if !annotation.isKind(of: ConversationPin.self) {  //Handle non Conversation Pins
            var pinAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: DEFAULT_PIN_IDENTIFIER)
            if pinAnnotationView == nil {
                pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: DEFAULT_PIN_IDENTIFIER)
            }
            return pinAnnotationView
        }

        //Handle Conversation pins
        var view: ConversationPinView? = mapView.dequeueReusableAnnotationView(withIdentifier: PIN_IDENTIFIER) as? ConversationPinView
        if view == nil {
            view = ConversationPinView(annotation: annotation, reuseIdentifier: PIN_IDENTIFIER)
        }
        
        let annotation = annotation as! ConversationPin
        //give the pin the number of conversations in the annotation
        view?.textView.text = String(annotation.conversations.count)
        view?.annotation = annotation
        
        return view
    }
    
    //resizes an image to the given height while maintaining the aspect ratio
    func resizeImage(image: UIImage, newHeight: CGFloat) -> UIImage? {
        let scale = newHeight / image.size.height
        let newWidth = image.size.width * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    //merges 2 images into 1 image
    func mergeImages(topImage: UIImage, bottomImage: UIImage) -> UIImage {
        let newSize = CGSize(width: bottomImage.size.width + topImage.size.width + 3, height: 40)
        UIGraphicsBeginImageContextWithOptions(newSize, false, topImage.scale)
        bottomImage.draw(in: CGRect(x: 0,y: 0,width: bottomImage.size.width,height: 40))
        topImage.draw(in: CGRect(x: bottomImage.size.width+3,y: 0,width: topImage.size.width,height: 40))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
}
