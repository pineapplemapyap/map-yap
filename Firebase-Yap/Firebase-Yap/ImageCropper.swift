//
//  Picture.swift
//  sportsHype
//
//  Created by Andrew Cunningham on 11/12/16.
//  Copyright © 2016 Jessica Lewis. All rights reserved.
//

import UIKit
import Foundation

class ImageCropper {
    var image = UIImage()
    
    func makeSquare(image: UIImage) -> UIImage {
        self.image = image
        
        var sideLength:CGFloat = 0.0
        if (image.size.width > image.size.height) {
            sideLength = image.size.height
        }
        else {
            sideLength = image.size.width
        }
        
        let origin = CGPoint(x: image.size.width/2 - sideLength/2, y: image.size.height/2 - sideLength/2)
        let size = CGSize(width: sideLength, height: sideLength)
        
        let squareImage = image.crop(rect: CGRect(origin: origin, size: size))
        return squareImage
    }
    
    func makeSquare(image: UIImage, withOrigin: CGPoint) -> UIImage {
        self.image = image
        
        var sideLength:CGFloat = 0.0
        if (image.size.width > image.size.height) {
            sideLength = image.size.height
        }
        else {
            sideLength = image.size.width
        }
        
        let size = CGSize(width: sideLength, height: sideLength)
        
        let squareImage = image.crop(rect: CGRect(origin: withOrigin, size: size))
        return squareImage
    }
    
    func makeRectangle(image: UIImage, width: CGFloat, height: CGFloat, withOrigin: CGPoint) -> UIImage {
        self.image = image

        //let origin = CGPoint(x: image.size.width/2 - width/2, y: image.size.height/2 - height/2)
        let size = CGSize(width: width, height: height)
        
        let croppedCGImage = image.cgImage?.cropping(to: CGRect(origin: withOrigin, size: size))
        let newImage = UIImage(cgImage: croppedCGImage!)
        
        //let newImage = image.crop(rect: CGRect(origin: origin, size: size))
        return newImage
    }
}

extension UIImage {
    func crop( rect: CGRect) -> UIImage {
        if (rect.size.width == 0 && rect.size.height == 0)
        {
            return UIImage()
        }
        var rect = rect
        rect.origin.x*=self.scale
        rect.origin.y*=self.scale
        rect.size.width*=self.scale
        rect.size.height*=self.scale
        
        let imageRef = self.cgImage!.cropping(to: rect)
        let image = UIImage(cgImage: imageRef!, scale: self.scale * 4, orientation: self.imageOrientation)
        return image
    }
}
