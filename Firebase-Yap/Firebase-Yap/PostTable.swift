//
//  ConversationTable.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 1/5/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import UIKit
import Firebase

class PostTable: UITableViewController {
    
    let rootRef = FIRDatabase.database().reference()
    let ref = FIRDatabase.database().reference(withPath: "conversations")
    
    var conversationID = ""
    var posts = [Post]()
    var navTitle = ""
    var conversationRef = FIRDatabaseReference()
    var postsRef = FIRDatabaseReference()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = navTitle
        conversationRef = ref.child(conversationID)
        postsRef = conversationRef.child("posts")
        
        postsRef.observe(.value, with: { snapshot in
            var newPosts: [Post] = []
            
            for item in snapshot.children {
                let post = Post(snapshot: item as! FIRDataSnapshot)
                newPosts.append(post)
            }
            
            self.posts = newPosts
            self.tableView.reloadData()
        })
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return posts.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pCell", for: indexPath) as! PostCell

        // Configure the cell...
        cell.postTextLabel.text = posts[indexPath.row].content

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        let child = segue.destination as! TextInputVC
        //self.senderId = nil
        // Pass the selected object to the new view controller.
        //let indexPath = self.tableView.indexPathForSelectedRow
        //child.posts = conversations[row].posts
        //child.navTitle = conversations[row].title
        //child.conversationID = conversationID
    }
    

}
