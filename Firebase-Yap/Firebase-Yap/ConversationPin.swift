//
//  ConversationPin.swift
//  Firebase-Yap
//
//  Created by Connor Grumling on 2/20/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Guilherme Pereira and Connor Grumling
 Last updated: 3/2/2017
 Purpose: This class deals with the map
 */

import Foundation
import UIKit
import MapKit
import Contacts

let PIN_FONT = "Helvetica Neue"
let PIN_IMAGE_TITLE = "NumberPin"


class ConversationPin: NSObject, MKAnnotation {
    let title: String?
    dynamic var coordinate: CLLocationCoordinate2D
    let conversations: [Conversation]
    
    init(title: String, coordinate: CLLocationCoordinate2D, conversations: [Conversation], color: UIColor) {
        self.title = title
        self.coordinate = coordinate
        self.conversations = conversations
        super.init()
    }
    init(title: String, coordinate: CLLocationCoordinate2D, conversations: [Conversation]) {
        self.title = title
        self.coordinate = coordinate
        self.conversations = conversations
        super.init()
    }
}

//custom view goes here
class ConversationPinView: MKAnnotationView {
    private var bubbleView: UIImageView!
    var textView: UITextView!
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        //set the frame of the view
        self.frame = CGRect(x: 0, y: 0,width: 80, height: 57.5)
        
        //add the speech bubble
        self.bubbleView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 57.5))
        self.addSubview(self.bubbleView)
        bubbleView.image = UIImage(named: PIN_IMAGE_TITLE)

        //add textView for conversation number
        self.textView = UITextView(frame: CGRect(x: 5, y: 5, width: 30, height: 30))
        self.textView.font = UIFont(name: PIN_FONT, size: 15)
        self.textView.textAlignment = .center
        self.textView.isUserInteractionEnabled = false
        self.addSubview(self.textView)
        
        //round the edges of the textview into a circle
        self.textView.layer.cornerRadius = 15.5
        
        //clip the circle an dtext to stay within the bounds of the view
        self.textView.clipsToBounds = true
        self.bubbleView.clipsToBounds = true
    }
    
    //tests if the view was hit
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        if (hitView != nil)
        {
            self.superview?.bringSubview(toFront: self)
        }
        return hitView
    }
    
    //tests if point is inside the view
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let rect = self.bounds;
        var isInside: Bool = rect.contains(point);
        if(!isInside)
        {
            for view in self.subviews
            {
                isInside = view.frame.contains(point);
                if isInside
                {
                    return isInside
                }
            }
        }
        return isInside;
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
