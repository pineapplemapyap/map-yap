//
//  Post.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 1/5/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Andrew Cunningham
 Last Updated: 2 Febraury 2017
 Purpose: The Post class contains all the information pertaining to the post.
 */

import Foundation
import Firebase
import MapKit

class Post: NSObject {
    
    let coversationRef = FIRDatabase.database().reference(withPath: "conversations")
    let ref:FIRDatabaseReference?
    
    // ID of the post
    let postID: String
    
    // Content of the post
    let content: String
    
    // The ID of the user who posted it
    let userID: String
    
    // The time the post was added to the database
    let postTime: Date
    
    // The user's location at the time the post was added
    //let userLocation: CLLocation
    
    // A boolean determining if the post is text
    let isText: Bool
    
    // the conversation ID of the conversation the post is in
    var conversationID: String
    
    // the easy to read location of where the user posted
    var userLocationString = ""
    
    // the number of times the post was flagged
    var numFlagged = 0
    
    // The number of times the post was liked
    var numLikes = 0
    
    // The rating of the post
    var rating = 0.0
    
    override init() {
        self.postID = ""
        self.content = ""
        self.userID = ""
        self.postTime = Date()
        //self.userLocation = CLLocation()
        self.isText = false
        self.conversationID = ""
        //self.isLocalUserLiked = false
        self.ref = nil
    }
    
    init(content: String, userID: String, time:Date, userLocation: CLLocation, isText: Bool, conversationID: String) {
        
        self.postID = String(describing: UUID.init())
        self.content = content
        self.userID = userID
        self.postTime = time
        //self.userLocation = userLocation
        self.isText = isText
        self.conversationID = conversationID
        //self.isLocalUserLiked = false
        self.ref = nil
    }
    
    init(content: String, userID: String, time:Date, postID:String, numLikes:Int, rating:Double, userLocation: CLLocation, isText: Bool, conversationID: String) {
        
        self.postID = postID
        self.content = content
        self.userID = userID
        self.postTime = time
        self.numLikes = numLikes
        //self.userLocation = userLocation
        self.isText = isText
        self.conversationID = conversationID
        //self.isLocalUserLiked = false
        self.ref = nil
    }
    
    init(snapshot: FIRDataSnapshot) throws {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        
        postID = snapshot.key
        
        content = snapshotValue["content"] as! String
        userID = snapshotValue["user_id"] as! String
        
        let postTimeData = snapshotValue["post_time"] as! String
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZZZZZ"
        postTime = dateFormatter.date(from: postTimeData)!
        
        numLikes = snapshotValue["num_likes"] as! Int
        numFlagged = snapshotValue["num_flagged"] as! Int
        rating = snapshotValue["rating"] as! Double
        
        //let locationData = snapshotValue["user_location"] as! [String:AnyObject]
        //let lat = locationData["lat"] as! Double
        //let lon = locationData["lon"] as! Double
        //userLocation = CLLocation(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(lon))
 
        isText = snapshotValue["is_text"] as! Bool
        userLocationString = snapshotValue["user_location_string"] as! String
        //isLocalUserLiked = false
        
        conversationID = ""
        ref = snapshot.ref
    }
    
    public func geocodeLocation() {
        let geocoder = CLGeocoder()
        /*geocoder.reverseGeocodeLocation(userLocation, completionHandler: {
            (placemark, error) in self.processGeocodeResponse(withPlacemarks: placemark, error: error)
        })*/
    }
    
    func toAnyObject() -> Any {
        return [
            "content" : content,
            "user_id" : userID,
            "num_likes" : numLikes,
            "post_time" : postTime.description,
            "rating" : rating,
            //"user_location" : userLocation.toAnyObject(),
            "is_text" : isText,
            "num_flagged" : numFlagged,
            "user_location_string" : userLocationString
        ]
    }
    
    private func processGeocodeResponse(withPlacemarks placemarks: [CLPlacemark]?, error: Error?) {
        var locationString = ""
        
        if let error = error {//err1 means there was an error getting the geocode
            print("Unable to Reverse Geocode Location (\(error))")
            locationString = "ERR1"
        } else {
            if let placemarks = placemarks, let placemark = placemarks.first {
                
                if let town = placemark.locality {
                    locationString += town
                }
                
                if let country = placemark.country {
                    
                    if country == "United States" {
                        locationString += ", "
                        locationString += placemark.administrativeArea! // use state name
                    }
                    else {
                        locationString += ", "
                        locationString += placemark.country!
                    }
                }
                
            } else { // err2 means it could not find the address from the coords
                locationString = "ERR2"
            }
        }
        
        userLocationString = locationString
        updateUserLocationString()
    }
    
    private func updateUserLocationString() {
        let postRef = coversationRef.child(conversationID).child("posts")//.child(postID).child("user_location_string")
        postRef.observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            if snapshot.hasChild(self.postID) {
                snapshot.ref.child(self.postID).child("user_location_string").setValue(self.userLocationString)
            } else {
                print(snapshot)
                print(snapshot.ref)
                print(self.postID)
                print(self.conversationID)
                print(self.content)
                print(Repo.sharedInstance.currentConversationID)
            }
            
        })
    }
}
