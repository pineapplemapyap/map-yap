//
//  SendButton.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 4/15/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import Foundation

class SendButton: UIImageView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupButton()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupButton()
    }
    
    internal func setupButton() {
        self.image = #imageLiteral(resourceName: "send")
        self.isHidden = true
        self.isUserInteractionEnabled = false
        self.contentMode = .scaleAspectFill
    }
    
    func setButtonImage(image: UIImage) {
        self.image = image
    }
}
