//
//  Geocoder.swift
//  Firebase-Yap
//
//  Created by Nick Gillot on 3/2/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import Foundation
import CoreLocation

class Geocoder {
    
    var locationName = ""
    let geoCoder = CLGeocoder()
    func reverseGeocode(loc: CLLocation) -> String {
        geoCoder.reverseGeocodeLocation(loc, completionHandler: {(placemarks, error) -> Void in
            if (error != nil){
                self.locationName = " "
            }
            else if ((placemarks?.count)! > 0){
                let pm = placemarks?[0]
                self.locationName = pm!.locality!
                let country = pm!.country!
                if country == "United States" {
                    self.locationName += ", "
                    self.locationName += pm!.administrativeArea! // use state name
                }
                else {
                    self.locationName += ", "
                    self.locationName += pm!.country!
                }
            }
        })

        return self.locationName
    }
    
}
