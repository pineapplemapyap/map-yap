//
//  RepoDatabaseAccess.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 3/17/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import Foundation
import Firebase

class RepoDatabaseAccess {
    
    static let ref = FIRDatabase.database().reference(withPath: "conversations")
    
    static func observeSavedConversation(conversationID: String) {
        let postsRef = ref.child(conversationID).child("posts")
        
        postsRef.observe(.value, with: { (snapshot) -> Void in
            var newPosts: [Post] = []
            
            for item in snapshot.children {
                if let post = try? Post(snapshot: item as! FIRDataSnapshot) {
                    newPosts.append(post)
                }
            }
            
            updateSavedConversation(conversationID: conversationID, posts: newPosts)
        })
    }
    
    static func updateSavedConversation(conversationID: String, posts: [Post]) {
        
        if let newConversationIndex = Repo.sharedInstance.savedConversations.index(where: { (possibleConversation) in
                return possibleConversation.conversationID == conversationID
            }) {
                Repo.sharedInstance.savedConversations[newConversationIndex].posts = posts
            }
    }
}
