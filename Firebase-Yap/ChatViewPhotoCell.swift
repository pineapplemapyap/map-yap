//
//  ChatViewPhotoCell.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 2/17/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import UIKit

class ChatViewPhotoCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var likedImage: UIImageView!
    @IBOutlet weak var numLikesLabel: UILabel!
    // MARK: - View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
