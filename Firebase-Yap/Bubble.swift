//
//  Bubble.swift
//  sportsHype
//
//  Created by Andrew Cunningham on 11/12/16.
//  Copyright © 2016 Jessica Lewis. All rights reserved.
//

import UIKit
import Foundation

class Bubble:UIView {
    let RADIUS:CGFloat = 60.0

    init() {
        super.init(frame: CGRect(x: 0, y: 0, width: RADIUS, height: RADIUS + 10.0))
        
        layer.borderWidth = 2.0
        layer.masksToBounds = false
        layer.borderColor = UIColor.clear.cgColor
        clipsToBounds = true
        
        self.isUserInteractionEnabled = true
        
        addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(handlePan(gesture:))))
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap(gesture:))))
    }
    
    func setImage(image: UIImage) {
        let bubbleImage = BubbleImageView(image: image)
        self.addSubview(bubbleImage)
    }
    
    func setImage(image: UIImage, x: CGFloat, y: CGFloat) {
        let bubbleImage = BubbleImageView(image: image, x: x, y: y)
        self.addSubview(bubbleImage)
    }
    
    func setImage(image: UIImage, color: UIColor) {
        let bubbleImage = BubbleImageView(image: image)
        bubbleImage.backgroundColor = color
        bubbleImage.layer.borderColor = UIColor.clear.cgColor
        self.addSubview(bubbleImage)
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var dragStart : CGPoint?
    
    // used code from: http://stackoverflow.com/questions/33316965/dragging-images-around-the-screen-using-swift
    func handlePan(gesture: UIPanGestureRecognizer!) {
        if gesture.state == UIGestureRecognizerState.began {
            let locationInView = gesture.location(in: superview)
            dragStart = CGPoint(x: locationInView.x - center.x, y: locationInView.y - center.y)
            
            layer.shadowOffset = CGSize(width: 0, height: 5)
            layer.shadowOpacity = 0.3
            layer.shadowRadius = 2
            
            return
        }
        
        if gesture.state == UIGestureRecognizerState.ended {
            dragStart = nil
            
            layer.shadowOffset = CGSize(width: 0, height: 0)
            layer.shadowOpacity = 0.0
            layer.shadowRadius = 0
            
            return
        }
        
        let locationInView = gesture.location(in: superview)
        
        UIView.animate(withDuration: 0.1) {
            var locationY = locationInView.y - self.dragStart!.y
            var locationX = locationInView.x - self.dragStart!.x
            
            if ((locationY) - self.frame.height / 2.0) < Constants.STATUS_BAR_HEIGHT + Constants.BUBBLE_SNAP_DISTANCE {
                locationY = Constants.STATUS_BAR_HEIGHT + self.frame.height / 2.0
            } else if ((locationY) + self.frame.height / 2.0) > (self.superview?.frame.height)! - Constants.BUBBLE_SNAP_DISTANCE {
                locationY = (self.superview?.frame.height)! - (Constants.STATUS_BAR_HEIGHT + 5.0)
            }
            
            if ((locationX) - self.frame.width / 2.0) < Constants.BUBBLE_SNAP_DISTANCE {
                locationX = 0.0 + self.frame.width / 2.0
            } else if ((locationX) + self.frame.width/2.0) > (self.superview?.frame.width)! - Constants.BUBBLE_SNAP_DISTANCE {
                locationX = (self.superview?.frame.width)! - self.frame.width/2.0
            }
            
            self.center = CGPoint(x: locationX,
                                  y: locationY)
        }
    }
    
    func handleTap(gesture: UITapGestureRecognizer) {
        
        layer.shadowOffset = CGSize(width: 0, height: 5)
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 2
        
        let when = DispatchTime.now() + 0.1 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.layer.shadowOffset = CGSize(width: 0, height: 0)
            self.layer.shadowOpacity = 0.0
            self.layer.shadowRadius = 0
        }
    }
}
