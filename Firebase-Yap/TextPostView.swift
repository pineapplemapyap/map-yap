//
//  TextPostView.swift
//  Firebase-Yap
//
//  Created by Andrew Cunningham on 3/15/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
/*
 Authors: Andrew Cunningham
 Last Updated: 15 March 2017
 Purpose: This View Controller displays the conversation screen. PostViews are placed on the VC.
 */

import UIKit

class TextPostView: PostView {
    
    var contentLabel = UILabel()
    
    init() {
        super.init(frame: CGRect(x: 0.0, y: Constants.CONVERSATION_HEADER_HEIGHT, width: UIScreen.main.bounds.height / 4.0, height: UIScreen.main.bounds.width / 5.5))
        
        backgroundColor = UIColor.white
        layer.borderWidth = 3.0
        layer.borderColor = UIColor.white.cgColor
        layer.masksToBounds = false
        layer.cornerRadius = 5.0
        
        self.isUserInteractionEnabled = true
        
        locationLabel = UILabel(frame: CGRect(x: 3.0, y: frame.height - 23.0, width: frame.width - 10.0, height: 20.0))
        locationLabel.font = UIFont(name: "KohinoorBangla-Semibold", size: CGFloat(12))
        locationLabel.textAlignment = .right
        
        contentLabel = UILabel(frame: CGRect(x: 3.0, y: 3.0, width: frame.width - 3.0, height: frame.height - 23.0))
        contentLabel.font = UIFont(name: "KohinoorBangla-Semibold", size: CGFloat(14))
        contentLabel.numberOfLines = 2
        contentLabel.adjustsFontSizeToFitWidth = true
        contentLabel.textAlignment = .left
        
        self.addSubview(locationLabel)
        self.addSubview(contentLabel)
        
        isUserInteractionEnabled = true
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap(gesture:))))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func handleTap(gesture: UITapGestureRecognizer) {
        
        layer.shadowOffset = CGSize(width: 0, height: 5)
        layer.shadowOpacity = 0.3
        layer.shadowRadius = 2
        
        let when = DispatchTime.now() + 0.1 // change to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.layer.shadowOffset = CGSize(width: 0, height: 0)
            self.layer.shadowOpacity = 0.0
            self.layer.shadowRadius = 0
        }
        
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
