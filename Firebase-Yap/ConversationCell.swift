//
//  ConversationCell.swift
//  Firebase-Yap
//
//  Created by Jessica Lewis on 1/5/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//

import UIKit

class ConversationCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
