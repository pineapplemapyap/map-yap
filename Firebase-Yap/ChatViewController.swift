//
//  ChatViewController.swift
//  Firebase-Yap
//
//  Created by Nick Gillot on 2/1/17.
//  Copyright © 2017 Andrew Cunningham. All rights reserved.
//
//  some code from: https://www.raywenderlich.com/140836/firebase-tutorial-real-time-chat-2
/*
 Authors: Andrew Cunningham
 Last Updated: 1 February 2017
 Purpose: This is the old chat view controller. It will be deleted after the new chat controller is connected
 */

import UIKit
import Photos
import JSQMessagesViewController
import CoreLocation
import Firebase

class ChatViewController: JSQMessagesViewController, CLLocationManagerDelegate {
    
    // Database
    var databaseAccess: ChatViewDatabaseAccess!
    
    let screenWidth = UIScreen.main.bounds.width
    
    var posts = [Post]()
    var navTitle = ""
    var navBar : UINavigationBar = UINavigationBar()
    var navController = UINavigationController()
    var tabBar : UITabBar = UITabBar()
    var numParticipants = 0
    
    var isOnChat = true
    var hideMessages = false
    var colViewBack = UIColor()
    var viewBack = UIColor()
    var gestureRecognizer = UIGestureRecognizer()
    var captureSession = AVCaptureSession()
    
    var messages: [JSQMessage] = []
    var photoMessageMap = [String: JSQPhotoMediaItem]()
    
    var cameraOverlap = 30
    
    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()
    
    var conversationID = ""
    
    // location manager
    let locManager = CLLocationManager()
    var deviceLng = ""
    var deviceLat = ""
    var locationName = "" // write to the label
    let coder = CLGeocoder()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        databaseAccess = ChatViewDatabaseAccess.init(chatView: self)
        databaseAccess.observeMessages()
        
        // location
        locManager.delegate = self
        locManager.requestAlwaysAuthorization()
        locManager.startUpdatingLocation()
        
        // geocode using stored repo location to get location name
        coder.reverseGeocodeLocation(Repo.sharedInstance.userLocation, completionHandler: {(placemarks, error) ->Void in
            if (error != nil){
                self.locationName = " "
            }
            else if ((placemarks?.count)! > 0){
                let pm = placemarks?[0]
                //print("placemark:")
                //print(pm!.locality!)
                //self.locationName = pm!.locality!
                print(self.locationName)
            }
        })
        
        // Camera turning off/on
        captureSession.stopRunning()
        
        // Get device ID
        self.senderId = Repo.sharedInstance.userID
        self.senderDisplayName = Repo.sharedInstance.userID
        
        // No avatars
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero
        
        
        // Hide toolbar
        self.inputToolbar.removeFromSuperview()
        
        // Set the title of the nav bar
        self.title = navTitle
        
        messages.removeAll()
        
        // Sets the view equal to the bounds
        self.view.bounds = UIScreen.main.bounds
        self.view.layer.position = CGPoint(x: UIScreen.main.bounds.size.width / 2, y: UIScreen.main.bounds.size.height / 2)
        
        // fix stupid navBar covering messages bug
        // may have to adjust constant
        self.topContentAdditionalInset = navBar.layer.bounds.height + 20
        
        // Add the pan gesture
        gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(recognizer:)))
        self.view.addGestureRecognizer(gestureRecognizer)

        // Add the current user to the number of participants in the database
        addActiveUser(userID: Repo.sharedInstance.userID)
        
        // Remember colors of the background
        colViewBack = (self.collectionView?.backgroundColor)!
        viewBack = self.view.backgroundColor!
    }
    
    // Pulls the chat screen in after posting text
    func animateIn() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
            self.collectionView?.backgroundColor = self.colViewBack
            self.view.backgroundColor = self.viewBack
            self.view!.center = CGPoint(x: self.screenWidth * 0.5, y: self.view!.center.y)
            self.navBar.center = CGPoint(x: self.screenWidth * 0.5, y: self.navBar.center.y)
        },
                       completion: { finished in
                        self.isOnChat = true
                        self.hideMessages = false
                        self.collectionView.reloadData()
        })
    }
    
    // Add the current user to the number of participants in the database
    func addActiveUser(userID: String){
        //adds to conversation the userID
        databaseAccess.incrementActiveUsers(userID: userID)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // location functions
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.distanceFilter = 1
        locManager.startUpdatingLocation()
    }
    // when location changes, store location and change location name
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        deviceLat = String(locations[0].coordinate.latitude)
        deviceLng = String(locations[0].coordinate.longitude)
        
        Repo.sharedInstance.userLocation = locations[0]
        
        coder.reverseGeocodeLocation(locManager.location!, completionHandler: {(placemarks, error) -> Void in
                if (error != nil){
                   self.locationName = " "
                }
                else if ((placemarks?.count)! > 0){
                    let pm = placemarks?[0]
                    self.locationName = pm!.locality!
                    let country = pm!.country!
                    if country == "United States" {
                        self.locationName += ", "
                        self.locationName += pm!.administrativeArea! // use state name
                    }
                    else {
                        self.locationName += ", "
                        self.locationName += pm!.country!
                    }
                }
        })

    }
    
    //like a message
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        posts[indexPath.item].numLikes = databaseAccess.incrementNumLikes(post: posts[indexPath.item])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // Displaying messages
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }
    
    // Set the number of messages to display
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (hideMessages) {
            return 0
        }
        else {
            return messages.count
        }
    }
    
    // Displaying Messages images
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item] // 1
        if message.senderId == senderId  { // 2
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView
        }
    }
    
    // Text color of messages
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]
        
        if message.senderId == senderId {
            cell.textView?.textColor = UIColor.white
        } else {
            cell.textView?.textColor = UIColor.black
        }
        return cell
    }
    
    // Setting up bubbles
    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.jsq_messageBubbleBlue())
    }
    
    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let child = segue.destination as? InputTabBarController {
            child.conversationID = conversationID
        }
    }
    
    // Switching between camera and conversation view
    func handlePan(recognizer: UIPanGestureRecognizer) {
        
        if (recognizer.state == UIGestureRecognizerState.ended) {
            if (recognizer.view!.center.x <= 0) {
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                    self.collectionView?.backgroundColor = UIColor.clear
                    self.view.backgroundColor = UIColor.clear
                    recognizer.view!.center = CGPoint(x: self.screenWidth * -0.5, y: recognizer.view!.center.y)
                    self.navBar.center = CGPoint(x: self.screenWidth * -0.5, y: self.navBar.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth/2 , y: self.tabBar.center.y)},
                               completion: { finished in
                                self.isOnChat = false
                                self.hideMessages = true
                                self.collectionView.reloadData()
                })
            }
            else {
                captureSession.stopRunning()
                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                    recognizer.view!.center = CGPoint(x: UIScreen.main.bounds.width * 0.5, y: recognizer.view!.center.y)
                    self.navBar.center = CGPoint(x: self.screenWidth * 0.5, y: self.navBar.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth * 1.5 , y: self.tabBar.center.y)
                },
                               completion: { finished in
                                self.isOnChat = true
                                self.hideMessages = false
                                self.collectionView.reloadData()
                })
            }
        }
        
        if recognizer.state == .began || recognizer.state == .changed {
            let translation = recognizer.translation(in: self.view)
            if (translation.x < -40) {
                if (!captureSession.isRunning){
                    captureSession.startRunning()
                }
                self.collectionView?.backgroundColor = colViewBack
                self.view.backgroundColor = viewBack
                
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                    recognizer.view!.center = CGPoint(x: self.screenWidth * -0.5, y: recognizer.view!.center.y)
                    self.navBar.center = CGPoint(x: self.screenWidth * -0.5, y: self.navBar.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth/2 , y: self.tabBar.center.y)
                },
                               completion: { finished in
                                self.isOnChat = false
                                self.hideMessages = true
                                self.collectionView.reloadData()
                })
            }
            else if (translation.x > 40 && recognizer.view!.center.x < UIScreen.main.bounds.width * 0.5) {
                if (captureSession.isRunning){
                    captureSession.stopRunning()
                }
                
                self.collectionView?.backgroundColor = colViewBack
                self.view.backgroundColor = viewBack
                UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                    recognizer.view!.center = CGPoint(x: self.screenWidth * 0.5, y: recognizer.view!.center.y)
                    self.navBar.center = CGPoint(x: self.screenWidth * 0.5, y: self.navBar.center.y)
                    self.tabBar.center = CGPoint(x: self.screenWidth * 1.5 , y: self.tabBar.center.y)
                },
                               completion: { finished in
                                self.isOnChat = true
                                self.hideMessages = false
                                self.collectionView.reloadData()
                })
            }
            else if(translation.x < 0 || translation.x > 0 && recognizer.view!.center.x < UIScreen.main.bounds.width * 0.5) {
                
                DispatchQueue.global(qos: .background).async {
                    _ = StartCamera(captureSession: self.captureSession)
                }
                
                self.collectionView?.backgroundColor = colViewBack
                self.view.backgroundColor = viewBack
                recognizer.view!.center = CGPoint(x: recognizer.view!.center.x + translation.x, y: recognizer.view!.center.y)
                navBar.center = CGPoint(x: recognizer.view!.center.x + translation.x, y: navBar.center.y)
                tabBar.center = CGPoint(x: tabBar.center.x + translation.x, y: tabBar.center.y)
                recognizer.setTranslation(CGPoint.zero, in: self.view)
                
            }
            else if (translation.x > 50 && isOnChat) {
                navController.popToRootViewController(animated: true)
                isOnChat = true
            }
        }
    }

    
    
    // Add message
    // Add text message
    private func addMessage(withId id: String, name: String, text: String) {
        if let message = JSQMessage(senderId: id, displayName: name, text: text) {
            messages.append(message)
        }
    }
    
    // Add photo message
    private func addPhotoMessage(withId id: String, key: String, mediaItem: JSQPhotoMediaItem) {
        if let message = JSQMessage(senderId: id, displayName: "", media: mediaItem) {
            messages.append(message)
            
            if (mediaItem.image == nil) {
                photoMessageMap[key] = mediaItem
            }
            
            collectionView.reloadData()
        }
    }
    
    private func fetchImageDataAtURL(_ photoURL: String, forMediaItem mediaItem: JSQPhotoMediaItem, clearsPhotoMessageMapOnSuccessForKey key: String?) {
        let storageRef = FIRStorage.storage().reference(forURL: photoURL)
        
        storageRef.data(withMaxSize: INT64_MAX){ (data, error) in
            if let error = error {
                print("Error downloading image data: \(error)")
                return
            }
            
            storageRef.metadata(completion: { (metadata, metadataErr) in
                if let error = metadataErr {
                    print("Error downloading metadata: \(error)")
                    return
                }
            
                if (metadata?.contentType == "image/gif") {
                    //mediaItem.image = UIImage.gifWithData(data!)
                    print("ERROR: image/gif")
                } else {
                    mediaItem.image = UIImage.init(data: data!)
                }
                self.collectionView.reloadData()
                
                // 5
                guard key != nil else {
                    return
                }
                self.photoMessageMap.removeValue(forKey: key!)
            })
        }
    }


}
