'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
//const mkdirp = require('mkdirp-promise');
const gcs = require('@google-cloud/storage')();
const vision = require('@google-cloud/vision')();
//const exec = require('child-process-promise').exec;
const LOCAL_TMP_FOLDER = '/tmp/';


// **************** TESTS ****************************
// // Start writing Firebase Functions
// // https://firebase.google.com/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// })

// Take the text parameter passed to this HTTP endpoint and insert it into the
// Realtime Database under the path /messages/:pushId/original
/*exports.addMessage = functions.https.onRequest((req, res) => {
  // Grab the text parameter.
  const original = req.query.text;
  // Push it into the Realtime Database then send a response
  admin.database().ref('/messages').push({original: original}).then(snapshot => {
    // Redirect with 303 SEE OTHER to the URL of the pushed object in the Firebase console.
    res.redirect(303, snapshot.ref);
  });
});*/

// **************** TESTS ****************************

// ********* BEGIN IMAGE PROCESSING *************

/**
 * When an image is posted we check if it is flagged as Adult or Violence by the Cloud Vision
 * API and if it is we blur it using ImageMagick.
 */
exports.removeVulgarImages = functions.storage.object().onChange(event => {
  const object = event.data;
  const file = gcs.bucket(object.bucket).file(object.name);
  //console.log(object);
  //console.log(file);
    // Exit if this is a move or deletion event.
    if (object.resourceState === 'not_exists') {
      return console.log('This is a deletion event.');
    }
      // Check the image content using the Cloud Vision API.
      // return vision ??
    return vision.detectSafeSearch(file).then(data => {
      const safeSearch = data[0];
      //console.log('SafeSearch results on image', safeSearch);

      if (safeSearch.adult || safeSearch.violence) {
        //return blurImage(object.name, object.bucket, object.metadata);
        console.log('vulgar image content detected');
        return;
      }
    });
  
  //console.log("dict:"+object._delta[Object.keys(object._delta)]);
  //console.log("is_text:"+object._delta[Object.keys(object._delta)].is_text);
  //var isText = object._delta[Object.keys(object._delta)].is_text;
  //var content = object._delta[Object.keys(object._delta)].content;
  
  /*if (isText != null && isText) {
    // do text vulgarity check
    
  }
  else if (content != 'NOTSET'){
    
      
      console.log(object._delta);
      var filePath = admin.database().ref(content.substring(24, content.length));
      console.log(filePath);
      

  }
  else {
    // isText is null: posting post location
    return;
  }*/
  
  
});

// for text processing
//exports.removeVulgarContent = functions.database.ref('/conversations/{conversationID}/posts/').onWrite(event => {

/**
 * Blurs the given image located in the given bucket using ImageMagick.
 */
/*function blurImage(filePath, bucketName, metadata) {
  const filePathSplit = filePath.split('/');
  filePathSplit.pop();
  const fileDir = filePathSplit.join('/');
  const tempLocalDir = `${LOCAL_TMP_FOLDER}${fileDir}`;
  const tempLocalFile = `${LOCAL_TMP_FOLDER}${filePath}`;
  const bucket = gcs.bucket(bucketName);

  // Create the temp directory where the storage file will be downloaded.
  return mkdirp(tempLocalDir).then(() => {
    console.log('Temporary directory has been created', tempLocalDir);
    // Download file from bucket.
    return bucket.file(filePath).download({
      destination: tempLocalFile
    });
  }).then(() => {
    console.log('The file has been downloaded to', tempLocalFile);
    // Blur the image using ImageMagick.
    return exec(`convert ${tempLocalFile} -channel RGBA -blur 0x8 ${tempLocalFile}`);
  }).then(() => {
    console.log('Blurred image created at', tempLocalFile);
    // Uploading the Blurred image.
    return bucket.upload(tempLocalFile, {
      destination: filePath,
      metadata: {metadata: metadata} // Keeping custom metadata.
    });
  }).then(() => {
    console.log('Blurred image uploaded to Storage at', filePath);
  });
}*/
